/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.clt;

import java.net.URI;
import java.net.URISyntaxException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;
import com.openexchange.cli.AbstractRestCLI;

/**
 * {@link ImageConverterAdmin}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class ImageConverterAdmin extends AbstractRestCLI<Void> {

    protected static final String DELETE_LONG  = "delete";
    protected static final String DELETE_SHORT = "d";

    protected static final String METRICS_LONG  = "metrics";
    protected static final String METRICS_SHORT = "m";

    protected static final String CONTEXT_LONG    = "context";
    protected static final String CONTEXT_SHORT   = "c";
    protected static final String CONTEXT_DEFAULT = "";

    protected static final String ENDPOINT_LONG    = "api-root";
    protected static final String ENDPOINT_DEFAULT = "http://localhost:8005/imageconverter/";

    /**
     * @param args
     */
    public static void main(String[] args) {
        new ImageConverterAdmin().execute(args);
    }

    /**
     * Initializes a new {@link ImageConverterAdmin}.
     */
    public ImageConverterAdmin() {
        super();
    }

    // - AbstractRestCLI -----------------------------------------------------

    @Override
    protected Boolean requiresAdministrativePermission() {
        return Boolean.TRUE;
    }

    /**
     *
     */
    @Override
    protected void checkArguments(CommandLine cmd) {
        // nothing to do
    }

    /**
    *
    */
    @Override
    protected void checkOptions(CommandLine cmd) {
        // nothing to do
    }

    /**
    *
    */
    @Override
    protected String getFooter() {
        return "ImageConverter administration.";
    }

    /**
    *
    */
    @Override
    protected String getName() {
        return "imageconverter-admin -U myUser:myPassword [OPTIONS]";
    }

    /**
     *
     */
    @Override
    protected String getHeader() {
        return "imageconverter-admin -U myUser:myPassword [-m | -d [-c context]] [--api-root URL]";
    }

    /**
     *
     */
    @Override
    protected void addOptions(Options options) {
        options.addOption(METRICS_SHORT, METRICS_LONG, false, "Show the current metrics of the running server.");
        options.addOption(DELETE_SHORT, DELETE_LONG, false, "Delete all image keys on the running server, add optional \"-c context\" to restrict removal to that context");

        options.addOption(CONTEXT_SHORT, CONTEXT_LONG, true, "The optional context of the keys that should be removed. Default: n/a.");

        options.addOption(null, ENDPOINT_LONG, true, " URL to an alternative HTTP API endpoint. Example: 'https://192.168.0.1:8005/imageconverter/'");
    }

    /**
     *
     */
    @Override
    protected WebTarget getEndpoint(String endpointHost, CommandLine cmd) {
        final String endpoint = cmd.hasOption(ENDPOINT_LONG) ?
            cmd.getOptionValue(ENDPOINT_LONG, ENDPOINT_DEFAULT) :
                endpointHost + "/imageconverter";

        try {
            final URI uri = new URI(endpoint);
            final ClientConfig config = new ClientConfig();
            WebTarget target = ClientBuilder.newClient(config).target(uri);

            if (cmd.hasOption(METRICS_SHORT)) {
                target = target.path("status").queryParam("metrics", "true");
            } else if (cmd.hasOption(DELETE_SHORT)) {
                final String context = cmd.getOptionValue(CONTEXT_SHORT);

                target = target.path("clearImages");

                if ((null != context) && (context.length() > 0)) {
                    target = target.queryParam("context", context);
                }
            }

            return target;
        } catch (URISyntaxException e) {
            System.err.print("Unable to return endpoint: " + e.getMessage());
        }

        return null;
    }

    /**
     *
     */
    @Override
    protected Void invoke(Options option, CommandLine cmd, Builder context) throws Exception {
        context.accept(MediaType.APPLICATION_JSON_TYPE, MediaType.APPLICATION_OCTET_STREAM_TYPE, MediaType.TEXT_PLAIN_TYPE);

        final String response = cmd.hasOption(DELETE_SHORT) ? context.delete(String.class) : context.get(String.class);
        final JSONObject entity = new JSONObject(response);

        System.out.println("Operation result: " + entity.toString());

        return null;
    }
}
