/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client;

import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import com.openexchange.annotation.NonNull;
import com.openexchange.annotation.Nullable;
import com.openexchange.auth.Credentials;
import com.openexchange.imageconverter.client.generated.invoker.ApiClient;
import com.openexchange.imageconverter.client.generated.modules.ImageConverterApi;

/**
 * {@link ImageConverterAPIProvider}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
class ImageConverterApiProvider {

    // Default connection timeout of 10s
    final private static int DEFAULT_CONNECT_TIMEOUT_MILLIS = 10 * 1000;

    // Default read timeout of 60s
    final private static int DEFAULT_READ_TIMEOUT_MILLIS = 60 * 1000;

    /**
     * Unused
     */
    @SuppressWarnings("unused")
    private ImageConverterApiProvider() {
        // Unused
        super();
        m_apiClient = null;
    }

    /**
     * Initializes a new {@link ImageConverterAPIProvider}.
     *
     * @param imageConverterRemoteValidator
     */
    ImageConverterApiProvider(
        @NonNull final ImageConverterClientConfig imageConverterClientConfig,
        @Nullable final SSLSocketFactory sslSocketFactory,
        @Nullable final SSLContext sslContext) {

        super();

        if (null != imageConverterClientConfig.getImageConverterServerURL()) {
            // Using own http(s) connection factory to use given SSLSocketfactory;
            // since this SSLSocketfactory might be overridden by the Jersey impl. using
            // the configured SSLContext, the originating SSLContext needs to be
            // provided y the used ClientBuilder as well.
            m_apiClient = new ApiClient((clientConfig) -> {
                return (null != sslSocketFactory) ? clientConfig.connectorProvider(new HttpUrlConnectorProvider().connectionFactory((url) -> {
                    final HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();

                    if (httpConnection instanceof HttpsURLConnection) {
                        ((HttpsURLConnection) httpConnection).setSSLSocketFactory(sslSocketFactory);
                    }

                    return httpConnection;
                })) : clientConfig;
            }, (clientBuilder) -> {
                return (null != sslContext) ? clientBuilder.sslContext(sslContext) : clientBuilder;
            });
        }

        implConfigure(imageConverterClientConfig);
    }

    /**
     * @return
     */
    ImageConverterApi acquire() {
        final ApiClient apiClient = m_apiClient;

        if (null != apiClient) {
            final Credentials credentials = m_credentials;

            if (null != credentials) {
                apiClient.setUsername(credentials.getLogin());
                apiClient.setPassword(credentials.getPassword());
            }

            return new ImageConverterApi(m_apiClient);
        }

        return null;
    }

    /**
     * @param apiInstance
     */
    public void release(ImageConverterApi apiInstance) {
        // nothing to be done here
    }

    /**
     * @param imageConverterClientConfig
     */
    public void updateConfiguration(@NonNull final ImageConverterClientConfig imageConverterClientConfig) {
        implConfigure(imageConverterClientConfig);
    }

    /**
     * @param credentials
     */
    public void setCredentials(@NonNull final Credentials credentials) {
        m_credentials = credentials;
    }

    // - Implementation --------------------------------------------------------

    private void implConfigure(@NonNull final ImageConverterClientConfig imageConverterClientConfig) {
        final ApiClient apiClient = m_apiClient;

        if (null != apiClient) {
            final URL baseURL = imageConverterClientConfig.getImageConverterServerURL();

            if (null != baseURL) {
                apiClient.setBasePath(baseURL.toString());
            }

            apiClient.setTempFolderPath(imageConverterClientConfig.getSpoolDir().getAbsolutePath());
            apiClient.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS);
            apiClient.setReadTimeout(DEFAULT_READ_TIMEOUT_MILLIS);
        }
    }

    // - Members ---------------------------------------------------------------

    volatile private ApiClient m_apiClient;

    volatile private Credentials m_credentials;
}
