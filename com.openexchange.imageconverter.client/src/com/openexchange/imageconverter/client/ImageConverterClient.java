/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.client;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.annotation.Nullable;
import com.openexchange.auth.Credentials;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.exception.OXException;
import com.openexchange.imageconverter.api.IImageClient;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imageconverter.client.generated.invoker.ApiException;
import dev.failsafe.Failsafe;
import dev.failsafe.RetryPolicy;

/**
 * {@link ImageConverterClient}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterClient implements IImageClient {

    final private static int FILE_DELETE_ATTEMPTS = 5;

    /**
     * Private Ctor: not to be used!
     * Initializes a new {@link ImageConverterClient}.
     */
    @SuppressWarnings("unused")
    private ImageConverterClient() {
        super();

        m_clientConfig = null;
        m_remoteValidator = null;
        m_apiProvider = null;
    }

    /**
     * Initializes a new {@link ImageConverterClient}.
     *
     * @param imageConverterClientConfig
     * @throws OXException
     */
    public ImageConverterClient(
        @NonNull final ConfigurationService configurationService,
        @NonNull final SSLContext sslContext,
        @NonNull final SSLSocketFactory sslSocketFactory) throws OXException {

        super();

        m_clientConfig = new ImageConverterClientConfig(configurationService);
        m_apiProvider = new ImageConverterApiProvider(m_clientConfig, sslSocketFactory, sslContext);
        m_remoteValidator = new ImageConverterRemoteValidator(m_clientConfig, m_apiProvider);
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    @Override
    public void reloadConfiguration(final ConfigurationService configService) {
        m_clientConfig.reloadConfiguration(configService);

        m_apiProvider.updateConfiguration(m_clientConfig);
        m_remoteValidator.updateRemoteURL(m_clientConfig.getImageConverterServerURL());
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    @Override
    public Interests getInterests() {
        return m_clientConfig.getInterests();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() {
        m_clientConfig.shutdown();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImage(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public InputStream getImage(final String imageKey, final String requestFormat, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        InputStream ret = null;

        LOG.trace("IC client #getImage called (key: {}, requested: {})", imageKey, requestFormat);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) {
                    File tmpResponseFile = null;

                    try {
                        if (null != (tmpResponseFile = api.getImage(imageKey, requestFormat, implGetContext(context)))) {
                            ret = new ByteArrayInputStream(FileUtils.readFileToByteArray(tmpResponseFile));
                        }
                    } catch (ApiException | ProcessingException e) {
                        implHandleAPIException("Error in remote calling ImageConverter#getImage", e);
                    } catch (final Exception e) {
                        throw new ImageConverterException("Error in remote calling ImageConverter#getImage", e);
                    } finally {
                        deleteTempFile(tmpResponseFile);
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getMetadata(java.lang.String, java.lang.String[])
     */
    @Override
    public IMetadata getMetadata(final String imageKey, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        IMetadata ret = null;

        LOG.trace("IC client #getMetadata called (key: {})", imageKey);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey)) {
                    File tmpResponseFile = null;

                    try {
                        final var fileResponse = api.getMetadataWithHttpInfo(imageKey, implGetContext(context));

                        if ((null != fileResponse) && (null != (tmpResponseFile = fileResponse.getData()))) {
                            try (final InputStream fileInputStm = FileUtils.openInputStream(tmpResponseFile);
                                final var metadataImage = implCreateMetadataImage(fileInputStm, fileResponse.getHeaders())) {

                                if (null != metadataImage) {
                                    ret = metadataImage.getMetadata();
                                }
                            }
                        }
                    } catch (ApiException | ProcessingException e) {
                        implHandleAPIException("Error in remote calling ImageConverter#getMetadata", e);
                    } catch (final Exception e) {
                        throw new ImageConverterException("Error in remote calling ImageConverter#getMetadata", e);
                    } finally {
                        deleteTempFile(tmpResponseFile);
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImageAndMetadata(java.lang.String, java.lang.String, java.lang.String[])
     */
    @Override
    public MetadataImage getImageAndMetadata(final String imageKey, final String requestFormat, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        MetadataImage ret = null;

        LOG.trace("IC client #getImageAndMetadata called (key: {}, requested: {})", imageKey, requestFormat);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) {
                    File tmpResponseFile = null;

                    try {
                        final var fileResponse = api.getImageAndMetadataWithHttpInfo(imageKey, requestFormat, implGetContext(context));

                        if ((null != fileResponse) && (null != (tmpResponseFile = fileResponse.getData()))) {
                            try (final InputStream fileInputStm = FileUtils.openInputStream(tmpResponseFile)) {
                                ret = implCreateMetadataImage(fileInputStm, fileResponse.getHeaders());
                            }
                        }
                    } catch (ApiException | ProcessingException e) {
                        implHandleAPIException("Error in remote calling ImageConverter#getImageAndMetadata", e);
                    } catch (final Exception e) {
                        throw new ImageConverterException("Error in remote calling ImageConverter#getImageAndMetadata", e);
                    } finally {
                        deleteTempFile(tmpResponseFile);
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheImage(java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public void cacheImage(final String imageKey, final InputStream srcImageStm, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();

        LOG.trace("IC client #cacheImage called (key: {})", imageKey);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey) && (null != srcImageStm)) {
                    final var tmpInputFile = implCreateTempFile("icci", srcImageStm);

                    if (null != tmpInputFile) {
                        try {
                            api.cacheImage(imageKey, tmpInputFile, implGetContext(context));
                        } catch (ApiException | ProcessingException e) {
                            implHandleAPIException("Error in remote calling ImageConverter#cacheImage", e);
                        } finally {
                            deleteTempFile(tmpInputFile);
                        }
                    } else {
                        LOG.warn("IC client InputStream in call to ImageConverter#cacheImage is empty (key: {})", imageKey);
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheAndGetImage(java.lang.String, java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public InputStream cacheAndGetImage(final String imageKey, final String requestFormat, final InputStream srcImageStm, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        InputStream ret = null;

        LOG.trace("IC client #cacheAndGetImage called (key: {}, requested: {})", imageKey, requestFormat);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) {
                    if (null != srcImageStm) {
                        final var tmpInputFile = implCreateTempFile("icci", srcImageStm);

                        if (null != tmpInputFile) {
                            File tmpResponseFile = null;

                            try {
                                if (null != (tmpResponseFile = api.cacheAndGetImage(imageKey, requestFormat, tmpInputFile, implGetContext(context)))) {
                                    ret = new ByteArrayInputStream(FileUtils.readFileToByteArray(tmpResponseFile));
                                }
                            } catch (ApiException | ProcessingException e) {
                                implHandleAPIException("Error in remote calling ImageConverter#cacheAndGetImage", e);
                            } catch (final Exception e) {
                                throw new ImageConverterException("Error in remote calling ImageConverter#cacheAndGetImage", e);
                            } finally {
                                deleteTempFile(tmpInputFile);
                                deleteTempFile(tmpResponseFile);
                            }
                        } else {
                            LOG.warn("IC client InputStream in call to ImageConverter#cacheAndGetImage is empty (key: {})", imageKey);
                        }
                    } else {
                        ret = getImage(imageKey, requestFormat, context);
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheAndGetImageAndMetadata(java.lang.String, java.lang.String, java.io.InputStream, java.lang.String[])
     */
    @Override
    public MetadataImage cacheAndGetImageAndMetadata(final String imageKey, final String requestFormat, final InputStream srcImageStm, final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        MetadataImage ret = null;

        LOG.trace("IC client #cacheAndGetImageAndMetadata called (key: {}, requested: {})", imageKey, requestFormat);

        if (null != api) {
            try {
                if (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) {
                    if (null != srcImageStm) {
                        final var tmpInputFile = implCreateTempFile("icci", srcImageStm);

                        if (null != tmpInputFile) {
                            File tmpResponseFile = null;

                            try {
                                final var fileResponse = api.cacheAndGetImageAndMetadataWithHttpInfo(imageKey, requestFormat, tmpInputFile, implGetContext(context));

                                if ((null != fileResponse) && (null != (tmpResponseFile = fileResponse.getData()))) {
                                    try (final InputStream fileInputStm = FileUtils.openInputStream(tmpResponseFile)) {
                                        ret = implCreateMetadataImage(fileInputStm, fileResponse.getHeaders());
                                    }
                                }
                            } catch (ApiException | ProcessingException e) {
                                implHandleAPIException("Error in remote calling ImageConverter#cacheAndGetImageAndMetadata", e);
                            } catch (final Exception e) {
                                throw new ImageConverterException("Error in remote calling ImageConverter#cacheAndGetImageAndMetadata", e);
                            } finally {
                                deleteTempFile(tmpInputFile);
                                deleteTempFile(tmpResponseFile);
                            }
                        } else {
                            LOG.warn("IC client InputStream in call to ImageConverter#cacheAndGetImageAndMetadata is empty (key: {})", imageKey);
                        }
                    }
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImagesByContext(java.lang.String)
     */
    @Override
    public void clearImages(final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();

        LOG.trace("IC client #clearImages called");

        if (null != api) {
            try {
                api.clearImages(implGetContext(context));
            } catch (ApiException | ProcessingException e) {
                implHandleAPIException("Error in remote calling ImageConverter#clearImages with context: " + context, e);
            } finally {
                m_apiProvider.release(api);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImagesByKey(java.lang.String)
     */

    @Override
    public void clearImagesByKey(final String imageKey) throws ImageConverterException {
        final var api = m_apiProvider.acquire();

        LOG.trace("IC client #clearImagesByKey called (key: {})", imageKey);

        if (null != api) {
            try {
                api.clearImagesByKey(imageKey);
            } catch (ApiException | ProcessingException e) {
                implHandleAPIException("Error in remote calling ImageConverter#clearImagesByKey", e);
            } finally {
                m_apiProvider.release(api);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeyCountByContext(java.lang.String)
     */
    @Override
    public long getKeyCount(final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        var ret = 0L;

        LOG.trace("IC client #getKeyCount called");

        if (null != api) {
            try {
                ret = api.getKeyCount(implGetContext(context)).getLong("count");
            } catch (ApiException | ProcessingException | JSONException e) {
                implHandleAPIException("Error in remote calling ImageConverter#getKeyCount with context: " + context, e);
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeys()
     */
    @Override
    public String[] getKeys(final String... context) throws ImageConverterException {
        final var api = m_apiProvider.acquire();
        String[] ret = {};

        LOG.trace("IC client #getKeys called");

        if (null != api) {
            try {
                final var keyList = api.getKeys(implGetContext(context)).getJSONArray("keys").asList();
                ret = keyList.toArray(new String[keyList.size()]);
            } catch (ApiException | ProcessingException | JSONException e) {
                implHandleAPIException("Error in remote calling ImageConverter#getKeys with context: " + context, e);
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getTotalImagesSize(java.lang.String[])
     */
    @Override
    public long getTotalImagesSize(final String... context) throws ImageConverterException {
        // Dummy implementation, no HTTP Api method available and needed
        return 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageClient#status()
     */
    @Override
    public String status() throws OXException {
        final var api = m_apiProvider.acquire();
        String ret = null;

        LOG.trace("IC client #status called");

        if (null != api) {
            try {
                ret = api.status("false");
            } catch (final ApiException e) {
                final var ice = implWrapAPIException("Error in remote calling ImageConverter#status", e, e.getCode());
                if (ice != null) {
                    throw OXException.general(ice.getMessage(), ice);
                }
            } finally {
                m_apiProvider.release(api);
            }
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageClient#isConnected()
     */
    @Override
    public boolean isConnected() {
        return m_remoteValidator.isConnected(false);
    }

    /**
     *
     */
    @Override
    public void setCredentials(final Credentials credentials) {
        if (null != credentials) {
            m_apiProvider.setCredentials(credentials);
        }
    }

    // - public interface ------------------------------------------------------

    /**
     * @return
     */
    @NonNull public ImageConverterClientConfig getConfig() {
        return m_clientConfig;
    }

    /**
     * @return
     */
    public boolean isRemoteServiceAvailable() {
        return m_remoteValidator.isConnected(true);
    }

    // - Implementation --------------------------------------------------------

    private void implHandleAPIException(final String msg, final Exception e) throws ImageConverterException {
        final var ice = implWrapAPIException(msg, e, (e instanceof ApiException) ? ((ApiException) e).getCode() : 503);
        if (ice != null) {
            throw ice;
        }
    }

    private ImageConverterException implWrapAPIException(final String msg, final Exception e, final int statusCode) {
        final var str = """
            """;
        var throwException = true;

        if (null != e) {
            final var cause = Throwables.getRootCause(e);
            final var causeMessage = (null != cause) ? cause.getMessage() : null;

            if (Status.NO_CONTENT.getStatusCode() != statusCode) {
                if ((statusCode == 404) || (statusCode == 503) ||
                    (cause instanceof IOException) || (cause instanceof ConnectException) || (cause instanceof ProcessingException) ||
                    StringUtils.containsIgnoreCase(causeMessage, "terminated")) {

                    m_remoteValidator.connectionLost();

                    LOG.warn(new StringBuilder("IC client lost remote connection (cause: ").
                        append(StringUtils.isBlank(causeMessage) ? "n/a" : causeMessage).
                        append(")").toString());
                }

                return new ImageConverterException(str, e);
            }
            throwException = false;

            if (LOG.isTraceEnabled()) {
                LOG.trace("IC client received a resource NOT_FOUND error from server => proceeding normally");
            }
        }

        if (throwException) {
            return new ImageConverterException(str);
        }

        return null;
    }

    /**
     * @param context
     * @return
     */
    private static String implGetContext(final String... context) {
        return ((isEmpty(context) || isEmpty(context[0])) ? null : context[0]);
    }

    /**
     * @param inputStm
     * @return
     * @throws ImageConverterException
     */
    private File implCreateTempFile(@Nullable final String prefix, @NonNull final InputStream inputStm) throws ImageConverterException {
        File ret = null;

        try {
            ret = File.createTempFile(((null != prefix) ? (prefix +  "_")  : StringUtils.EMPTY) + TEMP_COUNTER.incrementAndGet(), ".tmp",
                m_clientConfig.getSpoolDir());


            ret = File.createTempFile("icclii", ".tmp", m_clientConfig.getSpoolDir());

            if (null != ret) {
                FileUtils.copyInputStreamToFile(inputStm, ret);

                if (ret.length() <= 0) {
                    deleteTempFile(ret);
                    ret = null;
                }
            }
        } catch (final IOException e) {
            throw new ImageConverterException("Error whilee creating temp. file", e);
        }

        return ret;
    }

    protected static void deleteTempFile(@Nullable final File fileToDelete) {
        if (null != fileToDelete) {
            Failsafe.with(RETRY_POLICY_DELETE_FILE).
            onFailure(event -> LOG.error("IC client was not able to delete temp. file after {}/{} attempts within {}ms: {}",
                event.getAttemptCount(), FILE_DELETE_ATTEMPTS,
                event.getElapsedTime().toMillis(),
                fileToDelete.getAbsolutePath(), event.getException())).
            onSuccess(event -> {
                final var attemptCount = event.getAttemptCount();

                if (attemptCount > 1) {
                    LOG.warn("IC client deleted temp. file after {} attempts within {}ms: {}",
                        attemptCount, event.getElapsedTime().toMillis(), fileToDelete.getAbsolutePath());
                }
            }).
            run(() -> {
                if (fileToDelete.exists()) {
                    if (!fileToDelete.canWrite()) {
                        throw new IOException("IC client can not delete write protected temp. file: " + fileToDelete.getAbsolutePath());
                    }
                    FileUtils.forceDelete(fileToDelete);

                    Thread.yield();

                    if (fileToDelete.exists()) {
                        // throw exception if file still exists
                        throw new IOException("IC client could not delete temp. file althogh no exception has been thrown: " + fileToDelete.getAbsolutePath());
                    }
                }
            });
        }
    }

    /**
     * @param metadataImageMultipartFile
     */
    private static MetadataImage implCreateMetadataImage(
        @NonNull final InputStream metadataImageMultipartInputStm,
        @Nullable final Map<String, List<String>> headers) throws IOException, IndexOutOfBoundsException {

        final var contentTypeHeaderList = (null != headers) ? headers.get("Content-Type") : null;
        final var contentType = ((null != contentTypeHeaderList) && !contentTypeHeaderList.isEmpty()) ? contentTypeHeaderList.get(0) : null;
        var boundaryStartPos = -1;
        MetadataImage ret = null;

        if ((null != contentType) && contentType.startsWith("multipart/") &&
            ((boundaryStartPos = contentType.lastIndexOf("boundary")) > -1)) {

            final var boundary = (contentType.substring(boundaryStartPos).split("="))[1].trim();
            final var multipartStream = new MultipartStream(metadataImageMultipartInputStm, boundary.getBytes(), 8192, null);
            InputStream imageInputStm = null;
            IMetadata metadata = null;

            if (multipartStream.skipPreamble()) {
                do {
                    final var partHeaders = multipartStream.readHeaders();

                    try (final var bodyOutputStm = new ByteArrayOutputStream()) {
                        if (multipartStream.readBodyData(bodyOutputStm) > 0) {
                            if (partHeaders.contains("application/octet-stream")) {
                                imageInputStm = new ByteArrayInputStream(bodyOutputStm.toByteArray());
                            } else if (partHeaders.contains("application/json")) {
                                final var metadataReader = Services.optService(IMetadataReader.class);

                                if (null != metadataReader) {
                                    try {
                                        metadata = metadataReader.readMetadata(new JSONObject(IOUtils.toString(new ByteArrayInputStream(bodyOutputStm.toByteArray()))));
                                    } catch (final Exception e) {
                                        LOG.warn("IC client received Exception", e);
                                    }
                                }
                            }
                        }
                    }
                } while (multipartStream.readBoundary());

                ret = new MetadataImage(imageInputStm, metadata);
            }
        }

        return ret;
    }

    // -- Statics --------------------------------------------------------------

    final static Logger LOG = LoggerFactory.getLogger(ImageConverterClient.class);

    final private static AtomicLong TEMP_COUNTER = new AtomicLong(0);


    /**
     *
     */
    final private static RetryPolicy<Object> RETRY_POLICY_DELETE_FILE = RetryPolicy.builder().
        withMaxAttempts(FILE_DELETE_ATTEMPTS).
        withBackoff(10, 100, ChronoUnit.MILLIS).
        build();

    // - Members ---------------------------------------------------------------

    final private ImageConverterClientConfig m_clientConfig;

    final private ImageConverterRemoteValidator m_remoteValidator;

    final private ImageConverterApiProvider m_apiProvider;
}
