# Helm Chart core-imageconverter
This Helm Chart deploys ImageConverter service core in a kubernetes cluster.

## Introduction
This Chart includes the following components:

* ImageConverter application container to deploy in a kubernetes cluster.

## Requirements
Requires Kubernetes v1.19+

## Dependencies
This section will provide details about specific requirements in addition to this Helm Chart.

## Pushing to registry
From wihtin ${PROJECT_DIR}/helm/core-imageconverter directory:

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm push . ox-documents-registry
```

## Test installation
Run a test against a cluster deployment:

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm install --dry-run --debug --generate-name --version [VERSION] ox-documents-registry/core-imageconverter
```

## Installing the chart
Install the Chart with the release name 'alice':

```shell
helm repo add ox-documents-registry https://registry.open-xchange.com/chartrepo/documents
helm repo update
helm install alice --version [VERSION] ox-documents-registry/core-imageconverter [-f path/to/values_with_credentials.yaml]
```

### Configuration

## Global Configuration
| Parameter                                      | Description                                                                                              | Default                                                                                  |
|------------------------------------------------|----------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
| `defaultRegistry`                              | The image registry                                                                                       | `registry.open-xchange.com`                                                              |
| `image.repository`                             | The image repository                                                                                     | `core-imageconverter`                                                                    |
| `image.tag`                                    | The image tag                                                                                            | ``                                                                                       |
| `image.pullPolicy`                             | The imagePullPolicy for the deployment                                                                   | `IfNotPresent`                                                                           |
| `imagePullSecrets`                             | List of references to secrets for image registries                                                       | `[]`                                                                                     |
| `ingress.enabled`                              | Make ImageConverter service reachable from outside of cluster                                            | `false`                                                                                  |
| `ingress.controller`                           | The type of controller to use, possible values are `nginx` and `traefik`                                 | `nginx`                                                                                  |
| `ingress.hosts`                                | The list of hosts the service can be reached from. Set to empty to allow from everywhere.                | `[]`                                                                                     |
| `ingress.hosts.host`                           | The host the service can be reached from                                                                 | `chart-example.local`                                                                    |
| `mysql.host`                                   | The database connection host. Uses the global value if not set.                                          | `3306`                                                                                   |
| `mysql.port`                                   | The database connection port. Uses the global value if not set.                                          | `3306`                                                                                   |
| `mysql.database`                               | The database connection schema. Uses the global value if not set.                                        | ``                                                                                       |
| `mysql.auth.user`                              | The database connection user. Uses the global value if not set.                                          | ``                                                                                       |
| `mysql.auth.password`                          | The database connection password. Uses the global value if not set.                                      | ``                                                                                       |
| `imageconverter.targetFormats`                 | Specifies the list of target image formats to create for each key                                        | `[auto:200x150, auto:200x150~cover, auto:800x800, auto:1920x1080, auto:1920x1080~cover]` |
| `imageconverter.imageUserComment`              | Specifies the user comment to write into processed target images                                         | `OX_IC`                                                                                  |
| `imageconverter.useErrorDir`                   | Specifies if an error directory should be used to collect source images in case of an error              | `false`                                                                                  |
| `imageconverter.processingTimeoutSeconds`      | Specifies the maximum time in seconds an image will be processed                                         | `20`                                                                                     |
| `imageconverter.maxQueueLength`                | Specifies the maximum number of parallel requests than can be processed                                  | `512`                                                                                    |
| `imageconverter.maxAsyncQueueLengthPercentage` | Specifies the percentage of maxQueueLength keys that can be asynchronously processed in the background   | `95`                                                                                     |
| `imageconverter.cache.maxEntries`              | The maximum number of cache key entries. Use -1 for unlimited.                                           | `250000`                                                                                 |
| `imageconverter.cache.maxSizeMegaBytes`        | The maximum size of all cache key entries combined. Use -1 for unlimited.                                | `-1`                                                                                     |
| `imageconverter.cache.maxLifetimeSeconds`      | The maximum age in seconds of a cache key entry before it gets removed. Use -1 for unlimited.            | `2592000`                                                                                |
| `imageconverter.cache.cleanupPeriodSeconds`    | The period in seconds after which the next cache cleanup will be performed                               | `300`                                                                                    |
| `objectCache.mysql.host`                       | The CacheService database connection host. Uses the parent value if not set.                             | ``                                                                                       |
| `objectCache.mysql.port`                       | The CacheService database connection port. Uses the parent value if not set.                             | `3306`                                                                                   |
| `objectCache.mysql.database`                   | The CacheService database connection schema. Uses the parent value if not set.                           | ``                                                                                       |
| `objectCache.mysql.auth.user`                  | The CacheService database connection user. Uses the parent value if not set.                             | ``                                                                                       |
| `objectCache.mysql.auth.password`              | The CacheService database connection password. Uses the parent value if not set.                         | ``                                                                                       |
| `objectCache.mysql.properties`                 | The optional CacheService database connection properties to pass to the database drivers.                | `[]`                                                                                     |
| `objectCache.fileStore`                        | The numeric id of an already configured middleware FileStore if to be used (DEPRECATED)                  | ``                                                                                       |
| `objectCache.s3ObjectStores`                   | The list of S3 object stores to use                                                                      | `[]`                                                                                     |
| `objectCache.s3ObjectStores.id`                | The numeric id of the current S3 based object store that shouldn't be changed once assigned              | ``                                                                                       |
| `objectCache.s3ObjectStores.endpoint`          | The endpoint URL of the current S3 object store                                                          | ``                                                                                       |
| `objectCache.s3ObjectStores.region`            | The region of the current S3 object store                                                                | `eu-central-1`                                                                           |
| `objectCache.s3ObjectStores.bucketName`        | The bucket name of the current S3 object store                                                           | `objectcache`                                                                            |
| `objectCache.s3ObjectStores.accessKey`         | The access key of the current S3 object store                                                            | ``                                                                                       |
| `objectCache.s3ObjectStores.secretKey`         | The secret key of the current S3 object store                                                            | ``                                                                                       |
| `objectCache.sproxydObjectStores`              | The list of SproxyD object stores to use                                                                 | `[]`                                                                                     |
| `objectCache.sproxydObjectStores.id`           | The numeric id of the current SproxyD based object store that shouldn't be changed once assigned         | ``                                                                                       |
| `objectCache.sproxydObjectStores.endpoint`     | The endpoint URL of the current SproxyD based object store                                               | ``                                                                                       |
| `objectCache.sproxydObjectStores.path`         | The path where to store objects in the current SproxyD based object store                                | `proxyd/objectcache`                                                                     |
| `persistence.enabled`                          | Specifies if cluster volumes are mounted by container. Using emptyDir Volumes when false.                | `false`                                                                                  |
| `env`                                          | Configuration properties passed to the service via environment variables                                 | `[]`                                                                                     |
