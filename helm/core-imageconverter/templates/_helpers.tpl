{{/*
Create ic-configmap name.
*/}}
{{- define "core-imageconverter.configmap" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "ic-configmap" }}
{{- end -}}

{{/*
Create spoolDir name.
*/}}
{{- define "core-imageconverter.spoolDir" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "spooldir" }}
{{- end -}}

{{/*
Create errorDir name.
*/}}
{{- define "core-imageconverter.errorDir" -}}
  {{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "errordir" }}
{{- end -}}

{{/*
Create objectCache name.
*/}}
{{- define "core-imageconverter.objectCacheName" -}}
  {{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "objectache" }}
{{- end -}}

{{/*
ObjectCache path
*/}}
{{- define "core-imageconverter.objectCache.path" -}}
{{- printf "/var/opt/open-xchange/objectcache" }}
{{- end -}}

{{/*
Definition of ObjectStoreIds for the ObjectCache
*/}}
{{- define "core-imageconverter.objectCache.objectStoreId" -}}
{{- if .Values.objectCache.fileStore }}
value: {{ .Values.objectCache.fileStore | quote }}
{{- else if and (not .Values.objectCache.s3ObjectStores) (not .Values.objectCache.sproxydObjectStores) }}
valueFrom:
  configMapKeyRef:
      name: {{ include "core-imageconverter.configmap" . }}
      key: filestore
      optional: true
{{- else }}
value:
{{- end }}
{{- end -}}

{{/*
Create envVars secret.
*/}}
{{- define "core-imageconverter.envVars" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "envvars" }}
{{- end -}}

{{/*
KeyStore
*/}}
{{- define "core-imageconverter.keystore" -}}
{{- printf "%s-%s" (include "ox-common.names.fullname" . | trunc 53 | trimSuffix "-") "keystore" }}
{{- end -}}
