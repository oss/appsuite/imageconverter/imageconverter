/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imagemetadata.impl;

import java.awt.Dimension;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.drew.imaging.ImageMetadataReader;
import com.openexchange.annotation.NonNull;
import com.openexchange.annotation.Nullable;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.MetadataException;
import com.openexchange.imageconverter.api.MetadataGroup;
import com.openexchange.imageconverter.api.MetadataKey;
import com.openexchange.imagetransformation.ImageMetadata;

/**
 * {@link MetadataReaderImpl}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class MetadataReaderImpl implements IMetadataReader {

    /**
     * Initializes a new {@link MetadataReaderImpl}.
     */
    public MetadataReaderImpl() {
        super();
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.ExifService#readExifData(java.io.InputStream)
     */
    @Override
    public IMetadata readMetadata(InputStream mediaInputStm)  throws MetadataException {
        MetadataImpl ret = null;

        if (null == mediaInputStm) {
            throw new MetadataException(new MetadataException("Argument must not be null"));
        }

        try {
            ret = new MetadataImpl(ImageMetadataReader.readMetadata(new BufferedInputStream(mediaInputStm)));
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            throw new MetadataException(e);
        }

        return implUpdateMetadataHeader(ret, mediaInputStm);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.ExifService#readExifData(java.io.File)
     */
    @Override
    public IMetadata readMetadata(File mediaInputFile) throws MetadataException {
        MetadataImpl metadata = null;

        if (null == mediaInputFile) {
            throw new MetadataException(new MetadataException("Argument must not be null"));
        }

        try {
            metadata = new MetadataImpl(ImageMetadataReader.readMetadata(mediaInputFile));
        } catch (@SuppressWarnings("unused") Throwable e) {
            ExceptionUtils.handleThrowable(e);
            // ok, file format may not support metadata => create default object
            metadata = new MetadataImpl();
        }

        try (final InputStream imageInputStm = FileUtils.openInputStream(mediaInputFile)) {
            return implUpdateMetadataHeader(metadata, imageInputStm);
        } catch (IOException e) {
            LOG.trace("IC server caught Exception", e);
        }

        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.api.ExifService#readExifData(org.json.JSONObject)
     */
    @Override
    public IMetadata readMetadata(JSONObject jsonMetadata) throws MetadataException {
        if (null == jsonMetadata) {
            throw new MetadataException(new MetadataException("Argument must not be null"));
        }

        return implUpdateMetadataHeader(new MetadataImpl(jsonMetadata), null);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param metadata
     * @return
     */
    protected static MetadataImpl implUpdateMetadataHeader(@NonNull final MetadataImpl metadata, @Nullable final InputStream imageInputStm) {
        final MetadataGroupImpl headerGroup = (MetadataGroupImpl) metadata.getMetadataGroup(MetadataGroup.HEADER);

        if ((null == headerGroup) ||
            (headerGroup.getDataValue(MetadataKey.HEADER_PIXEL_WIDTH) == null) ||
            (headerGroup.getDataValue(MetadataKey.HEADER_PIXEL_HEIGHT) == null) ||
            (headerGroup.getDataValue(MetadataKey.HEADER_FORMAT_NAME) == null)) {

            Integer pixelWidth = null;
            Integer pixelHeight = null;
            String imageFormatName = null;

            if (null != imageInputStm) {
                try {
                    final ImageMetadata imageMetadata = MetadataUtils.getImageInformation(imageInputStm);
                    final Dimension dimension = imageMetadata.getDimension();

                    pixelWidth = Integer.valueOf(dimension.width);
                    pixelHeight = Integer.valueOf(dimension.height);
                    imageFormatName = imageMetadata.getFormatName();
                } catch (IOException e) {
                    LOG.trace("IC server caught Exception", e);
                }
            }

            final Map<MetadataKey, Object> headerMap = new HashMap<>();

            headerMap.put(MetadataKey.HEADER_PIXEL_WIDTH, (null != pixelWidth) ? pixelWidth : Integer.valueOf(-1));
            headerMap.put(MetadataKey.HEADER_PIXEL_HEIGHT, (null != pixelHeight) ? pixelHeight : Integer.valueOf(-1));
            headerMap.put(MetadataKey.HEADER_FORMAT_NAME, (null != imageFormatName) ? imageFormatName : "png");

            metadata.getOrCreateMetadataGroup(MetadataGroup.HEADER).addMap(headerMap);
        }

        return metadata;
    }

    // Static members ----------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(MetadataReaderImpl.class);
}
