@Library('pipeline-library') _

import com.openexchange.jenkins.Trigger

pipeline {
    agent {
        kubernetes {
            label "imageconverter-${UUID.randomUUID().toString()}"
            defaultContainer 'jnlp'
            yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: gradle
    image: registry-proxy.sre.cloud.oxoe.io/library/gradle:8.7-jdk21
    command:
    - cat
    tty: true
"""
        }
    }
    options {
        buildDiscarder(logRotator(daysToKeepStr: '30'))
        checkoutToSubdirectory('imageconverter')
        disableConcurrentBuilds()
        gitLabConnection('Gitlab')
        gitlabBuilds(builds: ['Integration build', 'Configuration documentation'])
    }
    stages {
        stage('Integration build') {
            when {
                anyOf {
                    branch 'main'
                    branch 'stable-*'
                }
            }
            options {
                gitlabCommitStatus(name: 'Integration build')
            }
            steps {
                script {
                    build job: "appsuite/integration/${env.GIT_BRANCH}", parameters: [text(name: 'OVERWRITE_COMPONENTS', value: '')]
                }
            }
        }
        stage('Configuration documentation') {
            when {
                allOf {
                    anyOf {
                        triggeredBy 'TimerTrigger'
                        triggeredBy 'UserIdCause'
                    }
                    expression { null != version4Documentation(env.BRANCH_NAME) }
                }
            }
            options {
                gitlabCommitStatus(name: 'Configuration documentation')
            }
            steps {
                script {
                    def targetVersion = version4Documentation(env.BRANCH_NAME)
                    def targetDirectory
                    dir('config-doc-processor') {
                        // Need to do some file operation in directory otherwise it is not created.
                        writeFile file: 'properties.json', text: ''
                        targetDirectory = pwd()
                    }
                    container('gradle') {
                        dir('imageconverter/documentation-generic/config') {
                            sh "gradle runConfigDocuProcessor -PtargetDirectory=${targetDirectory} -PtargetVersion=${targetVersion}"
                        }
                    }
                    dir('config-doc-processor') {
                        sshPublisher failOnError: true, publishers: [sshPublisherDesc(configName: 'documentation.open-xchange.com/var/www/documentation', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: "components/imageconverter/config/${targetVersion}", remoteDirectorySDF: false, removePrefix: '', sourceFiles: 'properties.json')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true)]
                    }
                    build job: 'middleware/propertyDocumentationUI/master', parameters: [string(name: 'targetVersion', value: targetVersion), string(name: 'targetDirectory', value: 'imageconverter/config')]
                }
            }
            post {
                success {
                    archiveArtifacts 'config-doc-processor/properties.json'
                }
            }
        }
    }
    post {
        failure {
            emailext attachLog: true,
                body: "${env.BUILD_URL} failed.\n\nFull log at: ${env.BUILD_URL}console\n\n",
                subject: "${env.JOB_NAME} (#${env.BUILD_NUMBER}) - ${currentBuild.result}",
                to: 'cc_file_conversion_viewing-notifications@open-xchange.com'
        }
    }
}

String version4Documentation(String branchName) {
    if ('main' == branchName)
        return branchName
    if (branchName.startsWith('master-'))
        return branchName.substring(7)
    if (branchName.startsWith('release-'))
        return branchName.substring(8)
    return null
}
