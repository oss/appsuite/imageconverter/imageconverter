---
title: Properties 
icon: fa-cog
tags: Properties
layout: properties
properties_file: "properties"
---

The configuration items for the Image Converter.