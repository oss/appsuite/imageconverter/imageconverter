---
title: Detailed software changes
icon: fa fa-info-circle
classes: no-counting
---

This page contains detailed information about software changes.


[Unreleased]
============

[8.34]
======

[8.33]
======


-[8.34]: https://gitlab.open-xchange.com/documents/cacheservice/-/compare/8.33.0...8.34.0
