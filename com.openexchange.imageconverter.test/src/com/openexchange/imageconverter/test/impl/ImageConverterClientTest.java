/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.test.impl;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.OXException;
import com.openexchange.imageconverter.api.IImageClient;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ImageConverterClientTest}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterClientTest implements Callable<Boolean> {

    final private static int TEST_CLIENT_THREAD_COUNT = 1;

    final private static String TEST_IMAGEGROUP_CONTEXT = "ImageConverterClientTest_Context";

    final private static String TEST_IMAGEGROUP_KEY_PNG = "ImageConverterClientTest_Key_PNG";

    final private static String TEST_IMAGEGROUP_KEY_JPG = "ImageConverterClientTest_Key_JPG";

    final private static String TEST_IMAGEGROUP_KEY_TIFF = "ImageConverterClientTest_Key_TIFF";

    /**
     * Initializes a new {@link ImageConverterClientTest}.
     */
    @SuppressWarnings("unused")
    private ImageConverterClientTest() {
        super();
    }

    /**
     * Initializes a new {@link ObjectCacheTest}.
     *
     * @param fileManager
     */
    public ImageConverterClientTest(@NonNull final IImageClient imageConverterClient) {
        super();
        m_imageConverterClient = imageConverterClient;
    }

    // - Callable --------------------------------------------------------------

    /* (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Boolean call() throws Exception {
        final long testStartTimeMillis = System.currentTimeMillis();
        final AtomicBoolean ret = new AtomicBoolean(false);

        implPrepareTest();

        if (isNotEmpty(m_imagePNG) && isNotEmpty(m_imageJPG) && isNotEmpty(m_imageTIFF)) {
            ImageConverterTestUtils.logTest("Starting IImageConverterClient test");

            final List<ImplTest> testList = new ArrayList<>(TEST_CLIENT_THREAD_COUNT);

            for (int i = 0; i < TEST_CLIENT_THREAD_COUNT; ++i) {
                testList.add(new ImplTest(m_instanceId.incrementAndGet()));
            }

            ret.set(true);

            final List<Future<Boolean>> resultList = Executors.newFixedThreadPool(TEST_CLIENT_THREAD_COUNT).invokeAll(testList);

            // wait for all tests to complete and check if all tests succeeded
            // if one threaded test fails, the test itself fails
            for (final Iterator<Future<Boolean>> iter = resultList.iterator(); iter.hasNext(); ) {
                final Future<Boolean> curResult = iter.next();

                // whole test failed if one test failed
                if (!curResult.get()) {
                    ret.set(false);
                }
            }

            if (ret.get()) {
                ImageConverterTestUtils.logTest("Successfully finished IImageConverterClient test: " + (System.currentTimeMillis() - testStartTimeMillis) + "ms");
            } else {
                ImageConverterTestUtils.logTest("Error while executing IImageConverterClient test");
            }
        } else {
            ImageConverterTestUtils.logTest("Could not start IImageConverterClient test due to missing image resources from bundle");
        }

        implFinishTest();

        return new Boolean(ret.get());
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    protected void implPrepareTest() {
        try {
            m_imagePNG = getResourceBuffer("ox-test.png");
            m_imageJPG = getResourceBuffer("ox-test.jpg");
            m_imageTIFF = getResourceBuffer("ox-test.tiff");
        } catch (Exception e) {
            ImageConverterTestUtils.logExcp(e);
        }
    }

    /**
     *
     */
    protected void implFinishTest() {
        m_imagePNG = m_imageJPG = m_imageTIFF = null;
    }
    // - Implementation --------------------------------------------------------

    protected class ImplTest implements Callable<Boolean> {

        public ImplTest(int ctxId) {
            super();
            m_imageKeyPNG = TEST_IMAGEGROUP_KEY_PNG + ctxId;
            m_imageKeyJPG = TEST_IMAGEGROUP_KEY_JPG + ctxId;
            m_imageKeyTIFF = TEST_IMAGEGROUP_KEY_TIFF + ctxId;
            m_context = TEST_IMAGEGROUP_CONTEXT + ctxId;
        }

        /* (non-Javadoc)
         * @see java.util.concurrent.Callable#call()
         */
        @Override
        public Boolean call() throws Exception {
            Boolean ret = Boolean.FALSE;

            try {
                ret = Boolean.valueOf(
                    testStatus() &&
                    testCacheImage() &&
                    testGetImage() &&
                    testGetMetadata() &&
                    testGetImageAndMetadata() &&
                    testNullGetAndCacheImage() &&
                    testCacheAndGetImage() &&
                    testCacheAndGetImageAndMetadata() &&
                    testClearImages() &&
                    testClearImagesByKey() &&
                    testGetKeyCount() &&
                    testGetKeys() &&
                    testMultithreaded() /*&&
                    testRateLimitStatus()*/);
            } catch (Exception e) {
                ImageConverterTestUtils.logExcp(Throwables.getRootCause(e));
            } finally {
                try {
                    m_imageConverterClient.clearImages(m_context);
                } catch (Exception e) {
                    ImageConverterTestUtils.logExcp(Throwables.getRootCause(e));
                }
            }

            return ret;
        }

        // - Single Tests ----------------------------------------------------------

        /**
         * @param servicePNG
         * @throws FileNotFoundException
         * @throws IOException
         * @throws ObjectCacheException
         */
        public boolean testStatus() throws Exception {
            boolean ret = false;

            startSingleTest("status");

            try {
                final String status = m_imageConverterClient.status();
                ret = isNotEmpty(status) && status.contains("57c6b91f-563b-4885-946d-0cc5799e2fdf");
            } catch (OXException e) {
                ImageConverterTestUtils.logExcp(e);
            }

            stopSingleTest("status");

            return ret;
        }

        /**
         * @return
         */
        public boolean testCacheImage() throws Exception {
            startSingleTest("cacheImage");

            boolean ret = implCacheImages(m_context);

            stopSingleTest("cacheImage");

            return ret;
        }

        /**
         * @return
         */
        public boolean testGetImage() throws Exception {
            startSingleTest("getImage");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                ret = implGetImages(m_context);
            }

            stopSingleTest("getImage");

            return ret;
        }

        /**
         * @return
         */
        public boolean testGetImageAndMetadata() throws Exception {
            startSingleTest("getImageAndMetadata");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                ret = implGetImagesAndMetadatas(m_context);
            }

            stopSingleTest("getImageAndMetadata");

            return ret;
        }

        /**
         * @return
         */
        public boolean testGetMetadata() throws Exception {
            startSingleTest("getMetadata");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                try {
                    final IMetadata metadataPNG = m_imageConverterClient.getMetadata(m_imageKeyPNG, m_context);
                    final IMetadata metadataJPG = m_imageConverterClient.getMetadata(m_imageKeyJPG, m_context);
                    final IMetadata metadataTIFF = m_imageConverterClient.getMetadata(m_imageKeyTIFF, m_context);

                    ret = ((null != metadataPNG) && (metadataPNG.getMetadataGroups().size() > 0)) &&
                        ((null != metadataJPG) && (metadataJPG.getMetadataGroups().size() > 0)) &&
                        ((null != metadataTIFF) && (metadataTIFF.getMetadataGroups().size() > 0));
                } catch (Exception e) {
                    ImageConverterTestUtils.logExcp(e);
                }

            }

            stopSingleTest("getMetadata");

            return ret;
        }

        /**
         * @return
         */
        public boolean testCacheAndGetImage() throws Exception {
            startSingleTest("cacheAndGetImage");

            boolean ret = false;

            try (final InputStream inputStmPNG1 = new ByteArrayInputStream(m_imagePNG)) {
                try (final InputStream resultStmPNG1 = m_imageConverterClient.cacheAndGetImage(m_imageKeyPNG, "jpg", inputStmPNG1, m_context)) {
                    try (final InputStream inputStmPNG2 = new ByteArrayInputStream(m_imagePNG)) {
                        try (final InputStream resultStmPNG2 = m_imageConverterClient.cacheAndGetImage(m_imageKeyPNG, "jpg", inputStmPNG2, m_context)) {
                            if ((null != resultStmPNG1) && (null != resultStmPNG2)) {
                                ret = Arrays.equals(IOUtils.toByteArray(resultStmPNG1), IOUtils.toByteArray(resultStmPNG2));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                ImageConverterTestUtils.logExcp(e);
            }

            stopSingleTest("cacheAndGetImage");

            return ret;
        }

        /**
         * @return
         */
        public boolean testCacheAndGetImageAndMetadata() throws Exception {
            startSingleTest("cacheAndGetImageAndMetadata");

            boolean ret = false;

            try (final InputStream inputStmJPG1 = new ByteArrayInputStream(m_imageJPG)) {
                try (final MetadataImage resultMetadataImageJPG1 = m_imageConverterClient.cacheAndGetImageAndMetadata(m_imageKeyJPG, "jpg", inputStmJPG1, m_context)) {
                    try (final InputStream inputStmJPG2 = new ByteArrayInputStream(m_imageJPG)) {
                        try (final MetadataImage resultMetadataImageJPG2 = m_imageConverterClient.cacheAndGetImageAndMetadata(m_imageKeyJPG, "jpg", inputStmJPG2, m_context)) {
                            if ((null != resultMetadataImageJPG1) && (null != resultMetadataImageJPG2)) {
                                ret = Arrays.equals(IOUtils.toByteArray(resultMetadataImageJPG1.getImageInputStream()), IOUtils.toByteArray(resultMetadataImageJPG2.getImageInputStream())) &&
                                    (resultMetadataImageJPG1.getMetadata().getMetadataGroups().size() > 0) &&
                                    (resultMetadataImageJPG1.getMetadata().getMetadataGroups().size() == resultMetadataImageJPG2.getMetadata().getMetadataGroups().size());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                ImageConverterTestUtils.logExcp(e);
            }

            stopSingleTest("cacheAndGetImageAndMetadata");

            return ret;
        }

        /**
         * @return
         */
        public boolean testNullGetAndCacheImage() throws Exception {
            startSingleTest("NullGetAndCacheImageAndMetadata. ERROR output is expected!");

            boolean ret = false;

            if (!implGetImagesAndMetadatas(m_context)) {
                ret = implCacheImages(m_context);
            }

            stopSingleTest("NullGetAndCacheImageAndMetadata. ERROR output was expected!");

            return ret;
        }

        /**
         * @return
         */
        public boolean testClearImages() throws Exception {
            startSingleTest("clearImages");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                ret = implClearImagesByContext(m_context);
            }

            stopSingleTest("clearImages");

            return ret;
        }

        /**
         * @return
         */
        public boolean testClearImagesByKey() throws Exception {
            startSingleTest("clearImagesByKey");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                ret = implClearImagesByContext(m_context);
            }

            stopSingleTest("clearImagesByKey");

            return ret;
        }

        /**
         * @return
         */
        public boolean testGetKeyCount() throws Exception {
            startSingleTest("getKeyCount");

            boolean ret = false;

            if (implClearImagesByContext(m_context) && implCacheImages(m_context)) {
                try {
                    ret = (3 == m_imageConverterClient.getKeyCount(m_context));
                } catch (Exception e) {
                    ImageConverterTestUtils.logExcp(e);
                }
            }

            stopSingleTest("getKeyCount");

            return ret;
        }

        /**
         * @return
         */
        public boolean testGetKeys() throws Exception {
            startSingleTest("getKeys");

            boolean ret = false;

            if (implCacheImages(m_context)) {
                try {
                    final String[] keys = m_imageConverterClient.getKeys(m_context);

                    if ((null != keys) &&
                        !keys[0].equals(keys[1]) &&
                        !keys[0].equals(keys[2]) &&
                        !keys[1].equals(keys[2])) {

                        ret = true;
                    }
                } catch (Exception e) {
                    ImageConverterTestUtils.logExcp(e);
                }
            }

            stopSingleTest("getKeys");

            return ret;
        }

        /**
         * @return
         */
        public boolean testMultithreaded() throws Exception {
            final int clientCount = 50;
            final String testName = new StringBuilder("multithreading, using ").append(clientCount).append(" clients").toString();

            startSingleTest(testName);

            final Collection<Callable<Boolean>> runnables = new ArrayList<>();
            boolean ret = true;

            for (int i = 0; i < clientCount; ++i) {
                final int curIndex = i;

                runnables.add(new Callable<Boolean>() {

                    @Override
                    public Boolean call() throws Exception {
                        final String curImageKey = m_imageKeyJPG + curIndex;
                        final String runnableContext = m_context + curIndex;
                        boolean localRet = false;

                        try (final InputStream inputStmJPG1 = new ByteArrayInputStream(m_imageJPG)) {
                            try (final MetadataImage resultMetadataImageJPG1 = m_imageConverterClient.cacheAndGetImageAndMetadata(curImageKey, "jpg:650x679", inputStmJPG1, runnableContext)) {
                                try (final InputStream inputStmJPG2 = new ByteArrayInputStream(m_imageJPG)) {
                                    try (final MetadataImage resultMetadataImageJPG2 = m_imageConverterClient.cacheAndGetImageAndMetadata(curImageKey, "jpg:649x680", inputStmJPG2, runnableContext)) {
                                        if ((null != resultMetadataImageJPG1) && (null != resultMetadataImageJPG2)) {
                                            if ((Arrays.equals(IOUtils.toByteArray(resultMetadataImageJPG1.getImageInputStream()), IOUtils.toByteArray(resultMetadataImageJPG2.getImageInputStream())) &&
                                                (resultMetadataImageJPG1.getMetadata().getMetadataGroups().size() > 0) &&
                                                (resultMetadataImageJPG1.getMetadata().getMetadataGroups().size() == resultMetadataImageJPG2.getMetadata().getMetadataGroups().size()))) {

                                                localRet = true;
                                            }
                                        } else {
                                            localRet = false;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            ImageConverterTestUtils.logExcp(e);
                        } finally {
                            implClearImagesByContext(runnableContext);
                        }

                        return Boolean.valueOf(localRet);
                    }
                });
            }

            try {
                List<Future<Boolean>> allResult = Executors.newFixedThreadPool(clientCount).invokeAll(runnables);

                for (final Future<Boolean> curFuture : allResult) {
                    if (!curFuture.get().booleanValue()) {
                        ImageConverterTestUtils.logTest("ignoring amgibuity: " + testName);
                    }
                }
            } catch (@SuppressWarnings("unused") InterruptedException | ExecutionException e) {
                ret = false;
            }

            stopSingleTest(testName);

            return ret;
        }

        /**
         * @return
         */
        public boolean testRateLimitStatus() throws Exception {
            final AtomicBoolean rateLimitReached = new AtomicBoolean(false);
            boolean ret = true;
            final int clientCount = 512;
            final String testName = new StringBuilder("rate limit, using ").append(clientCount).append(" clients").toString();

            startSingleTest(testName);

            final Collection<Callable<Boolean>> runnables = new ArrayList<>();

            for (int i = 0; i < clientCount; ++i) {
                final int curIndex = i;

                runnables.add(new Callable<Boolean>() {

                    @Override
                    public Boolean call() throws Exception {
                        try {
                            final String curStatus = m_imageConverterClient.status();
                        } catch (Exception e) {
                            ImageConverterTestUtils.logExcp(e);
                            rateLimitReached.compareAndSet(false, true);
                        }

                        return Boolean.TRUE;
                    }
                });
            }

            try {
                List<Future<Boolean>> allResult = Executors.newFixedThreadPool(clientCount).invokeAll(runnables);

                for (final Future<Boolean> curFuture : allResult) {
                    if (!curFuture.get().booleanValue()) {
                        ImageConverterTestUtils.logTest("ignoring amgibuity: " + testName);
                    }
                }
            } catch (@SuppressWarnings("unused") InterruptedException | ExecutionException e) {
                ret = false;
            }

            stopSingleTest(testName);

            return rateLimitReached.get();
        }

        /**
         * @param testName
         */
        protected synchronized void startSingleTest(@NonNull final String testName) throws Exception {
            implClearImagesByContext(m_context);

            if (m_testTimesMap.containsKey(testName)) {
                ImageConverterTestUtils.logTest("Test ist already started: " + testName);
            } else {
                ImageConverterTestUtils.logTest("Test started: " + testName);
                m_testTimesMap.put(testName, Long.valueOf(System.currentTimeMillis()));
            }
        }

        /**
         * @param testName
         */
        protected synchronized void stopSingleTest(@NonNull final String testName) throws Exception {
            final Long testStartTime = m_testTimesMap.get(testName);

            if (null != testStartTime) {
                ImageConverterTestUtils.logTest(new StringBuilder("Test finished: ").append(testName).append(" (").append(System.currentTimeMillis() - testStartTime.longValue()).append("ms)").toString());
            } else {
                ImageConverterTestUtils.logTest("Test was not started: " + testName);
            }

            implClearImagesByContext(m_context);
        }

        /**
         * @return
         */
        protected boolean implCacheImages(final String context, final String... appendix) {
            final String appendixStr = isNotEmpty(appendix) ? appendix[0] : "";
            boolean ret = false;

            try (final InputStream pngInputStm = new ByteArrayInputStream(m_imagePNG);
                final InputStream jpgInputStm = new ByteArrayInputStream(m_imageJPG);
                final InputStream tiffInputStm = new ByteArrayInputStream(m_imageTIFF)) {

               m_imageConverterClient.cacheImage(m_imageKeyPNG + appendixStr, pngInputStm, context);

               final long keyCount = m_imageConverterClient.getKeyCount();
               final long contextCount = m_imageConverterClient.getKeyCount(context);

               ret = (keyCount > 0) && (contextCount > 0);

               if (ret) {
                   m_imageConverterClient.cacheImage(m_imageKeyJPG + appendixStr, jpgInputStm, context);

                   if (true == (ret = (m_imageConverterClient.getKeyCount(context) > 0))) {
                       m_imageConverterClient.cacheImage(m_imageKeyTIFF +  appendixStr, tiffInputStm, context);
                       ret = (m_imageConverterClient.getKeyCount() > 0) && (m_imageConverterClient.getKeyCount(context) > 0);
                   }
               }
           } catch (Exception e) {
               ImageConverterTestUtils.logExcp(e);
           }

            return ret;
        }

        /**
         * @return
         */
        protected boolean implGetImages(final String context) {
            boolean ret = false;

            try (final InputStream inputStm1 = m_imageConverterClient.getImage(m_imageKeyPNG, "jpg:32x32", context);
                final InputStream inputStm2 = m_imageConverterClient.getImage(m_imageKeyJPG, "jpg:64x1", context);
                final InputStream inputStm3 = m_imageConverterClient.getImage(m_imageKeyTIFF, "jpg:1024x-1", context)) {
               ret = (null != inputStm1) && (IOUtils.toByteArray(inputStm1).length > 0) &&
                     (null != inputStm2) && (IOUtils.toByteArray(inputStm2).length > 0) &&
                     (null != inputStm3) && (IOUtils.toByteArray(inputStm3).length > 0);
           } catch (Exception e) {
               ImageConverterTestUtils.logExcp(e);
           }

            return ret;
        }

        /**
         * @return
         */
        protected boolean implGetImagesAndMetadatas(final String context) throws Exception {
            boolean ret = false;

            try (final MetadataImage metadataImagePNG = m_imageConverterClient.getImageAndMetadata(m_imageKeyPNG, "jpg:32x32", context);
                 final MetadataImage metadataImageJPG = m_imageConverterClient.getImageAndMetadata(m_imageKeyJPG, "jpg:64x1", context);
                 final MetadataImage metadataImageTIFF = m_imageConverterClient.getImageAndMetadata(m_imageKeyTIFF, "jpg:1024x-1", context)) {

                ret = (null != metadataImagePNG) && (IOUtils.toByteArray(metadataImagePNG.getImageInputStream()).length > 0) &&
                      (null != metadataImageJPG) && (IOUtils.toByteArray(metadataImageJPG.getImageInputStream()).length > 0) &&
                      (null != metadataImageTIFF) && (IOUtils.toByteArray(metadataImageTIFF.getImageInputStream()).length > 0);

                if (ret) {
                    @SuppressWarnings("unused") JSONObject jsonMetadataPNG = null;
                    @SuppressWarnings("unused") JSONObject jsonMetadataJPG = null;
                    @SuppressWarnings("unused") JSONObject jsonMetadataTIFF = null;

                    if ((null != metadataImagePNG) && (null != metadataImagePNG.getMetadata())) {
                        jsonMetadataPNG = metadataImagePNG.getMetadata().getJSONObject();
                    }

                    if ((null != metadataImageJPG) && (null != metadataImageJPG.getMetadata())) {
                        jsonMetadataJPG = metadataImageJPG.getMetadata().getJSONObject();
                    }

                    if ((null != metadataImageTIFF) && (null != metadataImageTIFF.getMetadata())) {
                        jsonMetadataTIFF = metadataImageTIFF.getMetadata().getJSONObject();
                    }

                    ret = (null != metadataImagePNG) && (null != metadataImageJPG) && (null != metadataImageTIFF) &&
                        (null != metadataImagePNG.getImageInputStream()) && (null != metadataImagePNG.getMetadata()) &&
                        (null != metadataImageJPG.getImageInputStream()) && (null != metadataImageJPG.getMetadata()) &&
                        (null != metadataImageTIFF.getImageInputStream()) && (null != metadataImageTIFF.getMetadata());
                }
            } catch (Exception e) {
               ImageConverterTestUtils.logExcp(e);
            }

            return ret;
        }

        /**
         * @return
         */

        /**
         * @return
         */
        protected boolean implClearImagesByContext(final String context) throws Exception {
            while (m_imageConverterClient.getKeyCount(context) > 0) {
                m_imageConverterClient.clearImages(context);
                Thread.sleep(100);
            }

            return (0 == m_imageConverterClient.getKeyCount(context));
        }

        // - Members ---------------------------------------------------------------

        protected String m_context = null;

        protected Map<String, Long> m_testTimesMap = new HashMap<>();

        protected String m_imageKeyPNG = null;

        protected String m_imageKeyJPG = null;

        protected String m_imageKeyTIFF = null;
    }

    /**
     * @param filename
     * @return
     * @throws OXException
     */
    protected static byte[] getResourceBuffer(@NonNull String filename) throws OXException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        byte[] resourceBuffer = null;

        if (null == loader) {
            loader = ImageConverterClientTest.class.getClassLoader();
        }

        final URL url = loader.getResource(filename);

        if (null != url) {
            URLConnection connection = null;

            try {
                connection = url.openConnection();

                if (null != connection) {
                    connection.connect();

                    try (InputStream resourceInputStm = connection.getInputStream()) {
                        if (null != resourceInputStm) {
                            resourceBuffer = IOUtils.toByteArray(resourceInputStm);
                        }
                    }
                }
            } catch (IOException e) {
                ImageConverterTestUtils.logExcp(e);
            }
        }

        return resourceBuffer;
    }

    // - Members ---------------------------------------------------------------

    protected IImageClient m_imageConverterClient = null;

    protected byte[] m_imagePNG = null;

    protected byte[] m_imageJPG = null;

    protected byte[] m_imageTIFF = null;

    protected AtomicInteger m_instanceId = new AtomicInteger(0);
}

