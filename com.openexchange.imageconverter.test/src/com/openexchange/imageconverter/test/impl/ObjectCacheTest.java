/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.test.impl;

import static org.junit.Assert.assertTrue;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IReadAccess;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link FileItemTest}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ObjectCacheTest implements Callable<Boolean> {

    final private static boolean RUN_MASS_FILEITEM_CREATION = false;

    /**
     * Initializes a new {@link ObjectCacheTest}.
     */
    @SuppressWarnings("unused")
    private ObjectCacheTest() {
        super();
    }

    /**
     * Initializes a new {@link ObjectCacheTest}.
     * @param fileManager
     */
    public ObjectCacheTest(@NonNull final IObjectCache iObjectCache) {
        m_objectCache = iObjectCache;
    }

    // - Callable --------------------------------------------------------------

    /* (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Boolean call() throws Exception {
        final long testStartTimeMillis = System.currentTimeMillis();

        if ((null == m_objectCache) || !m_objectCache.isValid()) {
            ImageConverterTestUtils.logTest("IObjectCache test cannot be performed since ObjectCache instance is not valid!");
            return Boolean.FALSE;
        }

        try {
            ImageConverterTestUtils.logTest("Starting IObjectCache test");

            // General I/O
            testGeneralIO();

            // Performance single threaded
            singleThreadPerformanceTest();

            // Performance multi threaded
            multiThreadPerformanceTest();

            // Performance mass DB entries
            if (RUN_MASS_FILEITEM_CREATION) {
                massPerformanceTest();
            }

            ImageConverterTestUtils.logTest("Successfully finished IObjectCache test: " + (System.currentTimeMillis() - testStartTimeMillis) + "ms");
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            ImageConverterTestUtils.logTest("Error while executing IObjectCache test. Exception caught: " + Throwables.getRootCause(e).getMessage());
            throw new Exception(e);
        } finally {
            m_objectCache.removeGroup(FILEITEMTEST_GROUP_ID);
        }

        return Boolean.TRUE;
    }

    // - Public API ------------------------------------------------------------

    /**
     * @param m_objectCache
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ObjectCacheException
     */
    public void testGeneralIO() throws FileNotFoundException, IOException, ObjectCacheException, AssertionError {
        ICacheObject cacheObject = null;
        final byte[] testbuffer = { 'T', 'e', 's', 't', 'C', 'o', 'n', 't', 'e', 'n', 't' };
        final byte[] updatedTestbuffer = { 'U', 'p', 'd', 'a', 't', 'e', 'd','T', 'e', 's', 't', 'C', 'o', 'n', 't', 'e', 'n', 't' };

        try {
            // register and check registered custom keys
            final GroupConfig groupConfig = GroupConfig.builder().
                withGroupId(FILEITEMTEST_GROUP_ID).
                withCustomKeys(new String[] {FILEITEMTEST_KEY_NAME}).build();

            m_objectCache.registerGroup(groupConfig);
            assertTrue("Expected registered key " + FILEITEMTEST_KEY_NAME, m_objectCache.hasCustomKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_NAME));

            // invalid initialization => Exception expected
            try {
                cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID);

                try (IReadAccess errorReadAccess = m_objectCache.getReadAccess(cacheObject)) {
                    errorReadAccess.close();
                }
            } catch (ObjectCacheException e) {
                ImageConverterTestUtils.logTest("Exception handler successfully caught ObjectCacheException with message: " + Throwables.getRootCause(e).getMessage());
            }

            // check removal of group and ensure empty group at beginning of test
            m_objectCache.removeGroup(FILEITEMTEST_GROUP_ID);

            // new empty file only
            try (IWriteAccess newEmptyFileWritAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)) {
                // empty file creation
            }
            assertTrue("Creating empty file", null != (cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)));

            // testing contains with result
            assertTrue("ContainsGroup TRUE", m_objectCache.containsGroup(FILEITEMTEST_GROUP_ID));
            assertTrue("ContainsGroup FALSE", !m_objectCache.containsGroup("nogroup"));
            assertTrue("ContainsKey TRUE", m_objectCache.containsKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1));
            assertTrue("ContainsKey FALSE", !m_objectCache.containsKey("nogroup", FILEITEMTEST_KEY_ID_1));
            assertTrue("Contains TRUE", m_objectCache.contains(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID));
            assertTrue("Contains FALSE", !m_objectCache.contains(FILEITEMTEST_GROUP_ID, "nokey", FILEITEMTEST_FILE_ID));

            // remove file
            if (null != (cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID))) {
                assertTrue("Removing empty file", m_objectCache.remove(cacheObject));
                assertTrue("Item count expected to be 0: ", 0 == m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1).length);
                assertTrue("Key count expected to be 0", 0 == m_objectCache.getKeyCount(FILEITEMTEST_GROUP_ID));
            }

            // TRUNCATE
            try (final IWriteAccess writeAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)) {
                if (null != writeAcc) {
                    try (final IReadAccess readAcc = m_objectCache.getReadAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)) {
                       readAcc.close();
                    } catch (ObjectCacheException e) {
                        ImageConverterTestUtils.logTest("ItemReadAccess expected to be null due to not yet written outer access with Exception: " + Throwables.getRootCause(e).getMessage());
                    }

                    assertTrue("ItemWriteAccess expected to be not null", null != writeAcc);

                    writeAcc.setKeyValue(FILEITEMTEST_KEY_NAME, FILEITEMTEST_KEY_VALUE);

                    try (final OutputStream outputStm = writeAcc.getOutputStream()) {
                        outputStm.write(testbuffer);
                    } catch (IOException e) {
                        ImageConverterTestUtils.logTest("ItemWriteAccess IOException writing to CacheObject output stream: " + Throwables.getRootCause(e).getMessage());
                    }

                    try {
                        writeAcc.getKeyValue("InvalidKey");
                    } catch (ObjectCacheException e) {
                        ImageConverterTestUtils.logTest("ItemWriteAccess ObjectCacheException expected when accessing invalid custom key name with exception: " + Throwables.getRootCause(e).getMessage());
                    }
                }
            }

            cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID);
            try (final IReadAccess readAcc = m_objectCache.getReadAccess(cacheObject)) {
                if (null != readAcc) {
                    assertTrue("Read item data into Memory, expecting 'TestContent'",  "TestContent".equals(new String(IOUtils.toByteArray(readAcc.getInputStream()))));

                    try (final IReadAccess readAcc2 = m_objectCache.getReadAccess(cacheObject)) {
                        if (null != readAcc2) {
                            assertTrue("Read item into TMP file , expecting 'TestContent'", "TestContent".equals(new String(IOUtils.toByteArray(readAcc2.getInputStream()))));

                            ImageConverterTestUtils.logTest("ReadProperty CreateDate: " + readAcc2.getCreateDate().toString());

                            ImageConverterTestUtils.logTest("ReadProperty ModificationDate: " + readAcc2.getModificationDate().toString());

                            ImageConverterTestUtils.logTest("ReadProperty Length: " + Long.toString(readAcc2.getLength()));

                            final String keyValue = readAcc2.getKeyValue(FILEITEMTEST_KEY_NAME);
                            assertTrue("Reading testKey1='TestValue2': ", FILEITEMTEST_KEY_VALUE.equals(keyValue));
                        }
                    }
                }
            }

            // check update
            try (final IWriteAccess writeAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)) {
                try (final OutputStream outputStm = writeAcc.getOutputStream()) {
                    outputStm.write(updatedTestbuffer);
                } catch (IOException e) {
                    ImageConverterTestUtils.logTest("ItemWriteAccess IOException writing to CacheObject output stream: " + Throwables.getRootCause(e).getMessage());
                }
            }

            try (final IReadAccess readAcc = m_objectCache.getReadAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID)) {
                try (final InputStream inputStm = readAcc.getInputStream()) {
                    assertTrue("Read item into TMP file , expecting 'UpdatedTestContent'", "UpdatedTestContent".equals(new String(IOUtils.toByteArray(inputStm))));
                    ImageConverterTestUtils.logTest("Update test for existing cache object was successful");
                } catch (IOException e) {
                    ImageConverterTestUtils.logTest("ItemWriteAccess IOException writing to CacheObject output stream: " + Throwables.getRootCause(e).getMessage());
                }
            }

            // check file count after operations
            cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID);
            assertTrue("CacheObject needs to be valid", null != cacheObject);
            assertTrue("FileKey needs to be valid", m_objectCache.containsKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1));
            assertTrue("FileGroup needs to be valid", m_objectCache.containsGroup(FILEITEMTEST_GROUP_ID));
            ImageConverterTestUtils.logTest("Group count is: " + m_objectCache.getGroupCount());
            ImageConverterTestUtils.logTest("Key count is: " + m_objectCache.getKeyCount(FILEITEMTEST_GROUP_ID));

            // test select by properties
            Properties properties = new Properties();
            properties.put(FILEITEMTEST_KEY_NAME, FILEITEMTEST_KEY_VALUE);
            int removedFileCount = m_objectCache.remove(FILEITEMTEST_GROUP_ID, properties);
            ImageConverterTestUtils.logTest("Count of removed entries that match group and properties: " + Integer.toString(removedFileCount));

            // test single file removal
            ICacheObject[] fileElementsAfter = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1);
            ImageConverterTestUtils.logTest("Item count of Key expected to be 0: " + Boolean.toString(0 == fileElementsAfter.length));

            m_objectCache.remove(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1, FILEITEMTEST_FILE_ID);

            fileElementsAfter = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1);
            ImageConverterTestUtils.logTest("Key count expected to be 0: " + Boolean.toString(0 == m_objectCache.getKeyCount(FILEITEMTEST_GROUP_ID)));
            ImageConverterTestUtils.logTest("Item count of Key expected to be 0: " + Boolean.toString(0 == fileElementsAfter.length));

            /* TODO (KA)
            fileElementsAfter = m_objectCache.getFiles(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1);
            ObjectCacheUtils.logTest("Key count expected to be 1: " + Boolean.toString(1 == m_objectCache.getKeyIds(FILEITEMTEST_GROUP_ID).length));

            String[] groupIdsAfter = m_objectCache.getGroupIds();
            ObjectCacheUtils.logTest("Group count expected to be 2: " + Boolean.toString(2 == groupIdsAfter.length));

            ObjectCacheUtils.logTest("Item count of Group expected to be 1: " + Boolean.toString(1 == m_objectCache.getKeyIds(FILEITEMTEST_GROUP_ID).length));
            m_objectCache.removeFile(implGetDefaultFileElement(m_objectCache));
            ObjectCacheUtils.logTest("Item count of Group expected to be 0: " + Boolean.toString(0 == m_objectCache.getKeyIds(FILEITEMTEST_GROUP_ID).length));

            ObjectCacheUtils.logTest("Key count expected to be 0: " + Boolean.toString(m_objectCache.removeFilesByGroupKey(testGroupId_2, FILEITEMTEST_KEY_ID_1) == 0));

            m_objectCache.removeFilesByGroup(FILEITEMTEST_GROUP_ID);
            groupIdsAfter = m_objectCache.getGroupIds();
            ObjectCacheUtils.logTest("Group count expected to be 1: " + Boolean.toString(groupIdsAfter.length == 1));

            m_objectCache.removeFilesByGroupKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1);
            groupIdsAfter = m_objectCache.getGroupIds();
            ObjectCacheUtils.logTest("Group count expected to be 0: " + Boolean.toString(groupIdsAfter.length == 0));
            */

            ImageConverterTestUtils.logTest("General I/O test finished successfully: ");
        } finally {
            m_objectCache.removeKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_1);
        }
    }


    /**
     * Performance
     *
     * @param m_objectCache
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ObjectCacheException
     */
    public void singleThreadPerformanceTest() throws FileNotFoundException, IOException, ObjectCacheException, AssertionError {
        final long cycleCount = 1;
        final long count = 100;
        long curStartTime = 0;
        long duration = 0;

        try {
            for (int cycle = 0; cycle < cycleCount; ++cycle) {
                // write performance (system)
                curStartTime = System.currentTimeMillis();
                for (int i = 0; i < count; ++i) {
                    final File testFile = new File("/tmp", "testfile_" + i);

                    try (final OutputStream outputStm = new BufferedOutputStream(new FileOutputStream(testFile))) {
                        outputStm.write(new byte[] { '1', '2', '3', '4' });
                        outputStm.flush();
                    }
                }
                duration = System.currentTimeMillis() - curStartTime;
                ImageConverterTestUtils.logTest(count + " files created. Write duration (System): " + duration + " (throughput per second: " + (double) count / duration * 1000.0 + ")");

                // write performance (TRUNCATE)
                curStartTime = System.currentTimeMillis();
                for (int i = 0; i < count; ++i) {
                    try (final IWriteAccess wAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_2, FILEITEMTEST_FILE_ID + i);
                         final OutputStream outputStm = wAcc.getOutputStream()) {

                        outputStm.write(new byte[] { '1', '2', '3', '4' });
                        wAcc.setKeyValue(FILEITEMTEST_KEY_NAME, FILEITEMTEST_KEY_VALUE);
                    } catch (IOException e) {
                        ImageConverterTestUtils.logTest("ItemWriteAccess IOException writing to CacheObject output stream: " + Throwables.getRootCause(e).getMessage());
                    }
                }
                duration = System.currentTimeMillis() - curStartTime;
                ImageConverterTestUtils.logTest(count + " files created. Write duration (TRUNCATE): " + duration + " (throughput per second: " + (double) count / duration * 1000.0 + ")");

                // read performance
                final byte[] buffer = new byte[16];
                curStartTime = System.currentTimeMillis();
                for (int i = 0, expectedByteCountWritten = 4; i < count; ++i) {
                    final ICacheObject cacheObject = m_objectCache.get(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_2, FILEITEMTEST_FILE_ID + i);

                    try (final IReadAccess rAcc = m_objectCache.getReadAccess(cacheObject)) {
                        final int byteCountRead = rAcc.getInputStream().read(buffer);

                        if (byteCountRead != expectedByteCountWritten) {
                            ImageConverterTestUtils.logTest(expectedByteCountWritten + " bytes expected to be read. Bytes actually read: " + byteCountRead);
                            break;
                        }

                        rAcc.getKeyValue(FILEITEMTEST_KEY_NAME);
                    }
                }
                duration = System.currentTimeMillis() - curStartTime;
                ImageConverterTestUtils.logTest(count + " files read. Read duration (8 bytes: " + duration + " (throughput per second: " + (double) count / duration * 1000.0 + ")");

                ImageConverterTestUtils.logTest("Singlethreaded test finished successfully");
            }
        } finally {
            m_objectCache.removeKey(FILEITEMTEST_GROUP_ID, FILEITEMTEST_KEY_ID_2);
        }
    }

    /**
     * @throws InterruptedException
     * @throws ObjectCacheException
     */
    public void multiThreadPerformanceTest() throws InterruptedException, ObjectCacheException, AssertionError {
        try {
            final int threadCount = 500;
            final int countPerThread = 20;
            long curStartTime = 0;
            long duration = 0;
            Thread[] threadArray = new Thread[threadCount];

            // Multithreading test (File write)
            curStartTime = System.currentTimeMillis();
            for (int i = 0; i < threadCount; ++i) {
                final int threadNumber = i + 1;

                threadArray[i] = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        final String fileKey = "filebased_file" + threadNumber;

                        for (int n = 1; n <= countPerThread; ++n) {
                            try (final IWriteAccess wAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, fileKey, FILEITEMTEST_FILE_ID + n)) {
                                final File outputFile = wAcc.getOutputFile();
                                FileUtils.write(outputFile, "Test");
                            } catch (Exception e) {
                                ImageConverterTestUtils.logTest("Thread #" + (n + 1) + " received Exception: " + Throwables.getRootCause(e).getMessage());
                            }
                        }
                    }
                });

                threadArray[i].start();
            }
            for (int i = 0; i < threadCount; ++i) {
                threadArray[i].join();
            }
            duration = System.currentTimeMillis() - curStartTime;
            ImageConverterTestUtils.logTest(threadCount + " Threads created with " + countPerThread +  " file based item creations via output file each. Total duration: " + duration + " (throughput per second: " + (double) threadCount * countPerThread / duration * 1000.0 + ")");

            // Multithreading test (Stream write)
            curStartTime = System.currentTimeMillis();
            for (int i = 0; i < threadCount; ++i) {
                final int threadNumber = i + 1;

                threadArray[i] = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        final String fileKey = "filebased_stream" + threadNumber;

                        for (int n = 1; n <= countPerThread; ++n) {
                            try {
                                try (final IWriteAccess wAcc = m_objectCache.getWriteAccess(FILEITEMTEST_GROUP_ID, fileKey, FILEITEMTEST_FILE_ID + n);
                                     final OutputStream outputStm = wAcc.getOutputStream()) {

                                    outputStm.write(new byte[] {1});
                                }
                            } catch (Exception e) {
                                ImageConverterTestUtils.logTest("Thread #" + (n + 1) + " received Exception: " + Throwables.getRootCause(e).getMessage());
                            }
                        }
                    }
                });

                threadArray[i].start();
            }
            for (int i = 0; i < threadCount; ++i) {
                threadArray[i].join();
            }
            duration = System.currentTimeMillis() - curStartTime;
            ImageConverterTestUtils.logTest(threadCount + " Threads created with " + countPerThread +  " file based item creations via output stream each. Total duration: " + duration + " (throughput per second: " + (double) threadCount * countPerThread / duration * 1000.0 + ")");
            ImageConverterTestUtils.logTest("Multithreaded test finished successfully");
        } finally {
            m_objectCache.removeGroup(FILEITEMTEST_GROUP_ID);
        }
    }

    /**
     * @throws InterruptedException
     * @throws ObjectCacheException
     */
    public void massPerformanceTest() throws InterruptedException, ObjectCacheException, AssertionError {
        final long massEntryCount = 10000000;
        final long throughputCount = 100;
        final int threadCount = 100;
        final AtomicLong counter = new AtomicLong();
        final StringBuilder keyNameBuilder = new StringBuilder(128).append(UUID.randomUUID().toString()).append('_');
        Thread threadArray[] = new Thread[threadCount];
        final AtomicLong lastTime = new AtomicLong(System.currentTimeMillis());
        final GroupConfig groupConfig = GroupConfig.builder().
            withGroupId(FILEITEMTEST_MASS_GROUP_ID).
            withCustomKeys(new String[] { FILEITEMTEST_KEY_NAME, FILEITEMTEST_MASS_KEY_NAME }).build();

        m_objectCache.registerGroup(groupConfig);

        for (int i = 0; i < threadCount; ++i) {
            threadArray[i] = new Thread(new Runnable() {

                @Override
                public void run() {
                    long curNumber = 0;

                    while ((curNumber = counter.incrementAndGet()) < massEntryCount) {
                        final String curKey = new StringBuilder(keyNameBuilder).append(curNumber).toString();

                        try (final IWriteAccess wAcc = m_objectCache.getWriteAccess(FILEITEMTEST_MASS_GROUP_ID, FILEITEMTEST_MASSS_KEY_ID, curKey)) {
                            wAcc.setKeyValue(FILEITEMTEST_MASS_KEY_NAME, FILEITEMTEST_MASS_KEY_VALUE);
                        } catch (Exception e) {
                            ImageConverterTestUtils.logTest("Mass test received Exception while creating new entry: " + Throwables.getRootCause(e).getMessage());
                        }

                        if ((curNumber % throughputCount) == 0) {
                            final long curTime = System.currentTimeMillis();
                            final long duration = curTime - lastTime.get();

                            ImageConverterTestUtils.logTest(throughputCount + " files created. Create time: " + duration + "ms (throughput per second: " + (double) throughputCount / duration * 1000.0 + ")");
                            lastTime.set(curTime);
                        }
                    }
                }
            });

            threadArray[i].start();
        }
        for (int i = 0; i < threadCount; ++i) {
            threadArray[i].join();
        }
    }

    // - Members ---------------------------------------------------------------

    protected IObjectCache m_objectCache = null;

    // - Static members --------------------------------------------------------

    final private static String FILEITEMTEST_GROUP_ID = "OXSYS_TestGroup";
    final private static String FILEITEMTEST_MASS_GROUP_ID = "OXSYS_MassTestGroup";
    final private static String FILEITEMTEST_KEY_ID_1 = "TestKey_1";
    final private static String FILEITEMTEST_KEY_ID_2 = "TestKey_2";
    final private static String FILEITEMTEST_MASSS_KEY_ID = "MassTestKey";
    final private static String FILEITEMTEST_FILE_ID = "TestFile";
    final private static String FILEITEMTEST_MASS_FILE_ID = "MassTestFile";
    final private static String FILEITEMTEST_KEY_NAME = "TestKeyName";
    final private static String FILEITEMTEST_MASS_KEY_NAME = "MassTestKeyName";
    final private static String FILEITEMTEST_KEY_VALUE = "TestKeyValue";
    final private static String FILEITEMTEST_MASS_KEY_VALUE = "MassTestKeyValue";
}
