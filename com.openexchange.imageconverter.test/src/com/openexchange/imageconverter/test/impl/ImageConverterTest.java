/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.test.impl;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Throwables;
import com.openexchange.annotation.NonNull;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;

/**
 * {@link ImageConverterTest}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterTest implements Callable<Boolean> {

    final private static boolean RUN_MASS_IMAGEKEY_CREATION_1 = false;
    final private static boolean RUN_MASS_IMAGEKEY_CREATION_2 = false;

    /**
     * Initializes a new {@link ImageConverterTest}.
     */
    public ImageConverterTest(@NonNull final IImageConverter imageConverter, @NonNull IMetadataReader metadataReader, @NonNull final File spoolDir) {
        super();

        m_imageConverter = imageConverter;
        m_metadataReader = metadataReader;

        implSetSpoolDir(spoolDir);
        implClearImages();
    }

    // - Callable --------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Boolean call() throws Exception {
        Thread.currentThread().setName("ImageConverter Test");

        final long testStartTimeMillis = System.currentTimeMillis();
        final Boolean ret = (implTestMultithreadedCaching() && implTestMultithreaded());
        long lastTimeMillis = 0;

        // Test finished
        ImageConverterTestUtils.logTest("IC_TEST successfully finished IImageConverter test: " + ((lastTimeMillis = System.currentTimeMillis()) - testStartTimeMillis) + "ms");


        // cleanup of created test keys
        final AtomicLong keysLeft = new AtomicLong(0);

        do {
            m_usedContexts.forEach((curContext) -> {
                try {
                    m_imageConverter.clearImages(curContext);
                    Thread.yield();
                } catch (ImageConverterException e) {
                    LOG.error(e.getMessage());
                }
            });

            keysLeft.set(0);

            m_usedContexts.forEach((curContext) -> {
                try {
                    keysLeft.addAndGet(m_imageConverter.getKeyCount(curContext));
                } catch (ImageConverterException e) {
                    LOG.error(e.getMessage());
                }
            });
        } while (keysLeft.get() > 0);

        // Test finished
        ImageConverterTestUtils.logTest("IC_TEST finished cleanup of contexts: " + (System.currentTimeMillis() - lastTimeMillis) + "ms");

        return ret;
    }

    /**
     * @return
     */
    protected Boolean implTestMultithreadedCaching() {
        final int count = 10;
        final ExecutorService execService = Executors.newFixedThreadPool(count);
        final BufferedImage image = new BufferedImage(2048, 2048, BufferedImage.TYPE_INT_RGB);
        final String key = UUID.randomUUID().toString();
        final String ctx_multi = "Ctx_Multi";
        final List<Future<Boolean>> futureList = new ArrayList<>();
        final byte[] imageBuffer;

        m_usedContexts.add(ctx_multi);

        try (final ByteArrayOutputStream oStm = new ByteArrayOutputStream()) {
            ImageIO.write(image, "PNG", oStm);
            oStm.flush();
            imageBuffer = oStm.toByteArray();

            for (int i = 0; i < count; ++i) {
                futureList.add(execService.submit(() -> {
                    Thread.yield();

                    try (final InputStream inStm = new ByteArrayInputStream(imageBuffer);
                         final InputStream imageInputStm = m_imageConverter.cacheAndGetImage(key, "auto:1024x1024", inStm, ctx_multi)) {

                        if (null != imageInputStm) {
                            return Boolean.TRUE;
                        }

                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }

                    return Boolean.FALSE;
                }));
            }

        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        boolean ret = true;

        for (final Future<Boolean> curFuture : futureList) {
            try {
                if (!curFuture.get().booleanValue()) {
                    ret = false;
                }
            } catch (InterruptedException | ExecutionException e) {
                LOG.error(e.getMessage());
            }
        }

        try {
            m_imageConverter.clearImages(ctx_multi);
        } catch (ImageConverterException e) {
            LOG.error(e.getMessage());
        }

        ImageConverterTestUtils.logTest("Multithreaded cache with same key " + (ret ? "successful" : "got errror"));

        return ret;
    }

    /**
     * @return
     */
    protected Boolean implTestMultithreaded() {
        final ExecutorService execService = Executors.newFixedThreadPool(100);
        final List<Future<Boolean>> futureList = new ArrayList<>();
        final int count = 10;


        for (int i = 0; i < count; ++i) {
            final int round = i + 1;

            futureList.add(execService.submit(() -> {
                try {
                    return implTest(round);
                } catch (Exception e) {
                    LOG.error(e.getMessage());
                }

                return Boolean.FALSE;
            }));
        }

        Boolean ret = Boolean.TRUE;

        for (final Future<Boolean> curFuture : futureList) {
            try {
                if (!curFuture.get().booleanValue()) {
                    ret = Boolean.FALSE;
                }
            } catch (InterruptedException | ExecutionException e) {
                LOG.error(e.getMessage());
            }
        }

        return ret;
    }

    /**
     * @return
     * @throws Exception
     */
    protected Boolean implTest(final int round ) throws Exception {
        final String ctx1 = "Ctx_1_r" + round;
        final String ctx2 = "Ctx_2_r" + round;
        File testInputFile = null;

        m_usedContexts.add(ctx1);
        m_usedContexts.add(ctx2);

        try {
            ImageConverterTestUtils.logTest("Starting IImageConverter test");

            if (null != m_imageConverter) {
                if (null != (testInputFile = implCreateTestImageFile(ImageFormat.ImageType.PNG))) {
                    int pos = -1;
                    File testOutputFile = null;

                    for (int i = 0; i < m_testMap.size(); ++i) {
                        testOutputFile = implGetFile(++pos);
                        implTest(testInputFile, UUID.randomUUID().toString(), testOutputFile, ((i % 2) == 0) ? ctx1 : ctx2);
                    }

                    long count = m_imageConverter.getKeyCount();
                    ImageConverterTestUtils.logTest("Current key count: " + count);

                    // get keys by contexts
                    final long ctx1Count = m_imageConverter.getKeyCount(ctx1);
                    final long ctx2Count = m_imageConverter.getKeyCount(ctx2);

                    ImageConverterTestUtils.logTest("Current key count for context " + ctx1 + ": " + ctx1Count);
                    ImageConverterTestUtils.logTest("Current key count for context " + ctx2 + ": " + ctx2Count);

                    // delete Ctx_2 files
                    m_imageConverter.clearImages(ctx2);

                    long newCount = m_imageConverter.getKeyCount();
                    ImageConverterTestUtils.logTest("Current key count after clearing images by context " + ctx2 + ": " + newCount);
                    ImageConverterTestUtils.logTest("Current key count after clearing images by context is correct: " + new Boolean(newCount == (count - ctx2Count)).toString());

                    // delete all ctx1 key images
                    ImageConverterTestUtils.logTest("Current key count after clearing all ctx1 images: " + m_imageConverter.getKeyCount());

                    // Performance test
                    implTestPerformance();

                    // Queue priority change
                    ImageConverterTestUtils.logTest("Running queue priority change test, please wait...");
                    implTestQueuePriorityChange(round);
                    ImageConverterTestUtils.logTest("Finished queue priority change test!");

                    // Create massive amount of image keys #1
                    if (RUN_MASS_IMAGEKEY_CREATION_1) {
                        ImageConverterTestUtils.logTest("Running mass key creation test #1, please wait...");
                        implTestMassImages1();
                        ImageConverterTestUtils.logTest("Finished mass key creation test #1!");
                    }

                    // Create massive amount of image keys #1
                    if (RUN_MASS_IMAGEKEY_CREATION_2) {
                        ImageConverterTestUtils.logTest("Running mass key creation test #2, please wait...");
                        implTestMassImages2();
                        ImageConverterTestUtils.logTest("Finished mass key creation test #2!");
                    }

                    // delete all ctx1 and ctx2 key images
                    m_imageConverter.clearImages(ctx1);
                    m_imageConverter.clearImages(ctx2);
                    ImageConverterTestUtils.logTest("Current key count after clearing all ctx1 and ctx2 images: " + m_imageConverter.getKeyCount());

                    // ImageMetadata test => see created file: /tmp/oxic_test_metadata.json
                    File metadaTmpFile = null;

                    try {
                        if (null != (metadaTmpFile = File.createTempFile("ictesti", ".tmp", m_spoolDir))) {
                            FileUtils.copyInputStreamToFile(new ByteArrayInputStream(
                                ImageConverterTestUtils.getBundleResourceBuffer("ox_metadata.jpg", ImageConverterTest.class)),
                                metadaTmpFile);

                            if (metadaTmpFile.canRead()) {
                                final String targetFilename = "/tmp/oxic_test_metadata.json";
                                final long startTime = System.currentTimeMillis();
                                m_metadataReader.readMetadata(metadaTmpFile).getJSONObject().prettyPrintTo(new File(targetFilename));
                                LOG.info("IC_TEST MetadataTest read time: " + (System.currentTimeMillis() - startTime) + "ms. File output written to: {}", targetFilename);
                            }
                        }
                    } finally {
                        FileUtils.deleteQuietly(metadaTmpFile);
                    }
                }
            }
        } catch (Exception e) {
            ImageConverterTestUtils.logTest("IC_TEST error while executing IImageConverter test. Exception caught: " + Throwables.getRootCause(e).getMessage());
            throw e;
        } finally {
            try {
                m_imageConverter.clearImages(ctx1);
                m_imageConverter.clearImages(ctx2);
            } catch (ImageConverterException e) {
                ImageConverterTestUtils.logTest("IC_TEST Received exception: " + Throwables.getRootCause(e).getMessage());
            }

            FileUtils.deleteQuietly(testInputFile);
            implClearImages();
        }

        return Boolean.TRUE;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param spoolDir
     */
    private void implSetSpoolDir(@NonNull final File spoolDir) {
        if (null != spoolDir) {
            File targetDir = new File(spoolDir, IMAGECONVERTER_TEST_TEMP_DIRNAME);

            // use or create given spool directory, use /tmp as fallback parent directory
            if (((targetDir.exists() && targetDir.canWrite() && targetDir.isDirectory()) ||
                targetDir.mkdirs() ||
                (targetDir = new File("/tmp", IMAGECONVERTER_TEST_TEMP_DIRNAME)).mkdirs())) {

                try {
                    FileUtils.forceDeleteOnExit(m_spoolDir = targetDir);
                    FileUtils.cleanDirectory(targetDir);
                } catch (@SuppressWarnings("unused") IOException e) {
                    LOG.error("IC_TEST is not able to delete spool directory when shut down: {}", m_spoolDir.toString());
                }
            }
        }
    }


    /**
    *
    */
    private static void implClearImages() {
        for (final String curFilename : m_testMap.keySet()) {
            FileUtils.deleteQuietly(new File(curFilename));
        }
    }

    /**
     * @param pos
     * @return
     */
    private static File implGetFile(int pos) {
        return new File(m_testFileNames[pos]);
    }

    /**
     * @param imageType
     * @return
     * @throws ImageConverterException
     */
    private File implCreateTestImageFile(@NonNull final ImageFormat.ImageType imageType) throws ImageConverterException {
        File ret = null;

        try {
            if (null != (ret = File.createTempFile("ictesti", ".tmp", m_spoolDir))) {
                FileUtils.copyInputStreamToFile(new ByteArrayInputStream(
                    ImageConverterTestUtils.getBundleResourceBuffer("ox-test." + imageType.getShortName(),ImageConverterTest.class)), ret);
            }
        } catch (Exception e) {
            FileUtils.deleteQuietly(ret);
            throw new ImageConverterException(e);
        }

        return ret;
    }

    /**
     * @param key
     * @param outputFile
     * @param context
     */
    void implTest(final File inputFile, final String key, final File outputFile, final String context) {
        final String path = outputFile.getAbsolutePath();
        final String format = m_testMap.get(path);

        try (final InputStream testInputStm = FileUtils.openInputStream(inputFile)) {
            try (final InputStream resultStm = m_imageConverter.cacheAndGetImage(key, format, testInputStm, context)) {
                if (null != resultStm) {
                    FileUtils.copyInputStreamToFile(resultStm, outputFile);
                } else {
                    ImageConverterTestUtils.logTest("Error creating/getting image for key: " + format);
                }
            }
        } catch (Exception e) {
            LOG.error(Throwables.getRootCause(e).getMessage());
        } finally {
            final StringBuilder logName = new StringBuilder(path).append(" / ").append(m_testMap.get(path)).append(" : ");

            if (outputFile.exists()) {
                ImageConverterTestUtils.logTest("Test sucess:" + logName.append(outputFile.length()));
            } else {
                ImageConverterTestUtils.logTest("Test error: " + logName.substring(0, logName.length() - 3));
            }
        }
    }

    /**
     *
     */
    void implTestPerformance() {
        File perfInputFile = null;
        final int perfCount = 10;
        final String perfContext = "performance";

        for (int n = 0; n < 1; ++n) {
            try {
                perfInputFile = implCreateTestImageFile(ImageFormat.ImageType.PNG);

                final long startTime = System.currentTimeMillis();

                for (int i = 0; i < perfCount; ++i) {
                    final String curPerfKey = "perf_" + i;

                    try (final InputStream perfInputStm = new BufferedInputStream(FileUtils.openInputStream(perfInputFile))) {
                        try (final InputStream ret0 = m_imageConverter.cacheAndGetImage(curPerfKey, "jpg:100x150", perfInputStm, perfContext)) {
                            if (null != ret0) {
                                ImageConverterTestUtils.logTest("Performance test got image when calling cacheAndGetImage: " + curPerfKey);

                                try (final InputStream ret1 = m_imageConverter.getImage(curPerfKey, "jpg:1279x767", perfContext)) {
                                    if (null != ret1) {
                                        ImageConverterTestUtils.logTest("Performance test got image when calling getImage: " + curPerfKey);
                                    }
                                }

                                try (final InputStream ret2 = m_imageConverter.getImage(curPerfKey, "jpg:1920x1080", perfContext)) {
                                    if (null != ret2) {
                                        ImageConverterTestUtils.logTest("Performance test got image when calling getImage: " + curPerfKey);
                                    }
                                }

                                try (final InputStream ret2 = m_imageConverter.getImage(curPerfKey, "jpg:200x150~cover", perfContext)) {
                                    if (null != ret2) {
                                        ImageConverterTestUtils.logTest("Performance test got 'cover' image when calling getImage: " + curPerfKey);
                                    }
                                }

                                try (final InputStream ret2 = m_imageConverter.getImage(curPerfKey, "jpg:400x300~cover", perfContext)) {
                                    if (null != ret2) {
                                        ImageConverterTestUtils.logTest("Performance test got 'cover' image when calling getImage: " + curPerfKey);
                                    }
                                }
                            }
                        }
                    }
                }

                final long perfDuration = System.currentTimeMillis() - startTime;

                ImageConverterTestUtils.logTest("Performance test duration for  " + perfCount + " images: " + perfDuration + "ms (Throughput: " + ((double) perfCount * 1000 / perfDuration) + "/s)");
                ImageConverterTestUtils.logTest("Current image key count: " + m_imageConverter.getKeyCount());

                if (1 == (n % 1)) {
                    m_imageConverter.clearImages(perfContext);
                }

                ImageConverterTestUtils.logTest("Current image key count after clearing images: " + m_imageConverter.getKeyCount());
            } catch (Exception e) {
                ImageConverterTestUtils.logTest("Performance test error: " + Throwables.getRootCause(e).getMessage());
            } finally {
                try {
                    m_imageConverter.clearImages(perfContext);
                } catch (ImageConverterException e) {
                    ImageConverterTestUtils.logTest("Performance test error: " + Throwables.getRootCause(e).getMessage());
                }

                if (null != perfInputFile) {
                    FileUtils.deleteQuietly(perfInputFile);
                }
            }
        }
    }

    /**
     * @throws ImageConverterException
     */
    void implTestQueuePriorityChange(final int round) throws ImageConverterException {
        final String ctx_queuePriorityTest = "ctx_testQueuePriority_r" + round;
        final String keyPrefix = "Queuetest-r" + round + "-";
        final int totalCount = 100;
        final int getCount = totalCount >> 1;
        final BufferedImage image = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
        final byte[] imageBuffer;

        m_usedContexts.add(ctx_queuePriorityTest);

        try {
            try (final ByteArrayOutputStream oStm = new ByteArrayOutputStream()) {
                ImageIO.write(image, "PNG", oStm);
                oStm.flush();
                imageBuffer = oStm.toByteArray();
            }

            // fill proccessing queue with background jobs
            for (int i = 1; i <= totalCount; ++i) {
                try (final InputStream inStm = new ByteArrayInputStream(imageBuffer)) {
                    final String key = new StringBuilder(keyPrefix).append(i).toString();
                    m_imageConverter.cacheImage(key, inStm, ctx_queuePriorityTest);
                }
            }

            // get images from back to front to test priority change to highest queue
            for (int i = totalCount; i > (totalCount - getCount); --i) {
                final String key = new StringBuilder(keyPrefix).append(i).toString();

                try (InputStream cachedImgStm = m_imageConverter.getImage(key, "jpg:999x999", ctx_queuePriorityTest)) {
                    if (null == cachedImgStm) {
                        ImageConverterTestUtils.logTest("Queue image test error when caching image");
                    }
                } finally {
                    m_imageConverter.clearImagesByKey(key);
                }
            }

            for (int i = (totalCount - getCount); i > 0; --i) {
                final String key = new StringBuilder(keyPrefix).append(i).toString();
                m_imageConverter.clearImagesByKey(key);
            }
        } catch (Exception e) {
            ImageConverterTestUtils.logTest("Queue image test error in queue changing test: " + e.getMessage());
        } finally {
            m_imageConverter.clearImages(ctx_queuePriorityTest);
        }
    }

    /**
     * @throws ImageConverterException
     */
    void implTestMassImages1() throws ImageConverterException {
        final long testStartTimeMillis = System.currentTimeMillis();
        final String ctx_massTest = "ctx_testMass";
        final int width = 32;
        final int height = 32;
        final int colorCount = 128;

        m_usedContexts.add(ctx_massTest);

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                for (int color = 0; color < colorCount; ++color) {
                    final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

                    image.setRGB(x, y, color);

                    try (final ByteArrayOutputStream oStm = new ByteArrayOutputStream()) {
                        ImageIO.write(image, "PNG", oStm);
                        oStm.flush();

                        try (final InputStream inStm = new ByteArrayInputStream(oStm.toByteArray())) {
                            final String key = new StringBuilder(Integer.toString(x)).
                                append("x").append(y).
                                append("-").append(color).toString();

                            try (InputStream imgStm = m_imageConverter.getImage(key, "jpg", ctx_massTest)) {
                                if (null == imgStm) {
                                    try (InputStream cachedImgStm = m_imageConverter.cacheAndGetImage(key, "jpg", inStm, ctx_massTest)) {
                                        if (null == cachedImgStm) {
                                            ImageConverterTestUtils.logTest("Mass image test error when caching image");
                                            throw new ImageConverterException("IC error when caching image");
                                        }
                                    }
                                }
                            }
                        }

                    } catch (Throwable e) {
                        ExceptionUtils.handleThrowable(e);
                        ImageConverterTestUtils.logTest("Mass image test excepted when creating image: " + Throwables.getRootCause(e));
                        throw new ImageConverterException(e);
                    }
                }
            }
        }

        ImageConverterTestUtils.logTest("Mass image test creation finished in " + (System.currentTimeMillis() - testStartTimeMillis) + "ms");
    }

    /**
     * @throws ImageConverterException
     */
    void implTestMassImages2() {
        final long testStartTimeMillis = System.currentTimeMillis();
        final String ctx_massTest2 = "ctx_testMass2";
        final long massEntryCount = 1000000;
        final long throughputCount = 1000;
        final int threadCount = 100;
        final AtomicLong counter = new AtomicLong();
        final StringBuilder keyNameBuilder = new StringBuilder(40).append("OX_MASSTEST2_IMAGEKEY").append('_');
        final Thread threadArray[] = new Thread[threadCount];
        final AtomicLong lastTime = new AtomicLong(testStartTimeMillis);
        final int width = 16;
        final int height = 16;
        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        byte[] tmpBuffer = null;

        m_usedContexts.add(ctx_massTest2);

        // create dummy PNG image buffer
        try (final ByteArrayOutputStream oStm = new ByteArrayOutputStream()) {
            ImageIO.write(image, "PNG", oStm);
            oStm.flush();
            tmpBuffer = oStm.toByteArray();
        } catch (IOException e) {
            ImageConverterTestUtils.logTest("IC Mass test #2 received Exception while creating image buffer: " +
                Throwables.getRootCause(e).getMessage());
        }

        final byte[] imageBuffer = tmpBuffer;

        for (int i = 0; i < threadCount; ++i) {
            threadArray[i] = new Thread(new Runnable() {

                @Override
                public void run() {
                    long curNumber = 0;

                    while ((curNumber = counter.incrementAndGet()) <= massEntryCount) {
                        final String curKey = new StringBuilder(keyNameBuilder).append(curNumber).toString();

                        try (final InputStream imageInputStm = new ByteArrayInputStream(imageBuffer)) {
                            m_imageConverter.cacheImage(curKey, imageInputStm, ctx_massTest2);
                        } catch (Exception e) {
                            ImageConverterTestUtils.logTest("IC Mass test #2 received Exception while creating new entry: " +
                                Throwables.getRootCause(e).getMessage());
                        }

                        if ((curNumber % throughputCount) == 0) {
                            final long curTime = System.currentTimeMillis();
                            final long duration = curTime - lastTime.get();

                            ImageConverterTestUtils.logTest(new StringBuilder(256).
                                append("IC Mass test #2 created ").append(throughputCount).append(" image keys /").
                                append(" Total: ").append(curNumber).append(" /").
                                append(" Create time: ").append(duration).append("ms").append(" /").
                                append(" Throughput: ").append((double) throughputCount / duration * 1000.0).toString());

                            lastTime.set(curTime);
                        }
                    }
                }
            });

            threadArray[i].start();
        }

        // wait until all threads will have finished
        for (int i = 0; i < threadCount; ++i) {
            try {
                threadArray[i].join();
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                // ok
            }
        }

        ImageConverterTestUtils.logTest("Mass image test #2 finished in " +
            (System.currentTimeMillis() - testStartTimeMillis) + "ms");
    }

    // - Static members --------------------------------------------------------

    final protected static Logger LOG = LoggerFactory.getLogger(ImageConverterTest.class);

    final protected static String IMAGECONVERTER_TEST_TEMP_DIRNAME = "oxictest";

    // - Members ---------------------------------------------------------------

    final private Set<String> m_usedContexts = new LinkedHashSet<>();

    protected IImageConverter m_imageConverter = null;

    private IMetadataReader m_metadataReader = null;

    private File m_spoolDir = null;

    private static final Map<String, String> m_testMap = new LinkedHashMap<>();

    private static String[] m_testFileNames = null;

    static {
        m_testMap.put("/tmp/imgs_00.img", "auto:268x128@90");
        m_testMap.put("/tmp/imgs_01.img", "jpg:268x128@90");
        m_testMap.put("/tmp/imgs_02.img", "png:128x256@6");
        m_testMap.put("/tmp/imgs_03.img", "128x128");
        m_testMap.put("/tmp/imgs_04.img", "368x128");
        m_testMap.put("/tmp/imgs_05.img", "32x64@6");
        m_testMap.put("/tmp/imgs_06.img", "jpg");
        m_testMap.put("/tmp/imgs_07.img", "32x");
        m_testMap.put("/tmp/imgs_08.img", "x32");
        m_testMap.put("/tmp/imgs_09.img", ":x32@90");
        m_testMap.put("/tmp/imgs_10.img", "jpg:-1x32");
        m_testMap.put("/tmp/imgs_11.img", "jpg:32x-1");
        m_testMap.put("/tmp/imgs_12.img", "jpg:-1x-1");
        m_testMap.put("/tmp/imgs_97.img", "32");
        m_testMap.put("/tmp/imgs_97.img", "png32");
        m_testMap.put("/tmp/imgs_98.img", "jpg32@");
        m_testMap.put("/tmp/imgs_99.img", "jpgx32");

        m_testFileNames = m_testMap.keySet().toArray(new String[] {});
    }
}
