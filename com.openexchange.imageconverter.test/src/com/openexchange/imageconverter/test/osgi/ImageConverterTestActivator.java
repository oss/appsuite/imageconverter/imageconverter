/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.test.osgi;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.auth.Credentials;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.imageconverter.api.IImageClient;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.test.impl.ImageConverterClientTest;
import com.openexchange.imageconverter.test.impl.ImageConverterTest;
import com.openexchange.imageconverter.test.impl.ImageConverterTestUtils;
import com.openexchange.imageconverter.test.impl.ObjectCacheTest;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link ImageConverterTestActivator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterTestActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            IObjectCache.class,
            IMetadataReader.class,
            IImageConverter.class,
            IImageClient.class
        };
    }

    /**
     * Initializes a new {@link ImageConverterTestActivator}.
     */
    public ImageConverterTestActivator() {
        super();
    }

    //-------------------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    public void startBundle() throws Exception {
        try {
            LOG.info("starting bundle: {}", SERVICE_NAME);

            m_configService = getService(ConfigurationService.class);

            if (null == m_configService) {
                throw new OXException(new Throwable("Service not available: Configuration"));
            }

            openTrackers();

            // read config
            final String[] testBundles = m_configService.getProperty("com.openexchange.imageconverter.test.bundles", "").split("[,;]");
            final String testOutputFilename = m_configService.getProperty("com.openexchange.imageconverter.test.logFile");
            final List<Callable<Boolean>> testList = new ArrayList<>();

            ImageConverterTestUtils.startTest(StringUtils.isNotBlank(testOutputFilename) ? new File(testOutputFilename) : null);

            if (isNotEmpty(testBundles)) {
                for (final String testBundle : testBundles) {
                    final String curBundle = testBundle.trim().toLowerCase();

                    if (isNotEmpty(curBundle)) {
                        Callable<Boolean> callable = null;
                        String errorBundle = null;

                        switch (curBundle) {
                            case ("objectcache"): {
                                if (null == (m_objectCache = getService(IObjectCache.class))) {
                                    errorBundle = "IObjectCache";
                                } else {
                                    callable = new ObjectCacheTest(m_objectCache);
                                }

                                break;
                            }

                            case ("imageconverter"): {
                                if (null == (m_imageConverter = getService(IImageConverter.class)) ||
                                    null == (m_metadataReader = getService(IMetadataReader.class))) {

                                    errorBundle = "IImageConverter";
                                } else {
                                    final File spoolPath = new File(m_configService.getProperty("com.openexchange.imageconverter.test.spoolPath", "/tmp"));
                                    callable = new ImageConverterTest(m_imageConverter, m_metadataReader, spoolPath);
                                }

                                break;
                            }

                            case ("imageconverterclient"): {
                                if (null == (m_imageConverterClient = getService(IImageClient.class))) {
                                    errorBundle = "IImageClient";
                                } else {
                                    final String authUsername = m_configService.getProperty("com.openexchange.imageconverter.test.username");
                                    final String authPassword = m_configService.getProperty("com.openexchange.imageconverter.test.password");

                                    if (StringUtils.isNotBlank(authUsername) && StringUtils.isNotBlank(authPassword)) {
                                        m_imageConverterClient.setCredentials(new Credentials(authUsername, authPassword));
                                    }

                                    callable = new ImageConverterClientTest(m_imageConverterClient);
                                }

                                break;
                            }

                            default: {
                                break;
                            }
                        }

                        if (null != callable) {
                            testList.add(callable);
                        } else if (null != errorBundle) {
                            ImageConverterTestUtils.logTest(errorBundle + " bundle cannot be tested due to unavailable service");
                        }
                    }
                }

                // execute list of tests after an initial delay time
                if (testList.size() > 0) {
                    Executors.newSingleThreadExecutor().submit(() -> {
                        long curTime = System.currentTimeMillis();
                        final long delayTimeMillis = 5000;
                        final long endTimeMillis = curTime + delayTimeMillis;

                        do {
                            try {
                                Thread.sleep(endTimeMillis - curTime);
                            } catch (@SuppressWarnings("unused") final Exception e) {
                                //
                            }
                        } while ((curTime = System.currentTimeMillis()) < endTimeMillis);

                        return Executors.newFixedThreadPool(testList.size()).invokeAll(testList);
                    });
                }
            }
        } catch (final Throwable e) {
            ExceptionUtils.handleThrowable(e);
            LOG.error("IC test starting bundle: {}", SERVICE_NAME + " failed", e);
            throw new RuntimeException(e);
        } finally {
            ImageConverterTestUtils.testFinished();
        }
    }

    //-------------------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    public void stopBundle() throws Exception {
        LOG.info("stopping bundle: {}", SERVICE_NAME);
    }

    // - Members ---------------------------------------------------------------

    private ConfigurationService m_configService = null;

    private IObjectCache m_objectCache = null;

    private IImageConverter m_imageConverter = null;

    private IMetadataReader m_metadataReader = null;

    private IImageClient m_imageConverterClient = null;

    // - Static Members --------------------------------------------------------

    final private static String SERVICE_NAME = "OX Software ImageConverter Test";

    final private static Logger LOG = LoggerFactory.getLogger(ImageConverterTestActivator.class);
}
