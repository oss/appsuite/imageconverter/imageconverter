/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.osgi;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.File;
import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.eclipse.microprofile.health.HealthCheck;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import com.eclipsesource.jaxrs.publisher.ApplicationConfiguration;
import com.google.common.base.Throwables;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.WildcardNamePropertyFilter;
import com.openexchange.database.DatabaseService;
import com.openexchange.database.Databases;
import com.openexchange.database.migration.DBMigration;
import com.openexchange.database.migration.DBMigrationConnectionProvider;
import com.openexchange.database.migration.DBMigrationExecutorService;
import com.openexchange.database.migration.DBMigrationMonitorService;
import com.openexchange.database.migration.resource.accessor.BundleResourceAccessor;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.DatabaseAccess;
import com.openexchange.filestore.DatabaseAccessProvider;
import com.openexchange.filestore.DatabaseTable;
import com.openexchange.filestore.FileStorage;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.filestore.FileStorageService;
import com.openexchange.filestore.FileStorages;
import com.openexchange.folder.FolderService;
import com.openexchange.html.HtmlService;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheConfig;
import com.openexchange.objectcache.api.ObjectCacheException;
import com.openexchange.objectcache.api.Services;
import com.openexchange.objectcache.impl.ConnectionWrapper;
import com.openexchange.objectcache.impl.ObjectCache;
import com.openexchange.objectcache.impl.ObjectCacheDatabase;
import com.openexchange.objectcache.impl.ObjectCacheUtils;
import com.openexchange.objectcache.impl.adapters.file.FileAdapter;
import com.openexchange.objectcache.impl.adapters.file.FileAdapterConfig;
import com.openexchange.objectcache.impl.adapters.file.FileAdapterConfig.FileAdapterConfigBuilder;
import com.openexchange.objectcache.impl.adapters.filestore.FileStorageAdapter;
import com.openexchange.objectcache.impl.adapters.s3.S3Adapter;
import com.openexchange.objectcache.impl.adapters.s3.S3AdapterConfig;
import com.openexchange.objectcache.impl.adapters.s3.S3AdapterConfig.S3AdapterConfigBuilder;
import com.openexchange.objectcache.impl.adapters.sproxyd.SproxydAdapter;
import com.openexchange.objectcache.impl.adapters.sproxyd.SproxydAdapterConfig;
import com.openexchange.objectcache.impl.adapters.sproxyd.SproxydAdapterConfig.SproxydAdapterConfigBuilder;
import com.openexchange.objectcache.impl.server.ObjectCacheServer;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link ObjectCacheActivator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ObjectCacheActivator extends HousekeepingActivator {

	final private static String SERVICE_NAME = "ObjectCache";
	final private static String FILEITEM_STORE_PREFIX = "fis";
	final private static String S3_PREFIX = "s3:";
	final private static String SPROXYD_PREFIX = "sproxyd:";
	final private static String FILE_PREFIX = "file:";
	final private static String HTTP_PREFIX = "http://";
	final private static String HTTPS_PREFIX = "https://";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
	 */
	@Override
	protected Class<?>[] getNeededServices() {
		return new Class<?>[] {
			ConfigurationService.class,
			FileStorageService.class,
			FolderService.class,
			DatabaseService.class,
			DBMigrationExecutorService.class,
			DBMigrationMonitorService.class,
			HtmlService.class,
			SSLSocketFactoryProvider.class
		};
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.openexchange.osgi.HousekeepingActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext ctx) throws Exception {
		super.start(ctx);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.openexchange.osgi.DeferredActivator#startBundle()
	 */
	@Override
	protected void startBundle() throws Exception {
		LOG.info("OC starting bundle: {} (OC)", SERVICE_NAME);

		Services.setServiceLookup(this);

		m_dbMigrationMonitorTracker = new ServiceTracker<>(context, DBMigrationMonitorService.class, new DBMigrationMonitorCustomizer(context));
		m_dbMigrationMonitorTracker.open();

		m_applicationConfigurationTracker = new ServiceTracker<>(context, ApplicationConfiguration.class, new ApplicationConfigurationCustomizer(context));
		m_applicationConfigurationTracker.open();

		openTrackers();

		LOG.info("OC successfully started bundle: {}", SERVICE_NAME);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
	 */
	@Override
	protected void stopBundle() throws Exception {
		LOG.info("OC stopping bundle: {} (OC)", SERVICE_NAME);

		if (m_running.compareAndSet(true, false)) {
			if (null != m_objectCacheServer) {
				m_objectCacheServer.shutdown();
				m_objectCacheServer = null;
			}

			if (null != m_objectCache) {
				m_objectCache.shutdown();
				m_objectCache = null;
			}

			closeTrackers();
			unregisterServices();

			Services.setServiceLookup(null);

			LOG.info("OC successfully stopped bundle: {}", SERVICE_NAME);
		}
	}

	/**
	 *
	 */
	protected void implRegisterServices() {
		final ConfigurationService configService = getService(ConfigurationService.class);
		final ObjectCacheConfig.Builder objectCacheConfigBuilder = ObjectCacheConfig.builder();
		final SSLSocketFactoryProvider sslSocketFactoryProvider = getService(SSLSocketFactoryProvider.class);
		final SocketFactory socketFactory = (null != sslSocketFactoryProvider) ?
				sslSocketFactoryProvider.getDefault() :
					SSLSocketFactory.getDefault();


		// compatibility calls to init config from c.o.fileitem.properties first
		implSetupObjectCacheConfigBuilderFromFileItemProperties(configService, objectCacheConfigBuilder, "com.openexchange.fileItem.");
		implSetupObjectCacheConfigBuilderFromFileItemProperties(configService, objectCacheConfigBuilder, "com.openexchange.fileitem.");

		// finally update Config from 'objectcache.properties'
		implSetupObjectCacheConfigBuilder(configService, objectCacheConfigBuilder);

		// Create Config
		final ObjectCacheConfig objectCacheConfig = objectCacheConfigBuilder.build();

		// set spool dir
		ObjectCacheUtils.setSpoolPath(objectCacheConfig.getSpoolPath());

		// try to get configured filestore
		final Set<String> storeIdSet = objectCacheConfig.getObjectStoreIds();
		final Optional<ObjectCacheDatabase> fileItemDatabaseOpt = Optional.ofNullable(implCreateObjectCacheDatabase(objectCacheConfig));
		final Optional<Set<IObjectStore>> obectStoreSetOpt = Optional.ofNullable(implGetObjectStores(configService, socketFactory, storeIdSet));

		m_objectCache = new ObjectCache(objectCacheConfig, obectStoreSetOpt, fileItemDatabaseOpt);

		registerService(IObjectCache.class, m_objectCache);
		registerService(HealthCheck.class, new ObjectCacheHealthCheck(m_objectCache, objectCacheConfig));

		try {
			// notify waiting threads that ObjectCache instance has been registered
			m_objectCacheLock.lock();
			m_objectCacheCondition.signalAll();
		} finally {
			m_objectCacheLock.unlock();
		}

		LOG.info("OC successfully registered ObjectCache");
	}

	/**
	 * @param configService
	 * @return
	 */
	private @Nullable Set<IObjectStore> implGetObjectStores(
			@NonNull final ConfigurationService configService,
			@NonNull SocketFactory socketFactory,
			@NonNull Set<String> storeSet) {

		Set<IObjectStore> ret = new HashSet<>();
		int storeIndex = 0;

		// retrieve file stores for the given ids
		for (String curStoreStr : storeSet) {
			curStoreStr = curStoreStr.trim();

			if (StringUtils.startsWithIgnoreCase(curStoreStr, S3_PREFIX)) {
				try {
					final int s3StoreConfigId = Integer.parseInt(curStoreStr.substring(S3_PREFIX.length()).trim());
					final S3AdapterConfig s3AdapterConfig = implGetS3AdapterConfig(
							configService,
							socketFactory,
							s3StoreConfigId);

					if (null != s3AdapterConfig) {
						ret.add(new S3Adapter(s3AdapterConfig, s3StoreConfigId));
						LOG.info("OC uses configured S3 ObjectStore: [endpoint: {}, bucket: {}]",
								s3AdapterConfig.getEndpoint(),
								s3AdapterConfig.getBucketName());
					}
				} catch (ObjectCacheException e) {
					LOG.error("OC is not able to connect to S3 ObjectStore for given config id: {} (Reason: {}",
							curStoreStr, Throwables.getRootCause(e).getMessage());
				} catch (@SuppressWarnings("unused") NumberFormatException e) {
					LOG.error("OC is not able to parse configured S3 ObjectStore (id: {})", curStoreStr);
				}
			} else if (StringUtils.startsWithIgnoreCase(curStoreStr, SPROXYD_PREFIX)) {
				try {
					final int sproxydStoreConfigId = Integer.parseInt(curStoreStr.substring(SPROXYD_PREFIX.length()).trim());
					final SproxydAdapterConfig sproxydAdapterConfig = implGetSproxydAdapterConfig(
							configService,
							socketFactory,
							sproxydStoreConfigId);

					if (null != sproxydAdapterConfig) {
						ret.add(new SproxydAdapter(sproxydAdapterConfig, sproxydStoreConfigId));
						LOG.info("OC uses configured SproxyD ObjectStore (url: {})", sproxydAdapterConfig.getBaseURLString());
					}
				} catch (ObjectCacheException e) {
					LOG.error("OC is not able to connect to SproxyD ObjectStore for given config id (id: {})", curStoreStr, e);
				} catch (@SuppressWarnings("unused") NumberFormatException e) {
					LOG.error("OC is not able to parse configured S3 ObjectStore (id: {})", curStoreStr);
				}
			} else if (StringUtils.startsWithIgnoreCase(curStoreStr, FILE_PREFIX)) {
				try {
					final int fileConfigId = Integer.parseInt(curStoreStr.substring(FILE_PREFIX.length()).trim());
					final FileAdapterConfig fileAdapterConfig = implGetFileAdapterConfig(configService, fileConfigId);

					if (null != fileAdapterConfig) {
						ret.add(new FileAdapter(fileAdapterConfig, fileConfigId));
						LOG.info("OC uses configured file ObjectStore: {}",
								fileAdapterConfig.getRootPath().getAbsolutePath());
					}
				} catch (ObjectCacheException e) {
					LOG.error("OC is not able to connect to file ObjectStore for given config (id: {})", curStoreStr, e);
				} catch (@SuppressWarnings("unused") NumberFormatException e) {
					LOG.error("OC is not able to parse configured file ObjectStore (id: {})", curStoreStr);
				}
			} else if (NumberUtils.isParsable(curStoreStr)) {
				try {
					final Integer curStoreId = Integer.parseInt(curStoreStr);
					final FileStorageService fileStoreService = getService(FileStorageService.class);
					final URI fileStorageURI = FileStorages.getFullyQualifyingUriFor(curStoreId.intValue(), FILEITEM_STORE_PREFIX + ++storeIndex);
					final FileStorage fileStorage = (null != fileStorageURI) ? fileStoreService.getFileStorage(fileStorageURI) : null;

					if (null != fileStorage) {
						ret.add(new FileStorageAdapter(fileStorage, curStoreId));
						LOG.info("OC uses configured FileStorage (id: {})", curStoreStr);
					} else {
						LOG.error("OC is not able to use configured FileStorage  (id: {})", curStoreStr);
					}
				} catch (IllegalStateException e) {
					// this exception is thrown, if the FileStorageInfoService has been started, but the internal members have not been initialized accordingly right now
					LOG.error("OC activator FileStorages#getFullyQualifyingUriFor is not yet able to successfully finish", e);
				} catch (OXException e) {
					LOG.error("OC activator could not connect to FileStorage for given config (id: {})", curStoreStr, e);
				} catch (@SuppressWarnings("unused") NumberFormatException e) {
					LOG.error("OC activator is not able to parse configured FileStorage (id: {})", curStoreStr);
				} catch (Exception e) {
					LOG.error("OC activator could not get FileStorage service for given config (id: {})", curStoreStr, e);
				}
			}
		}

		if (ret.size() < 1) {
			ret = null;
			LOG.error("OC activator could not establish object store connection, please check if an object store has been correctly set up and configured");
		}

		return ret;
	}

	/**
	 * @param configService
	 * @param storeIdSet
	 * @return
	 */
	private @Nullable ObjectCacheDatabase implCreateObjectCacheDatabase(@NonNull final ObjectCacheConfig objectCacheConfig) {
		final String dbConnectionStr = new StringBuilder(256).
				append(objectCacheConfig.getDBHost()).append(':').append(objectCacheConfig.getDBPort()).
				append('/').append(objectCacheConfig.getDBSchema()).toString();

		try {
			final ObjectCacheDatabase objectCacheDatabase = new ObjectCacheDatabase(objectCacheConfig);
			final DatabaseService dbService = getService(DatabaseService.class);

			if (null != dbService) {
				final ObjectCacheDatabaseAccess objectCacheDatabaseAccess = new ObjectCacheDatabaseAccess(objectCacheDatabase);

				registerService(DatabaseAccessProvider.class, (fileStorageId, prefix) -> {
					return (StringUtils.startsWith(prefix, FILEITEM_STORE_PREFIX)) ?
							objectCacheDatabaseAccess :
								null;
				});

				getService(DBMigrationExecutorService.class).scheduleDBMigration(
						new DBMigration(objectCacheDatabaseAccess, "/objectCacheChangeLog.xml",
								new BundleResourceAccessor(context.getBundle()), objectCacheConfig.getDBSchema())).awaitCompletion();

				objectCacheDatabase.databaseUpdateFinished();

				LOG.info("OC uses configured database (host: {})", dbConnectionStr);

				return objectCacheDatabase;
			}
		} catch (Exception e) {
			ObjectCacheUtils.logExcp(e);
			LOG.warn("OC is not able to use configured database (host: {})", dbConnectionStr);
		}

		return null;
	}

	/**
	 * @param configService
	 * @param configBuilder
	 */
	private void implSetupObjectCacheConfigBuilder(
			@NonNull final ConfigurationService configService,
			@NonNull final ObjectCacheConfig.Builder configBuilder) {

		final String objectCacheConfigPrefix = "com.openexchange.objectcache.";
		String curValue = null;

		// objectStoreIds
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "objectStoreIds"))) {
			final Set<String> objectStoreIdSet = new LinkedHashSet<>();

			// retrieve valid file store ids from config
			for (String curObjectStoreIdStr : curValue.split(",")) {
				curObjectStoreIdStr = curObjectStoreIdStr.trim();

				if (StringUtils.isNotBlank(curObjectStoreIdStr)) {
					objectStoreIdSet.add(curObjectStoreIdStr);
				}
			}

			configBuilder.withObjectStoreIds(objectStoreIdSet);
		}

		// database.host
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.host"))) {
			configBuilder.withDBHost(curValue);
		}

		// database.port
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.port")) && NumberUtils.isParsable(curValue)) {
			final int dbPort = NumberUtils.toInt(curValue);

			if (dbPort > 0) {
				configBuilder.withDBPort(dbPort);
			}
		}

		// database.schema
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.schema"))) {
			configBuilder.withDBSchema(curValue);
		}

		// database.user
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.user"))) {
			configBuilder.withDBUser(curValue);
		}

		// database.password
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.password"))) {
			configBuilder.withDBPassword(curValue);
		}

		// database.property.*
		try {
			final Map<String, String> dbPropertyMap = configService.getProperties(new WildcardNamePropertyFilter(objectCacheConfigPrefix + "database.property.*"));
			final Properties dbProperties = new Properties();

			for (final String curKey : dbPropertyMap.keySet()) {
				final String dbConfigPair = dbPropertyMap.get(curKey);

				if (isNotEmpty(dbConfigPair)) {
					final int assignPos = dbConfigPair.indexOf('=');

					if ((assignPos > 0) && (assignPos < (dbConfigPair.length() - 1))) {
						dbProperties.put(dbConfigPair.substring(0, assignPos), dbConfigPair.substring(assignPos + 1));
					}
				}
			}

			configBuilder.withDBProperties(dbProperties);
		} catch (final OXException e) {
			LOG.trace("OC caught Exception while reading database properties", e);
		}

		// database.connectionpool.maxPoolSize
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.connectionpool.maxPoolSize")) && NumberUtils.isParsable(curValue)) {
			final int dbMaxPoolSize = NumberUtils.toInt(curValue);

			if (dbMaxPoolSize > 0) {
				configBuilder.withDBPoolMaxSize(dbMaxPoolSize);
			}
		}

		// database.connectionpool.connectTimeout
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.connectionpool.connectTimeout")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolConnectTimeoutMillis = NumberUtils.toInt(curValue);

			if (dbPoolConnectTimeoutMillis > 0) {
				configBuilder.withDBPoolConnectTimeoutMillis(dbPoolConnectTimeoutMillis);
			}
		}

		// database.connectionpool.idleTimeout
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.connectionpool.idleTimeout")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolIdleTimeoutMillis = NumberUtils.toInt(curValue);

			if (dbPoolIdleTimeoutMillis > 0) {
				configBuilder.withDBPoolIdleTimeoutMillis(dbPoolIdleTimeoutMillis);
			}
		}

		// database.connectionpool.maxLifetime
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "database.connectionpool.maxLifetime")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolMaxLifeTimeMillis = NumberUtils.toInt(curValue);

			if (dbPoolMaxLifeTimeMillis > 0) {
				configBuilder.withDBPoolMaxLifetimeMillis(dbPoolMaxLifeTimeMillis);
			}
		}

		// spoolPath
		if (null != (curValue = configService.getProperty(objectCacheConfigPrefix + "spoolPath"))) {
			configBuilder.withSpoolPath(new File(curValue));
		}
	}

	/**
	 * @param configService
	 * @param configBuilder
	 */
	private void implSetupObjectCacheConfigBuilderFromFileItemProperties(
			@NonNull final ConfigurationService configService,
			@NonNull final ObjectCacheConfig.Builder configBuilder,
			@NonNull final String configPrefix) {

		String curValue = null;

		// objectStoreIds
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "fileStoreIds"))) {
			final Set<String> objectStoreIdSet = new LinkedHashSet<>();

			// retrieve valid file store ids from config
			for (String curObjectStoreIdStr : curValue.split(",")) {
				curObjectStoreIdStr = curObjectStoreIdStr.trim();

				if (StringUtils.isNotBlank(curObjectStoreIdStr)) {
					objectStoreIdSet.add(curObjectStoreIdStr.trim());
				}
			}

			configBuilder.withObjectStoreIds(objectStoreIdSet);
		}

		// extract host, port and schema from DB write connection URL
		final Properties dbProperties = new Properties();
		final String writeUrlConfigKey = configPrefix + "writeUrl";

		if (isNotBlank(curValue = configService.getProperty(writeUrlConfigKey))) {
			final int lastSlashPos = curValue.lastIndexOf('/');

			if ((lastSlashPos > -1) && (lastSlashPos < (curValue.length() - 1))) {
				configBuilder.withDBSchema(curValue.substring(lastSlashPos + 1).trim());

				curValue = curValue.substring(0, lastSlashPos);

				final int hostSlashPos = curValue.lastIndexOf('/');

				if ((hostSlashPos > -1) && (hostSlashPos < (curValue.length() - 1))) {
					curValue = curValue.substring(hostSlashPos + 1);

					final int colonPos = curValue.lastIndexOf(':');

					if ((colonPos > -1) && (colonPos < (curValue.length() - 1))) {
						configBuilder.withDBHost(curValue.substring(0, colonPos).trim());

						try {
							configBuilder.withDBPort(Integer.parseInt(curValue.substring(colonPos + 1).trim()));
						} catch (@SuppressWarnings("unused") NumberFormatException e) {
							LOG.error("OC not able to parse port number from given config (url: {})", writeUrlConfigKey);
						}
					} else {
						configBuilder.withDBHost(curValue.trim());
					}
				}
			}
		}

		// writeProperty.*
		try {
			final Map<String, String> dbPropertyMap = configService.getProperties(new WildcardNamePropertyFilter(configPrefix + "writeProperty.*"));

			for (final String curKey : dbPropertyMap.keySet()) {
				final String dbConfigPair = dbPropertyMap.get(curKey);

				if (isNotEmpty(dbConfigPair)) {
					final int assignPos = dbConfigPair.indexOf('=');

					if ((assignPos > 0) && (assignPos < (dbConfigPair.length() - 1))) {
						dbProperties.put(dbConfigPair.substring(0, assignPos), dbConfigPair.substring(assignPos + 1));
					}
				}
			}

			configBuilder.withDBProperties(dbProperties);
		} catch (final OXException e) {
			LOG.trace("OC caught Exception when reading database properties", e);
		}

		// DB user
		final String dbUser = dbProperties.getProperty("user");

		if (null != dbUser) {
			configBuilder.withDBUser(dbUser);
		}

		// DB password
		final String dbPassword = dbProperties.getProperty("password");

		if (null != dbPassword) {
			configBuilder.withDBPassword(dbPassword);
		}

		// database.connectionpool.maxPoolSize
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "connectionpool.maxPoolSize")) && NumberUtils.isParsable(curValue)) {
			final int dbMaxPoolSize = NumberUtils.toInt(curValue);

			if (dbMaxPoolSize > 0) {
				configBuilder.withDBPoolMaxSize(dbMaxPoolSize);
			}
		}

		// database.connectionpool.connectTimeout
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "connectionpool.connectTimeout")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolConnectTimeoutMillis = NumberUtils.toInt(curValue);

			if (dbPoolConnectTimeoutMillis > 0) {
				configBuilder.withDBPoolConnectTimeoutMillis(dbPoolConnectTimeoutMillis);
			}
		}

		// database.connectionpool.idleTimeout
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "connectionpool.idleTimeout")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolIdleTimeoutMillis = NumberUtils.toInt(curValue);

			if (dbPoolIdleTimeoutMillis > 0) {
				configBuilder.withDBPoolIdleTimeoutMillis(dbPoolIdleTimeoutMillis);
			}
		}

		// database.connectionpool.maxLifetime
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "connectionpool.maxLifetime")) && NumberUtils.isParsable(curValue)) {
			final int dbPoolMaxLifeTimeMillis = NumberUtils.toInt(curValue);

			if (dbPoolMaxLifeTimeMillis > 0) {
				configBuilder.withDBPoolMaxLifetimeMillis(dbPoolMaxLifeTimeMillis);
			}
		}

		// spoolPath
		if (isNotBlank(curValue = configService.getProperty(configPrefix + "spoolPath"))) {
			configBuilder.withSpoolPath(new File(curValue));
		}
	}

	/**
	 * @param configService
	 * @param s3StoreConfigId
	 * @return
	 */
	private S3AdapterConfig implGetS3AdapterConfig(
			@NonNull final ConfigurationService configService,
			@NonNull SocketFactory socketFactory,
			final int s3StoreConfigId) {

		final S3AdapterConfigBuilder s3ConfigBuilder = S3AdapterConfig.builder();
		final String s3StoreConfigPrefix = new StringBuilder(256).
				append("com.openexchange.objectcache.objectstore.s3.").append(s3StoreConfigId).append('.').toString();

		String curValue = null;

		// get S3 object store endpoint
		if (null != (curValue = configService.getProperty(s3StoreConfigPrefix + "endpoint"))) {
			s3ConfigBuilder.withEndpoint(curValue);
		}

		// get S3 object store region
		if (null != (curValue = configService.getProperty(s3StoreConfigPrefix + "region", "eu-central-1"))) {
			s3ConfigBuilder.withRegion(curValue);
		}

		// get S3 object store bucket name
		if (null != (curValue = configService.getProperty(s3StoreConfigPrefix + "bucketName"))) {
			s3ConfigBuilder.withBucketName(curValue);
		}

		// get S3 object store access key
		if (null != (curValue = configService.getProperty(s3StoreConfigPrefix + "accessKey"))) {
			s3ConfigBuilder.withAccessKey(curValue);
		}

		// get S3 object store secret key
		if (null != (curValue = configService.getProperty(s3StoreConfigPrefix + "secretKey"))) {
			s3ConfigBuilder.withSecretKey(curValue);
		}

		// set S3 object store socket factory, that has been provided
		s3ConfigBuilder.withSocketFactory(socketFactory);

		// create and return S3AdapterConfig if valid
		final S3AdapterConfig s3AdapterConfig = s3ConfigBuilder.build();

		return s3AdapterConfig.isValid() ? s3AdapterConfig : null;
	}

	/**
	 * @param configService
	 * @param sproxydStoreConfigId
	 * @return
	 */
	private SproxydAdapterConfig implGetSproxydAdapterConfig(
			@NonNull final ConfigurationService configService,
			@NonNull SocketFactory socketFactory,
			final int sproxydStoreConfigId) {

		final SproxydAdapterConfigBuilder sproxydConfigBuilder = SproxydAdapterConfig.builder();
		final String sproxydStoreConfigPrefix = new StringBuilder(256).
				append("com.openexchange.objectcache.objectstore.sproxyd.").append(sproxydStoreConfigId).append('.').toString();

		String curValue = null;

		// get Sproxyd object store endpoint
		if (null != (curValue = configService.getProperty(sproxydStoreConfigPrefix + "endpoint", StringUtils.EMPTY))) {
			curValue = curValue.trim();

			if (curValue.startsWith(HTTP_PREFIX) || curValue.startsWith(HTTPS_PREFIX)) {
				sproxydConfigBuilder.withEndpoint(curValue);
			}
		}

		// get Sproxyd object store path
		if (null != (curValue = configService.getProperty(sproxydStoreConfigPrefix + "path"))) {
			sproxydConfigBuilder.withPath(curValue);
		}

		// get Sproxyd object store max connections
		sproxydConfigBuilder.withMaxConnections(configService.getIntProperty(sproxydStoreConfigPrefix + "maxConnections", 100));

		// get Sproxyd object store connection timeout in milliseconds
		sproxydConfigBuilder.withConnectTimeoutMillis(configService.getIntProperty(sproxydStoreConfigPrefix + "connectTimeout", 5000));

		// get Sproxyd object store connection timeout in milliseconds
		sproxydConfigBuilder.withSocketTimeoutMillis(configService.getIntProperty(sproxydStoreConfigPrefix + "socketTimeout", 30000));

		// get Sproxyd object store socket timeout in milliseconds
		sproxydConfigBuilder.withHeartbeatIntervalMillis(configService.getIntProperty(sproxydStoreConfigPrefix + "heartbeatInterval", 60000));

		// set Sproxyd object store socket factory, that has been provided
		sproxydConfigBuilder.withSocketFactory(socketFactory);

		// create and return SproxydAdapterConfig if valid
		final SproxydAdapterConfig sproxydAdapterConfig = sproxydConfigBuilder.build();

		return sproxydAdapterConfig.isValid() ? sproxydAdapterConfig : null;
	}

	/**
	 * @param configService
	 * @param socketFactory
	 * @param sproxydStoreConfigId
	 * @return
	 */
	private FileAdapterConfig implGetFileAdapterConfig(@NonNull final ConfigurationService configService, final int fileConfigId) {
		final FileAdapterConfigBuilder fileConfigBuilder = FileAdapterConfig.builder();
		final String fileConfigPrefix = new StringBuilder(256).
				append("com.openexchange.objectcache.objectstore.file.").append(fileConfigId).append('.').toString();

		String curValue = null;

		// get root path for filesystem based store
		if (null != (curValue = configService.getProperty(fileConfigPrefix + "rootPath"))) {
			fileConfigBuilder.withRootPath(new File(curValue));
		}

		// create and return FileAdapterConfig if valid
		final FileAdapterConfig fileAdapterConfig = fileConfigBuilder.build();

		return fileAdapterConfig.isValid() ? fileAdapterConfig : null;
	}

	// - Members ---------------------------------------------------------------

	final private Lock m_objectCacheLock = new ReentrantLock();

	final private Condition m_objectCacheCondition = m_objectCacheLock.newCondition();

	final private AtomicBoolean m_running = new AtomicBoolean(true);

	private ServiceTracker<DBMigrationMonitorService, DBMigrationMonitorService> m_dbMigrationMonitorTracker = null;

	private ServiceTracker<ApplicationConfiguration, ApplicationConfiguration> m_applicationConfigurationTracker = null;

	private volatile ObjectCache m_objectCache = null;

	private ObjectCacheServer m_objectCacheServer = null;

	// - Inner classes ---------------------------------------------------------

	/**
	 * {@link ObjectCacheDatabaseAccess}
	 *
	 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
	 * @since v7.10.3

	 */
	private static class ObjectCacheDatabaseAccess implements DatabaseAccess, DBMigrationConnectionProvider {

		/**
		 * {@link ConnectionWrappperCreator}
		 *
		 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
		 * @since v7.10.3

		 * @param <R>
		 */
		private interface ConnectionWrappperCreator {
			ConnectionWrapper create() throws OXException;
		}

		/**
		 * Initializes a new {@link ObjectCacheDatabaseAccess}.
		 * @param objectCacheDatabase
		 */
		ObjectCacheDatabaseAccess(@NonNull final ObjectCacheDatabase objectCacheDatabase) {
			super();
			m_objectCacheDatabase = objectCacheDatabase;
		}

		/**
		 *
		 */
		@Override
		public void createIfAbsent(DatabaseTable... tables) throws OXException {
			final Connection con = acquireWritable();

			if (null == con) {
				return;
			}

			int rollback = 0;

			try {
				Databases.startTransaction(con);
				rollback = 1;

				for (DatabaseTable curTable : tables) {
					if (!Databases.tableExists(con, curTable.getTableName())) {
						try (final PreparedStatement stmt = con.prepareStatement(curTable.getCreateTableStatement(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
							stmt.executeUpdate();
						}
					}
				}

				con.commit();
				rollback = 2;
			} catch (Exception e) {
				throw FileStorageCodes.SQL_PROBLEM.create(e, e.getMessage());
			} finally {
				if (rollback > 0) {
					if (rollback == 1) {
						Databases.rollback(con);
					}

					Databases.autocommit(con);
				}

				releaseWritable(con, false);
			}
		}

		/**
		 *
		 */
		@Override
		public Connection acquireReadOnly() throws OXException {
			return implAcquireConnection(m_readConMap, () -> {
				try {
					return m_objectCacheDatabase.getReadConnectionWrapper();
				} catch (ObjectCacheException e) {
					throw new OXException(e);
				}
			});
		}

		/**
		 *
		 */
		@Override
		public void releaseReadOnly(Connection con) {
			implReleaseConnection(m_readConMap, con);
		}

		/**
		 *
		 */
		@Override
		public Connection acquireWritable() throws OXException {
			return implAcquireConnection(m_writeConMap, () -> {
				try {
					return m_objectCacheDatabase.getWriteConnectionWrapper();
				} catch (ObjectCacheException e) {
					throw new OXException(e);
				}
			});
		}

		/**
		 *
		 */
		@Override
		public void releaseWritable(Connection con, boolean forReading) {
			implReleaseConnection(m_writeConMap, con);
		}

		@Override
		public Connection get() throws OXException {
			return acquireWritable();
		}

		@Override
		public void back(final Connection con) {
			releaseWritable(con, false);
		}

		@Override
		public void backAfterReading(final Connection con) {
			releaseWritable(con, true);
		}

		// - Implementation ----------------------------------------------------

		/**
		 * @param con
		 * @param conMap
		 */
		private synchronized Connection implAcquireConnection(
				@NonNull final Map<Connection, ConnectionWrapper> conMap,
				@NonNull final ConnectionWrappperCreator connectionWrappperCreator ) throws OXException {

			Connection ret = null;

			try {
				final ConnectionWrapper conWrapper = connectionWrappperCreator.create();

				if ((null != conWrapper) && (null != (ret = conWrapper.getConnection()))) {
					// Connection map is synchronized
					conMap.put(ret, conWrapper);
				}
			} catch (Exception e) {
				throw new OXException(e);
			}

			return ret;
		}

		/**
		 * @param con
		 * @param conMap
		 */
		private void implReleaseConnection(@NonNull final Map<Connection, ConnectionWrapper> conMap, final Connection con) {
			if (null != con) {
				// Connection map is synchronized;
				// ConnectionWrapper is closed via try-with-resource after a commit on the connection;
				try (final ConnectionWrapper conWrapper = conMap.remove(con)) {
					if (null != conWrapper) {
						conWrapper.commit();
					}
				} catch (Exception e) {
					ObjectCacheUtils.logExcp(e);
				}
			}
		}

		// - Members -----------------------------------------------------------

		final private Map<Connection, ConnectionWrapper> m_readConMap = Collections.synchronizedMap(new HashMap<>());

		final private Map<Connection, ConnectionWrapper> m_writeConMap = Collections.synchronizedMap(new HashMap<>());

		final private ObjectCacheDatabase m_objectCacheDatabase;
	}

	/**
	 * {@link DBMigrationMonitorCustomizer}
	 *
	 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
	 * @since v7.10.0
	 */
	private class DBMigrationMonitorCustomizer implements ServiceTrackerCustomizer<DBMigrationMonitorService, DBMigrationMonitorService> {

		private final BundleContext m_context;

		/**
		 * Initializes a new {@link DBMigrationMonitorCustomizer}.
		 *
		 * @param parent
		 * @param context
		 */
		DBMigrationMonitorCustomizer(final BundleContext context) {
			m_context = context;
		}

		@Override
		public DBMigrationMonitorService addingService(ServiceReference<DBMigrationMonitorService> reference) {
			if (null != reference) {
				final DBMigrationMonitorService migrationMonitor = m_context.getService(reference);

				if (migrationMonitor != null) {
					Executors.newSingleThreadExecutor().execute(() -> {
						try {
							boolean dbUpdateInProgress = !migrationMonitor.getScheduledFiles().isEmpty();
							if (dbUpdateInProgress) {
								long waitNanos = TimeUnit.SECONDS.toNanos(1L); // 1 second
								do {
									LockSupport.parkNanos(waitNanos);
									dbUpdateInProgress = !migrationMonitor.getScheduledFiles().isEmpty();
								} while (dbUpdateInProgress);
							}
						} catch (@SuppressWarnings("unused") Exception e) {
							// ok
						}

						implRegisterServices();
					});

					return migrationMonitor;
				}

				m_context.ungetService(reference);
			}

			return null;
		}

		@Override
		public void modifiedService(ServiceReference<DBMigrationMonitorService> reference, DBMigrationMonitorService service) {
			//
		}

		@Override
		public void removedService(ServiceReference<DBMigrationMonitorService> reference, DBMigrationMonitorService service) {
			m_context.ungetService(reference);
		}
	}

	/**
	 * {@link ApplicationConfigurationCustomizer}
	 *
	 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
	 * @since v8.0.0
	 */
	private class ApplicationConfigurationCustomizer implements ServiceTrackerCustomizer<ApplicationConfiguration, ApplicationConfiguration> {

		private final BundleContext m_context;

		ApplicationConfigurationCustomizer(final BundleContext context) {
			m_context = context;
		}

		@Override
		public ApplicationConfiguration addingService(ServiceReference<ApplicationConfiguration> reference) {
			final ApplicationConfiguration monitor = (null != reference) ? m_context.getService(reference) : null;

			if (monitor != null) {
				Executors.newSingleThreadExecutor().execute(() -> {
					while (m_running.get() && (null == m_objectCache)) {
						try {
							m_objectCacheLock.lock();
							m_objectCacheCondition.await(1000, TimeUnit.MILLISECONDS);

							if (null != m_objectCache) {
								registerService(ObjectCacheServer.class, m_objectCacheServer = new ObjectCacheServer(m_objectCache));

								LOG.info("OC successfully started REST service: {}", SERVICE_NAME);

								break;
							}
						} catch (@SuppressWarnings("unused") InterruptedException e) {
							// ok
						} catch (Exception e) {
							LOG.error("OC is unable to start REST service {}", SERVICE_NAME, e);
							break;
						} finally {
							m_objectCacheLock.unlock();
						}
					}
				});

				context.ungetService(reference);
			}

			return null;
		}

		@Override
		public void modifiedService(ServiceReference<ApplicationConfiguration> reference, ApplicationConfiguration service) {
		}

		@Override
		public void removedService(ServiceReference<ApplicationConfiguration> reference, ApplicationConfiguration service) {
			m_context.ungetService(reference);
		}
	}
}
