/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl.server;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.base.Throwables;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IReadAccess;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.MutableWrapper;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;
import com.openexchange.objectcache.impl.ObjectCache;
import com.openexchange.objectcache.impl.ObjectCacheMetrics;
import com.openexchange.objectcache.impl.ObjectCacheMetrics.RequestType;
import com.openexchange.objectcache.impl.ObjectCacheUtils;

/**
 * {@link ObjectCacheServer}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
@Path("/objectcache")
@Produces("application/json")
public class ObjectCacheServer {

    /**
     * StatusLocked
     */
    final private static StatusType STATUS_LOCKED = new StatusType() {

        /**
         * STATUS_REASON_LOCKED
         */
        final private static String STATUS_REASON_LOCKED = "Locked";

        @Override
        public int getStatusCode() {
            return 423;
        }

        @Override
        public Family getFamily() {
            return Family.CLIENT_ERROR;
        }

        @Override
        public String getReasonPhrase() {
            return STATUS_REASON_LOCKED;
        }
    };

    /**
     * Initializes a new {@link ObjectCacheServer}.
     * @param serviceLookup
     */
    public ObjectCacheServer(@NonNull final ObjectCache objectCache) {
        super();

        m_objectCache = objectCache;
        m_objectCacheMetrics = objectCache.getObjectCacheMetrics();
    }

    // - Request handler -------------------------------------------------------

    /**
     * @param metrics
     * @return
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @PermitAll
    public String root(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/ request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final String ret = implCreateObjectCacheServerHtmlPage(
            m_running.get() ? (m_objectCache.isValid() ? OBJECTCACHE_SERVER_OK : OBJECTCACHE_SERVER_UNAVAILABLE) : OBJECTCACHE_SERVER_GONE,
            StringUtils.equalsIgnoreCase(metrics, "true") ? m_objectCacheMetrics : null);

        m_objectCacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
}

    /**
     * @param metrics
     * @return
     */
    @GET
    @Path("/status")
    @PermitAll
    public Response status(@QueryParam("metrics") @DefaultValue("false") String metrics) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/status request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implGetStatus("true".equalsIgnoreCase(metrics));

        m_objectCacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @return
     */
    @GET
    @Path("/health")
    @PermitAll
    public Response health() {
        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/health request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implGetStatus(false);

        m_objectCacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @return
     */
    @GET
    @Path("/live")
    @PermitAll
    public Response live() {
        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/live request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implCreateJSONResponse(Status.OK);

        m_objectCacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    /**
     * @return
     */
    @GET
    @Path("/ready")
    @PermitAll
    public Response ready() {
        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/ready request");
        }

        final long requestStartTimeMillis = System.currentTimeMillis();
        final Response ret = implCreateJSONResponse(m_running.get() ?
            (m_objectCache.isValid() ? Status.OK : Status.SERVICE_UNAVAILABLE) :
                Status.GONE);

        m_objectCacheMetrics.incrementAdminRequestCount(RequestType.STATUS, System.currentTimeMillis() - requestStartTimeMillis);

        return ret;
    }

    // - IObjectCache -------------------------------------------------------

    @POST
    @Path("/registerGroup/{group}")
    @Consumes("application/json")
    @PermitAll
    public Response registerGroup(
        @PathParam("group") String group,
        JSONObject jsonGroupConfig) {

        final boolean trace = LOG.isTraceEnabled();

        if (trace) {
            LOG.trace("OC received #/registerGroup request: {}", group);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();

        try {
            if (m_running.get()) {
                if (StringUtils.isNotBlank(group) && (null != jsonGroupConfig)) {
                    final GroupConfig groupConfig = GroupConfig.builder().
                        withGroupId(group).
                        withJSON(jsonGroupConfig).build();

                    if (IdLocker.tryLock(group)) {
                        try {
                            final GroupConfig updatedGroupConfig = m_objectCache.registerGroup(groupConfig);

                            if (null != updatedGroupConfig) {
                                if (trace) {
                                    LOG.trace("OC registered group: {}", group);
                                }

                                return Response.
                                    ok().
                                    type(MediaType.APPLICATION_JSON).
                                    entity(updatedGroupConfig.toJSON()).
                                    build();
                            }

                            return implCreateJSONResponse(Status.INTERNAL_SERVER_ERROR);
                        } catch (ObjectCacheException e) {
                            return implCreateJSONExceptionResponse(e);
                        } finally {
                            IdLocker.unlock(group);
                        }
                    }

                    return implCreateJSONResponse(STATUS_LOCKED);
                }

                return implCreateJSONResponse(Status.BAD_REQUEST);
            }

            return implCreateJSONResponse(Status.GONE);
        } finally {
            m_objectCacheMetrics.incrementRequestCount(group, RequestType.REGISTERGROUP, System.currentTimeMillis() - requestStartTimeMillis);
        }
    }

    @GET
    @Path("/getGroupInfo/{group}")
    @PermitAll
    public Response getGroupInfo(
        @PathParam("group") String group,
        @QueryParam("metrics") @DefaultValue("false") String metrics) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/getGroupInfo request: {}, metrics: {}", group, metrics);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();

        try {
            if (m_running.get()) {
                final Set<String> groupSetToUse = new HashSet<>();

                try {
                    if ((null == group) || StringUtils.equalsIgnoreCase(group, STR_ALL)) {
                        final String[] allGroupArray = m_objectCache.getGroups();

                        if (ArrayUtils.isNotEmpty(allGroupArray)) {
                            for (final String curGroup : allGroupArray) {
                                groupSetToUse.add(curGroup);
                            }
                        }
                    } else {
                        groupSetToUse.add(group);
                    }

                    final JSONArray jsonGroupArray = new JSONArray(groupSetToUse.size());
                    final AtomicInteger curPos = new AtomicInteger(-1);

                    groupSetToUse.forEach((curGroup) -> {
                        final GroupConfig curGroupConfig = m_objectCache.getGroupConfig(curGroup);

                        if (null != curGroupConfig) {
                            try {
                                final JSONObject jsonGroupInfo = curGroupConfig.toJSON();

                                if (StringUtils.equalsIgnoreCase(metrics, STR_TRUE)) {
                                    // add cache info if requested
                                    jsonGroupInfo.put(JSON_KEY_CACHE, new JSONObject().
                                        put(JSON_KEY_CACHE_KEYCOUNT, m_objectCache.getKeyCount(curGroup)).
                                        put(JSON_KEY_CACHE_MAX_KEYAGE_MILLIS, m_objectCache.getMaxKeyAgeMillis(curGroup)).
                                        put(JSON_KEY_CACHE_GROUPLENGTH, m_objectCache.getGroupSize(curGroup)));
                                }

                                jsonGroupArray.add(curPos.incrementAndGet(), jsonGroupInfo);
                            } catch (Exception e) {
                                ObjectCacheUtils.logExcp(e);
                            }
                        }
                    });

                    return Response.
                        ok().
                        type(MediaType.APPLICATION_JSON).
                        entity(new JSONObject().put(JSON_KEY_GROUPS, jsonGroupArray)).
                        build();
                } catch (Exception e) {
                    return implCreateJSONExceptionResponse(e);
                }
            }

            return implCreateJSONResponse(Status.GONE);
        } finally {
            m_objectCacheMetrics.incrementRequestCount(group, RequestType.GETGROUPINFO, System.currentTimeMillis() - requestStartTimeMillis);
        }
    }

    /**
     * @param group
     * @param key
     * @param object
     * @return
     */
    @POST
    @Path("/addObjects/{group}/{key}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response addObjects(
        @PathParam("group") String group,
        @PathParam("key") String key,
        MultiPart multiPart) {

        if (LOG.isTraceEnabled()) {
            LOG.trace("OC received #/addObjects request: {}/{}", group, key);
        }

        final long requestStartTimeMillis = System.currentTimeMillis();

        try {
            if (m_running.get()) {
                if (StringUtils.isNotBlank(group) && StringUtils.isNotBlank(key) && (null != multiPart)) {
                    final AtomicBoolean done = new AtomicBoolean(true);

                    multiPart.getBodyParts().forEach((curPart) -> {
                        final MultivaluedMap<String, String> headers = curPart.getHeaders();
                        final List<String> contentDisposition = headers.get(HEADER_CONTENT_DISPOSITION);

                        for (final String curField : contentDisposition) {
                            final String nameValue = implGetParamValue(curField, HEADER_KEY_NAME);

                            try (final InputStream inputStm = curPart.getEntityAs(InputStream.class)) {
                                if (null != inputStm) {
                                    try (final IWriteAccess writeAcc = m_objectCache.getWriteAccess(group, key, nameValue)) {
                                        FileUtils.copyInputStreamToFile(inputStm, writeAcc.getOutputFile());
                                    }
                                }
                            } catch (final Exception e) {
                                LOG.error("OC caught Exception in #/addObjects", e);
                                done.set(false);
                                break;
                            }
                        }
                    });

                    return implCreateJSONResponse(done.get() ?
                        Status.OK :
                            Status.INTERNAL_SERVER_ERROR);
                }

                return implCreateJSONResponse(Status.BAD_REQUEST);
            }

            return implCreateJSONResponse(Status.GONE);
        } finally {
            m_objectCacheMetrics.incrementRequestCount(group, RequestType.ADDOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
        }
    }

    /**
     * @param group
     * @param key
     * @param objects
     * @return
     */
    @GET
    @Path("/hasObjects/{group}/{key}")
    @PermitAll
    public Response hasObjects(
        @PathParam("group") String group,
        @PathParam("key") String key,
        @QueryParam("objects") Set<String> objects,
        @QueryParam("removeKeyOnError") @DefaultValue("false") boolean removeKeyOnError,
        @QueryParam("removeFileOnError") @DefaultValue("false") boolean removeFileOnError) {

        final long requestStartTimeMillis = System.currentTimeMillis();
        final boolean trace = LOG.isTraceEnabled();

        if (trace) {
            final StringBuilder objectsStrBuilder = new StringBuilder(256);

            objects.forEach((curObjectId) -> objectsStrBuilder.append(curObjectId).append(", "));

            LOG.trace("OC received #/hasObjects request: {}/{}/[{}]",
                group,
                key,
                objectsStrBuilder.substring(0, objectsStrBuilder.length() - 2));
        }

        try {
            if (m_running.get()) {
                if (StringUtils.isNotBlank(group) && StringUtils.isNotBlank(key) && (null != objects) && (objects.size() > 0)) {
                    try {
                        if (m_objectCache.isValid() && (null != objects) && (objects.size() > 0)) {
                            final ICacheObject[] objectsArray = m_objectCache.get(group, key);
                            final boolean consistencyCheck = removeFileOnError || removeKeyOnError;

                            if (ArrayUtils.isNotEmpty(objectsArray)) {
                                int foundCount = 0;

                                for (final String curObject : objects) {
                                    for (final ICacheObject curCacheObject : objectsArray) {
                                        if (curCacheObject.getFileId().equals(curObject)) {
                                            if (consistencyCheck && !implCheckConsistency(curCacheObject, removeFileOnError, removeKeyOnError)) {
                                                // we can leave here in case removeKeyOnError is set to true
                                                //since the whole subgroup with given key has just been removed
                                                if (removeKeyOnError) {
                                                    return implCreateJSONResponse(Status.NOT_FOUND);
                                                }
                                            } else {
                                                ++foundCount;
                                                break;
                                            }
                                        }
                                    }
                                }

                                return implCreateJSONResponse((objects.size() == foundCount) ?
                                    Status.OK :
                                        Status.NOT_FOUND);
                            }
                        }

                        return implCreateJSONResponse(Status.NOT_FOUND);
                    } catch (Exception e) {
                        return implCreateJSONExceptionResponse(e);
                    }
                }

                return implCreateJSONResponse(Status.BAD_REQUEST);
            }

            return implCreateJSONResponse(Status.GONE);
        } finally {
            m_objectCacheMetrics.incrementRequestCount(group, RequestType.HASOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
        }
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param context
     * @return
     */
    @GET
    @Path("/getObjects/{group}/{key}")
    @Produces(MediaType.MULTIPART_FORM_DATA)
    @PermitAll
    public Response getObjects(
        @PathParam("group") String group,
        @PathParam("key") String key,
        @QueryParam("objects") Set<String> objects,
        @QueryParam("removeKeyOnError") @DefaultValue("false") boolean removeKeyOnError,
        @QueryParam("removeFileOnError") @DefaultValue("false") boolean removeFileOnError) {

        final long requestStartTimeMillis = System.currentTimeMillis();
        final boolean trace = LOG.isTraceEnabled();

        if (trace) {
            final StringBuilder objectsStrBuilder = new StringBuilder(256);

            objects.forEach((curObjectId) -> objectsStrBuilder.append(curObjectId).append(", "));

            LOG.trace("OC received #/getObjecst request: {}/{}/[{}]",
                group,
                key,
                objectsStrBuilder.substring(0, objectsStrBuilder.length() - 2));
        }

        try {
            if (m_running.get()) {
                final int requestedObjectCount = (null != objects) ? objects.size() : 0;
                final MutableWrapper<Exception> lastExceptionWrapper = new MutableWrapper<>(null);

                if (StringUtils.isNotBlank(group) && StringUtils.isNotBlank(key) && (requestedObjectCount > 0)) {
                    try (final MultiPart multipartResponse = new MultiPart()) {
                        objects.forEach((curObject) -> {
                            try {
                                if (m_objectCache.contains(group, key, curObject)) {
                                    try (final IReadAccess readAcc = m_objectCache.getReadAccess(group, key, curObject)) {
                                        if (null != readAcc) {
                                            final BodyPart bodyPart = new BodyPart(
                                                new ByteArrayInputStream(FileUtils.readFileToByteArray(readAcc.getInputFile())),
                                                MediaType.APPLICATION_OCTET_STREAM_TYPE);

                                            bodyPart.setContentDisposition(FormDataContentDisposition.name(curObject).fileName(OBJECT_FILENAME).build());
                                            multipartResponse.bodyPart(bodyPart);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                if (removeFileOnError) {
                                    LOG.warn("OC could not get object in #/getObjects call => Removing object {}/{}/{}!)",
                                        group, key, curObject, e);

                                    try {
                                        m_objectCache.remove(group, key, curObject);
                                    } catch (ObjectCacheException e1) {
                                        LOG.error("OC caught Exception", e1);
                                    }
                                } else {
                                    lastExceptionWrapper.set(e);
                                }
                            }
                        });

                        // remove complete subgroup in case of an error and if requested
                        final Exception lastException = lastExceptionWrapper.get();

                        if (null != lastException) {
                            if (removeKeyOnError) {
                                LOG.warn("OC could not get object(s) in #/getObjects call => Removing key (group: {}, key: {})",
                                    group, key, lastException);

                                try {
                                    m_objectCache.removeKey(group, key);
                                } catch (ObjectCacheException e) {
                                    LOG.error("OC caught Exception", e);
                                }
                            } else {
                                LOG.error("OC could not get object(s) in #/getObjects call for key {}/{}!",
                                    group, key, lastException);
                            }
                        }

                        // 'Not Found' status code in case one of the requested objects was not found
                        return (multipartResponse.getBodyParts().size() == requestedObjectCount) ?
                            Response.ok(multipartResponse, MediaType.MULTIPART_FORM_DATA_TYPE).build() :
                                implCreateJSONResponse(Status.NOT_FOUND);
                    } catch (IOException e) {
                        return implCreateJSONExceptionResponse(e);
                    }
                }

                return implCreateJSONResponse(Status.BAD_REQUEST);
            }

            return implCreateJSONResponse(Status.GONE);
        } finally {
            m_objectCacheMetrics.incrementRequestCount(group, RequestType.GETOBJECTS, System.currentTimeMillis() - requestStartTimeMillis);
        }
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("OC shutdown finished");
            }
        }
    }

    // - Implementation --------------------------------------------------------

    private Response implGetStatus(final boolean metrics) {
        final JSONObject jsonStatus = new JSONObject();

        try {
            // general
            jsonStatus.put(JSON_KEY_NAME, JSON_VALUE_OBJECTCACHE);
            jsonStatus.put(JSON_KEY_SERVERID, ObjectCacheUtils.OBJECTCACHE_SERVER_ID);
            jsonStatus.put(JSON_KEY_API, OX_OBJECTCACHE_SERVER_API_VERSION);
            jsonStatus.put(JSON_KEY_APITEXT, JSON_VALUE_APITEXT);
            jsonStatus.put(JSON_KEY_STATUS, (m_running.get() && m_objectCache.isValid()) ? JSON_VALUE_UP : JSON_VALUE_DOWN);

            // ObjectCache status
            jsonStatus.put(JSON_KEY_OBJECTCACHE, m_objectCache.getHealth());

            // metrics on demand
            if (metrics) {
                jsonStatus.put(JSON_KEY_METRICS, implGetMetrics(m_objectCacheMetrics));
            }
        } catch (JSONException e) {
            LOG.error("OC could not create JSON response for status request", e);
        }

        return Response.
            ok().
            type(MediaType.APPLICATION_JSON).
            entity(jsonStatus).
            build();
    }

    /**
     * @param str
     * @param paramName
     * @return
     */
    private static String implGetParamValue(@NonNull final String str, @NonNull final String paramName) {
        final String paramNameToUse = paramName.endsWith("=")  ? paramName : (paramName + "=");
        final int nameStartPos = str.lastIndexOf(paramNameToUse);

        if (nameStartPos > -1) {
            final int namePos =  nameStartPos + paramNameToUse.length();
            final int endNamePos = str.indexOf(';', namePos );

            return StringUtils.strip(str.substring(namePos, (-1 == endNamePos) ? str.length() : endNamePos).trim(), "\"").trim();
        }

        return StringUtils.EMPTY;
    }

    /**
     * @param content
     * @return
     */
    private static String implCreateObjectCacheServerHtmlPage(final String[] content, @Nullable final ObjectCacheMetrics objectCacheMetrics) {
        final StringBuilder htmlBuilder = new StringBuilder(1024);

        htmlBuilder.append("<html>").
            append("<head><meta charset=\"UTF-8\"><title>").append(OBJECTCACHE_SERVER_TITLE).append("</title></head>").
            append("<body><h1 align=\"center\">").append(OBJECTCACHE_SERVER_TITLE).append("</h1>");

        // print server Id in every case
        htmlBuilder.append("<p>Id: ").append(ObjectCacheUtils.OBJECTCACHE_SERVER_ID).append("</p>");

        // print API version in every case
        htmlBuilder.append("<p>API: v").append(OX_OBJECTCACHE_SERVER_API_VERSION).append("</p>");

        if (isNotEmpty(content)) {
            for (int i = 0; i < content.length;) {
                htmlBuilder.append("<p>").append(content[i++]).append("</p>");
            }
        }

        if (null != objectCacheMetrics) {
            htmlBuilder.append("<br />").append("<p><u>Metrics</u></p>");

            for (Method method : objectCacheMetrics.getClass().getMethods())
                if (null != method) {
                    final String methodName = method.getName();

                    if (isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (method.getParameterTypes().length == 0)) {
                        final String metricsPropertyName = methodName.substring(3);
                        final boolean isRatioMetric = metricsPropertyName.endsWith("Ratio");

                        try {
                            final String numberString = new StringBuilder(256).
                                append(method.invoke(objectCacheMetrics)).toString();

                            if (NumberUtils.isParsable(numberString)) {
                                htmlBuilder.append("<p>").append(metricsPropertyName).append(": ");

                                if (isRatioMetric) {
                                    htmlBuilder.append(Double.parseDouble(numberString));
                                } else {
                                    htmlBuilder.append(Long.parseLong(numberString));
                                }

                                htmlBuilder.append("</p>");
                            }
                        } catch (@SuppressWarnings("unused") Exception e) {
                            //
                        }
                    }
                }
        }

        return htmlBuilder.append("</body></html>").toString();
    }

    /**
     * @param objectCacheMetrics
     * @return
     * @throws JSONException
     */
    private JSONObject implGetMetrics(final ObjectCacheMetrics objectCacheMetrics) throws JSONException  {
        final List<Method> methodList = Arrays.asList(objectCacheMetrics.getClass().getMethods());
        final JSONObject jsonMetrics = new JSONObject();

        Collections.sort(methodList, (one, other) -> one.getName().compareTo(other.getName()));

        for (Method curMethod : methodList) {
            if (null != curMethod) {
                final String methodName = curMethod.getName();

                if (isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (curMethod.getParameterTypes().length == 0)) {
                    final String metricsPropertyName = methodName.substring(3);
                    final boolean isRatioMetric = metricsPropertyName.contains("Ratio");

                    try {
                        final String numberString = new StringBuilder().append(curMethod.invoke(objectCacheMetrics)).toString();

                        if (NumberUtils.isParsable(numberString)) {
                            if (isRatioMetric) {
                                jsonMetrics.put(metricsPropertyName, Double.parseDouble(numberString));
                            } else {
                                jsonMetrics.put(metricsPropertyName, Long.parseLong(numberString));
                            }
                        }
                    } catch (@SuppressWarnings("unused") Exception e) {
                        //
                    }
                }
            }
        }

        return jsonMetrics;
    }

    /**
     * @param status
     * @return
     */
    private static Response implCreateJSONResponse(@Nullable final StatusType status) {
        return implCreateJSONResponse(status, null);
    }

    /**
     * @param status
     * @param message
     * @return
     * @throws JSONException
     */
    private static Response implCreateJSONResponse(@Nullable final StatusType status, @Nullable final String message) {
        final StatusType statusToUse = (null != status) ? status : Status.OK;

        return Response.
            status(statusToUse).
            type(MediaType.APPLICATION_JSON).
            entity(implCreateJSONResult(statusToUse.getStatusCode(), (null != message) ? message : statusToUse.getReasonPhrase())).
            build();
    }

    /**
     * @param e
     * @return
     */
    private static Response implCreateJSONExceptionResponse(final Exception e) {
        return Response.
            serverError().
            type(MediaType.APPLICATION_JSON).
            entity(implCreateJSONResult(Status.INTERNAL_SERVER_ERROR.getStatusCode(), Throwables.getRootCause(e).getMessage())).
            build();
    }

    /**
     * @param code
     * @param message
     * @return
     */
    private static JSONObject implCreateJSONResult(final int code, @Nullable final String message) {
        try {
            return new JSONObject().put("code", code).put("message", (null != message) ? message : "n/a");
        } catch (JSONException e1) {
            LOG.error("OC could not create JSON object with code {} and message {}", code, message, e1);
        }

        return new JSONObject();
    }

    /**
     * @param cacheObject
     * @param removeFileOnError
     * @param removeKeyOnError
     * @return
     */
    private boolean implCheckConsistency(
        @NonNull final ICacheObject cacheObject,
        final boolean removeFileOnError,
        final boolean removeKeyOnError) {

        try (final IReadAccess readAcc = m_objectCache.getReadAccess(cacheObject)) {
            // just a simple access test
            if (null != readAcc) {
                return true;
            }

            throw new IOException("OC not able to access object: " + cacheObject.toString());
        } catch (Exception e) {
            if (removeFileOnError) {
                LOG.warn("OC consistency check failed => Removing object {}/{}/{}!",
                    cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), e);

                try {
                    m_objectCache.remove(cacheObject);
                } catch (ObjectCacheException e1) {
                    LOG.error("OC caught Exception", e1);
                }
            } else if (removeKeyOnError) {
                LOG.warn("OC consistency check failed => Removing key {}/{}!",
                    cacheObject.getGroupId(), cacheObject.getKeyId(), e);

                try {
                    m_objectCache.removeKey(cacheObject.getGroupId(), cacheObject.getKeyId());
                } catch (ObjectCacheException e1) {
                    LOG.error("OC caught Exception", e1);
                }
            } else {
                LOG.error("OC consistency check failed for object {}/{}/{}!",
                    cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), e);
            }
        }

        return false;
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private IObjectCache m_objectCache;

    final private ObjectCacheMetrics m_objectCacheMetrics;

    // - Static Members --------------------------------------------------------

    final private static String OBJECT_FILENAME = "object.bin";

    final private static String OX_OBJECTCACHE_SERVER_API_VERSION = "1";

    final private static String OBJECTCACHE_SERVER_TITLE = "Open-Xchange GmbH ObjectCache server";

    final private static String[] OBJECTCACHE_SERVER_OK = { "Code: 0", " Status: running..." };

    final private static String[] OBJECTCACHE_SERVER_GONE = { "Code: 410", " terminated" };

    final private static String[] OBJECTCACHE_SERVER_UNAVAILABLE = { "Code: 503", " Status: unavailable" };

    final private static String HEADER_CONTENT_DISPOSITION = "Content-Disposition";

    final private static String HEADER_KEY_NAME = "name=";

    final private static String JSON_KEY_API = "api";

    final private static String JSON_KEY_APITEXT = "apitext";

    final private static String JSON_KEY_CACHE = "cache";

    final private static String JSON_KEY_CACHE_KEYCOUNT = "keyCount";

    final private static String JSON_KEY_CACHE_MAX_KEYAGE_MILLIS = "maxKeyAgeMillis";

    final private static String JSON_KEY_CACHE_GROUPLENGTH = "groupLength";

    final private static String JSON_KEY_GROUPS = "groups";

    final private static String JSON_KEY_SERVERID = "serverId";

    final private static String JSON_KEY_METRICS = "metrics";

    final private static String JSON_KEY_NAME = "name";

    final private static String JSON_KEY_OBJECTCACHE = "objectCache";

    final private static String JSON_KEY_STATUS = "status";

    final private static String JSON_VALUE_APITEXT = "API: v" + OX_OBJECTCACHE_SERVER_API_VERSION;

    final private static String JSON_VALUE_DOWN = "DOWN";

    final private static String JSON_VALUE_OBJECTCACHE = JSON_KEY_OBJECTCACHE;

    final private static String JSON_VALUE_UP = "UP";

    final private static String STR_ALL = "all";

    final private static String STR_TRUE = "true";
}
