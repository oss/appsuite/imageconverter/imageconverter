/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.objectcache.impl.adapters.file;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import com.google.common.base.Throwables;
import io.micrometer.core.instrument.MeterRegistry.Config;

/**
 * {@link FileAdapterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class FileAdapterConfig {

    /**
     * {@link Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class FileAdapterConfigBuilder {

        /**
         * Initializes a new {@link Builder}.
         */
        public FileAdapterConfigBuilder() {
            super();
        }

        /**
         * @return
         */
        public FileAdapterConfig build() {
            final FileAdapterConfig config = new FileAdapterConfig();

            config.m_rootPath = m_rootPath;

            return config;
        }

        /**
         * @param path
         * @return
         */
        public FileAdapterConfigBuilder withRootPath(final File rootPath) {
            m_rootPath = rootPath;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private File m_rootPath = null;
    }

    /**
     * Initializes a new {@link Config}.
     */
    private FileAdapterConfig() {
        super();
    }

    /**
     * @return
     */
    public static FileAdapterConfigBuilder builder() {
        return new FileAdapterConfigBuilder();
    }

    /**
     * @return <code>true</code> if all config values are set and valid, <code>false</code> otherwise
     */
    public boolean isValid() {
        if (null != m_rootPath) {
            if (m_rootPath.isDirectory() && m_rootPath.canWrite()) {
                return true;
            }

            try {
                FileUtils.forceMkdir(m_rootPath);
                return true;
            } catch (IOException e) {
                LOG.error("OC caught Exception", e);
            }
        }

        return false;
    }

    /**
     * @return
     */
    public File getRootPath() {
        return m_rootPath;
    }

    // - Members ---------------------------------------------------------------

    private File m_rootPath;
}
