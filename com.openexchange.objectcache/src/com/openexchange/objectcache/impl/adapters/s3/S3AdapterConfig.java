/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.objectcache.impl.adapters.s3;

import java.io.File;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.objectcache.impl.adapters.sproxyd.SproxydAdapterConfig;
import io.micrometer.core.instrument.MeterRegistry.Config;

/**
 * {@link SproxydAdapterConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class S3AdapterConfig {

    /**
     * {@link S3ClientSideEncryptionType}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public enum S3ClientSideEncryptionType {
        NONE,
        RSA
    }

    /**
     * {@link Builder}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.0.0
     */
    public static class S3AdapterConfigBuilder {

        /**
         * Initializes a new {@link Builder}.
         */
        public S3AdapterConfigBuilder() {
            super();
        }

        /**
         * @return
         */
        public S3AdapterConfig build() {
            final S3AdapterConfig config = new S3AdapterConfig();

            config.m_endpoint = m_endpoint;
            config.m_region = m_region;
            config.m_accessKey = m_accessKey;
            config.m_secretKey = m_secretKey;
            config.m_bucketName = m_bucketName;
            config.m_cseType = m_cseType;
            config.m_cseRSAKeyStore = m_cseRSAKeyStore;
            config.m_cseRSAPassword = m_cseRSAPassword;
            config.m_pathStyleAccess = m_pathStyleAccess;
            config.m_socketFactory = m_socketFactory;

            return config;
        }

        /**
         * @param endpoint
         * @return
         */
        public S3AdapterConfigBuilder withEndpoint(final String endpoint) {
            m_endpoint = endpoint;
            return this;
        }

        /**
         * @param region
         * @return
         */
        public S3AdapterConfigBuilder withRegion(final String region) {
            m_region = region;
            return this;
        }

        /**
         * @param bucketName
         * @return
         */
        public S3AdapterConfigBuilder withBucketName(final String bucketName) {
            m_bucketName = bucketName;
            return this;
        }

        /**
         * @param accessKey
         * @return
         */
        public S3AdapterConfigBuilder withAccessKey(final String accessKey) {
            m_accessKey = accessKey;
            return this;
        }

        /**
         * @param secretKey
         * @return
         */
        public S3AdapterConfigBuilder withSecretKey(final String secretKey) {
            m_secretKey = secretKey;
            return this;
        }

        /**
         * @param cse
         * @return
         */
        public S3AdapterConfigBuilder withClientSideEncryptionType(final S3ClientSideEncryptionType cse) {
            m_cseType = cse;
            return this;
        }

        /**
         * @param cseRSAKeyStore
         * @return
         */
        public S3AdapterConfigBuilder withClientSideEncryptionRSAKeyStore(final File cseRSAKeyStore) {
            m_cseRSAKeyStore = cseRSAKeyStore;
            return this;
        }

        /**
         * @param cseRSAPassword
         * @return
         */
        public S3AdapterConfigBuilder withClientSideEncryptionRSAPassword(final String cseRSAPassword) {
            m_cseRSAPassword = cseRSAPassword;
            return this;
        }

        /**
         * @param pathStyleAccess
         * @return
         */
        public S3AdapterConfigBuilder withPathStyleAcces(final boolean pathStyleAccess) {
            m_pathStyleAccess = pathStyleAccess;
            return this;
        }

        /**
         * @param socketFactory
         * @return
         */
        public S3AdapterConfigBuilder withSocketFactory(final SocketFactory socketFactory) {
            m_socketFactory = socketFactory;
            return this;
        }

        // - Members ---------------------------------------------------------------

        private String m_endpoint = null;

        private String m_region = "eu-central-1";

        private String m_bucketName = null;

        private String m_accessKey = null;

        private String m_secretKey = null;

        private S3ClientSideEncryptionType m_cseType = S3ClientSideEncryptionType.NONE;

        private File m_cseRSAKeyStore = null;

        private String m_cseRSAPassword = null;

        private boolean m_pathStyleAccess = true;

        private SocketFactory m_socketFactory = SSLSocketFactory.getDefault();
    }

    /**
     * Initializes a new {@link Config}.
     */
    private S3AdapterConfig() {
        super();
    }

    /**
     * @return
     */
    public static S3AdapterConfigBuilder builder() {
        return new S3AdapterConfigBuilder();
    }

    /**
     * @return <code>true</code> if all necessary config values are set, <code>false</code> otherwise
     */
    public boolean isValid() {
        return StringUtils.isNotBlank(m_endpoint) && StringUtils.isNotBlank(m_region) && StringUtils.isNotBlank(m_bucketName) && StringUtils.isNotBlank(m_accessKey) && StringUtils.isNotBlank(m_secretKey);
    }

    /**
     * @return
     */
    public String getEndpoint() {
        return m_endpoint;
    }

    /**
     * @return
     */
    public String getRegion() {
        return m_region;
    }

    /**
     * @return
     */
    public String getBucketName() {
        return m_bucketName;
    }

    /**
     * @return
     */
    public String getAccessKey() {
        return m_accessKey;
    }

    /**
     * @return
     */
    public String getSecretKey() {
        return m_secretKey;
    }

    /**
     * @return
     */
    public S3ClientSideEncryptionType getClientSideEncryptionType() {
        return m_cseType;
    }

    /**
     * @return
     */
    public File getClientSideEncryptionRSAKeyStore() {
        return m_cseRSAKeyStore;
    }

    /**
     * @return
     */
    public String getClientSideEncryptionRSAPassword() {
        return m_cseRSAPassword;
    }

    /**
     * @return
     */
    public boolean isPathStyleAccess() {
        return m_pathStyleAccess;
    }

    /**
     * @return
     */
    public SocketFactory getSocketFactory() {
        return m_socketFactory;
    }

    // - Members ---------------------------------------------------------------

    private String m_endpoint;

    private String m_region;

    private String m_bucketName;

    private String m_accessKey;

    private String m_secretKey;

    private S3ClientSideEncryptionType m_cseType;

    private File m_cseRSAKeyStore;

    private String m_cseRSAPassword;

    private boolean m_pathStyleAccess;

    private SocketFactory m_socketFactory;
}
