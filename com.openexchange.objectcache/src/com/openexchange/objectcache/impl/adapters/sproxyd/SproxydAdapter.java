/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl.adapters.sproxyd;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;
import com.openexchange.objectcache.api.ObjectStoreResult;

import dev.failsafe.Failsafe;
import dev.failsafe.RetryPolicy;
import okhttp3.ConnectionPool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * {@link SproxydAdapter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class SproxydAdapter implements IObjectStore {

	final private static int EXECUTE_ATTEMPT_COUNT = 5;

	final private static int SPROXYDSTORE_PREFIX_NUMBER = 8380;
	final private static long DEFAULT_CONNECTION_POOL_IDLETIME_MILLIS = 600000;
	final private static long CHUNK_SIZE = 1 * 1024 * 1024; // TBD (KA): alternatively to write no chunks: -1;
	final private static String JSON_KEY_CHUNK_INFO = "chunkInfo";
	final private static String JSON_KEY_OBJECT_LENGTH = "objectLength";
	final private static String JSON_KEY_MAX_CHUNK_SIZE = "maxChunkSize";
	final private static String JSON_KEY_CHUNK_NAMES = "chunkNames";
	final private static MediaType MEDIA_TYPE_OCTET_STREAM = MediaType.parse("application/octet-stream");
	final private static MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
	final private static List<String> STR_LIST_EMPTY = new ArrayList<>();

	/**
	 * Unused
	 */
	@SuppressWarnings("unused")
	private SproxydAdapter() {
		super();

		m_config = null;
		m_storeId = 0;
		m_httpClient = null;
		m_baseURLStr = null;
	}

	/**
	 * Initializes a new {@link SproxydAdapter}.
	 * @param config
	 * @param storeId
	 * @throws ObjectCacheException
	 */
	public SproxydAdapter(@NonNull final SproxydAdapterConfig config, final int storeId) throws ObjectCacheException {
		super();

		if (!config.isValid()) {
			throw new ObjectCacheException("OC given SproxyD config is not valid. A valid host must be configured");
		}

		if ((storeId < 1) || (Integer.toString(storeId).length() > 5)) {
			throw new ObjectCacheException("OC given SproxyD store id must be a number between 1 and 99999");
		}

		m_config = config;
		m_storeId = Integer.parseInt(("" + SPROXYDSTORE_PREFIX_NUMBER) + storeId);
		m_baseURLStr = m_config.getBaseURLString();

		try {
			m_httpClient = new OkHttpClient.Builder().
					connectionPool(new ConnectionPool(m_config.getMaxConnections(), DEFAULT_CONNECTION_POOL_IDLETIME_MILLIS, TimeUnit.MILLISECONDS)).
					retryOnConnectionFailure(true).
					connectTimeout(m_config.getConnectTimeoutMillis(), TimeUnit.MILLISECONDS).
					readTimeout(m_config.getSocketTimeoutMillis(), TimeUnit.MILLISECONDS).
					writeTimeout(m_config.getSocketTimeoutMillis(), TimeUnit.MILLISECONDS).
					followRedirects(false).
					followSslRedirects(false).
					build();
		} catch (final Exception e) {
			LOG.error("OC could not create HttpClient", e);
			throw new ObjectCacheException(e);
		}

		LOG.info("OC SproxyD object store successfully initialized (endpoint: {})", m_baseURLStr);
	}

	/**
	 *
	 */
	@Override
	public int getId() {
		return m_storeId;
	}

	/**
	 *
	 */
	@Override
	public String createObject(final File inputFile) throws ObjectCacheException {
		if (null == inputFile) {
			throw new ObjectCacheException("OC input file for object to create must not be null");
		}

		final String objectId = StringUtils.remove(UUID.randomUUID().toString(), '-');

		updateObject(objectId, inputFile);

		return objectId;
	}

	/**
	 *
	 */
	@Override
	public void updateObject(final String objectId, final File inputFile) throws ObjectCacheException {
		if (null == objectId) {
			throw new ObjectCacheException("OC object id for object to update must not be null");
		}

		if (null == inputFile) {
			throw new ObjectCacheException("OC input file for object to update must not be null");
		}

		// non-chunked mode (CHUNK_SIZE <= 0) transfers complete file at once
		if (CHUNK_SIZE <= 0) {
			final Callable<ObjectStoreResult<Void>> putFileCallable = (() -> {
				try (final Response putFileResponse = m_httpClient.newCall(
						implGetRequestBuilder(objectId).put(RequestBody.create(inputFile, MEDIA_TYPE_OCTET_STREAM)).build()).execute()) {

					if (putFileResponse.isSuccessful()) {
						return new ObjectStoreResult<>();
					}

					return new ObjectStoreResult<>(putFileResponse.code());
				} catch (final Exception e) {
					return new ObjectStoreResult<>(e);
				}
			});

			final ObjectStoreResult<Void> curResult = implExecute(putFileCallable);

			if ((null == curResult) || curResult.isNotOk()) {
				// TODO (KA): delete written chunk list?
				throw new ObjectCacheException(curResult.getOrCreateExceptionIfNotOk());
			}

			return;
		}

		// chunked mode write one inventory file as json object and 1+ chunk files
		final List<String> writtenChunkList = new LinkedList<>();
		final long inputFileLen = inputFile.length();
		final long chunkCount = (inputFileLen / CHUNK_SIZE) + (((inputFileLen % CHUNK_SIZE) > 0) ? 1 : 0);
		final AtomicLong bytesWritten = new AtomicLong(0);

		// write single chunks
		for (int chunkPos = 0; chunkPos++ < chunkCount; ) {
			final String chunkName = new StringBuilder(objectId.length() + 16).append(objectId).append('.').append(chunkPos).toString();
			final byte[] chunk = new byte[(int) Math.min(CHUNK_SIZE, inputFileLen - bytesWritten.get())];

			try (final RandomAccessFile raFile = new RandomAccessFile(inputFile, "r")) {
				raFile.seek(bytesWritten.get());
				raFile.read(chunk);

				final Callable<ObjectStoreResult<Void>> putChunkCallable = (() -> {
					try (final Response putChunkResponse = m_httpClient.newCall(
							implGetRequestBuilder(chunkName).put(RequestBody.create(chunk, MEDIA_TYPE_OCTET_STREAM)).build()).execute()) {

						if (putChunkResponse.isSuccessful()) {
							bytesWritten.addAndGet(chunk.length);
							writtenChunkList.add(chunkName);

							return new ObjectStoreResult<>();
						}

						return new ObjectStoreResult<>(putChunkResponse.code());
					} catch (final Exception e) {
						return new ObjectStoreResult<>(e);
					}
				});

				final ObjectStoreResult<Void> curResult = implExecute(putChunkCallable);

				if ((null == curResult) || curResult.isNotOk()) {
					// TODO (KA): delete written chunk list?
					throw new ObjectCacheException(curResult.getOrCreateExceptionIfNotOk());
				}
			} catch (final IOException e) {
				// TODO (KA): delete written chunk list?
				throw new ObjectCacheException(e);
			}
		}

		// write chunk info as JSON object with given objectId
		if (inputFileLen != bytesWritten.get()) {
			// TODO (KA): delete written chunk list?
			throw new ObjectCacheException("OC total size of written chunks does not match expected size");
		}
		try {
			final String jsonContentStr = new JSONObject().
					put(JSON_KEY_CHUNK_INFO, new JSONObject().
							put(JSON_KEY_OBJECT_LENGTH, inputFileLen).
							put(JSON_KEY_MAX_CHUNK_SIZE, CHUNK_SIZE).
							put(JSON_KEY_CHUNK_NAMES, new JSONArray(writtenChunkList))).
					toString();


			final Callable<ObjectStoreResult<Void>> putChunkInfoCallable = (() -> {
				try (final Response putChunkInfoResponse = m_httpClient.newCall(
						implGetRequestBuilder(objectId).put(RequestBody.create(jsonContentStr, MEDIA_TYPE_JSON)).build()).execute()) {

					return putChunkInfoResponse.isSuccessful() ?
							new ObjectStoreResult<>() :
								new ObjectStoreResult<>(putChunkInfoResponse.code());
				} catch (final Exception e) {
					return new ObjectStoreResult<>(e);
				}
			});

			final ObjectStoreResult<Void> curResult = implExecute(putChunkInfoCallable);

			if (curResult.isNotOk()) {
				// TODO (KA): delete written chunk list?
				throw new ObjectCacheException(curResult.getOrCreateExceptionIfNotOk());
			}
		} catch (final JSONException e) {
			// TODO (KA): delete written chunk list?
			throw new ObjectCacheException(e);
		}
	}

	/**
	 *
	 */
	@Override
	public void getObject(final String objectId, final File outputFile) throws ObjectCacheException {
		if (null == objectId) {
			throw new ObjectCacheException("OC object id for object to read must not be null");
		}

		if (null == outputFile) {
			throw new ObjectCacheException("OC object content output file for object to read must not be null and writable");
		}

		final List<ObjectStoreResult<Void>> errorResutList = new ArrayList<>();

		// read single chunks after retrieving the chunk names from JSON chunk info object with given objectId
		implGetObjectChunkNames(objectId).forEach(curChunkName -> {
			final Callable<ObjectStoreResult<Void>> getChunkCallable = (() -> {
				try (final Response getChunkResponse = m_httpClient.newCall(
						implGetRequestBuilder(curChunkName).get().build()).execute()) {

					if (getChunkResponse.isSuccessful()) {
						final ResponseBody getChunkResponseBody = getChunkResponse.body();

						if ((null != getChunkResponseBody) && (getChunkResponseBody.contentLength() > 0)) {
							try (final RandomAccessFile raFile = new RandomAccessFile(outputFile, "rwd")) {
								raFile.seek(outputFile.length());

								try (final InputStream inputStm = getChunkResponseBody.byteStream()) {
									raFile.write(IOUtils.toByteArray(inputStm));
									return new ObjectStoreResult<>();
								}
							}
						}

						return new ObjectStoreResult<>(new ObjectCacheException("OC invalid get chunk content response although response code is ok"));
					}

					return new ObjectStoreResult<>(getChunkResponse.code());
				} catch (final Exception e) {
					return new ObjectStoreResult<>(e);
				}
			});

			final ObjectStoreResult<Void> curResult = implExecute(getChunkCallable);

			if ((null == curResult) || curResult.isNotOk()) {
				errorResutList.add(curResult);
			}
		});

		// throw exception if one of the previous operations did not succeed
		if (!errorResutList.isEmpty()) {
			throw new ObjectCacheException(errorResutList.get(0).getOrCreateExceptionIfNotOk());
		}
	}

	/**
	 *
	 */
	@Override
	public void deleteObject(final String objectId) throws ObjectCacheException {
		if (null == objectId) {
			throw new ObjectCacheException("OC objectId for object to delete must not be null");
		}

		try {
			// delete single chunks after retrieving chunk names from JSON chunk info object with given objectId
			implGetObjectChunkNames(objectId).forEach(curChunkName -> {
				implDelete(curChunkName);
			});
		} catch (@SuppressWarnings("unused") final ObjectCacheException e) {
			// TODO (KA): Trace at least
		}

		// delete chunk info object with given objectId
		implDelete(objectId);
	}

	/**
	 *
	 */
	@Override
	public Set<String> deleteObjects(final String[] objectIds) throws ObjectCacheException {
		if (ArrayUtils.isEmpty(objectIds)) {
			throw new ObjectCacheException("OC object id array for objects to delete must not be null");
		}

		final Set<String> leftOverObjectIdSet = new HashSet<>();

		for (final String curObjectId : objectIds) {
			try {
				deleteObject(curObjectId);
			} catch (@SuppressWarnings("unused") final Exception e) {
				leftOverObjectIdSet.add(curObjectId);
			} finally {
				Thread.yield();
			}
		}

		return leftOverObjectIdSet;
	}

	// - Implementation --------------------------------------------------------

	/**
	 * @param objectI
	 * @return
	 */
	private Request.Builder implGetRequestBuilder(@NonNull final String id) {
		try {
			return new Request.Builder().url(new URL(m_baseURLStr + id));
		} catch (final Exception e) {
			LOG.error("OC caught Exception", e);
		}

		return null;
	}

	/**
	 * @param objectId
	 * @return the list of object chnunk names for the given chunk info object
	 * @throws ObjectCacheException
	 */
	private List<String> implGetObjectChunkNames(@NonNull final String objectId) throws ObjectCacheException  {
		// The Failsafe callable to be executed one or more times up to the retry limit
		final Callable<ObjectStoreResult<List<String>>> getChunkNamesCallable = (() -> {
			try (final Response getInfoResponse = m_httpClient.newCall(
					implGetRequestBuilder(objectId).get().build()).execute()) {

				if (getInfoResponse.isSuccessful()) {
					final ResponseBody responseBody = getInfoResponse.body();

					if ((null != responseBody) && (responseBody.contentLength() > 0)) {
						final JSONObject jsonChunkInfo = new JSONObject(responseBody.string()).optJSONObject(JSON_KEY_CHUNK_INFO);

						if ((null != jsonChunkInfo) && jsonChunkInfo.has(JSON_KEY_CHUNK_NAMES)) {
							return new ObjectStoreResult<>((List) jsonChunkInfo.getJSONArray(JSON_KEY_CHUNK_NAMES).asList());
						}
					}

					return new ObjectStoreResult<>(new ObjectCacheException("OC invalid get chunk name response although response code is ok"));
				}

				return new ObjectStoreResult<>(getInfoResponse.code());
			} catch (final Exception e) {
				return new ObjectStoreResult<>(e);
			}
		});

		final ObjectStoreResult<List<String>> result = implExecute(getChunkNamesCallable);

		if ((null == result) || result.isNotOk() ) {
			throw new ObjectCacheException(result.getOrCreateExceptionIfNotOk());
		}

		return (result.hasResult()) ? result.getResult() : STR_LIST_EMPTY;
	}

	/**
	 * @param elementId
	 * @throws ObjectCacheException
	 */
	private void implDelete(@NonNull final String elementId) {
		// The Failsafe callable to be executed one or more times up to the retry limit
		final Callable<ObjectStoreResult<Void>> deleteCallable = (() -> {
			try (final Response deleteResponse = m_httpClient.newCall(
					implGetRequestBuilder(elementId).delete().build()).execute()) {

				return deleteResponse.isSuccessful() ?
						new ObjectStoreResult<>() :
							new ObjectStoreResult<>(deleteResponse.code());
			} catch (final Exception e) {
				return new ObjectStoreResult<>(e);
			}
		});

		implExecute(deleteCallable);
	}

	/**
	 * @param executor
	 * @return
	 */
	private <T> ObjectStoreResult<T> implExecute(@NonNull final Callable<ObjectStoreResult<T>> execCallable) {
		return Failsafe.with(EXECUTE_RETRY_POLICY).get(() -> execCallable.call());
	}

	// Static members ----------------------------------------------------------

	private static RetryPolicy<Object> EXECUTE_RETRY_POLICY = RetryPolicy.<Object>builder().
			withMaxAttempts(EXECUTE_ATTEMPT_COUNT).
			withBackoff(10, 100, ChronoUnit.MILLIS).
			handleResultIf(result -> ((null == result) || ((ObjectStoreResult<?>) result).isNotOk())).
			build();

	// - Members ---------------------------------------------------------------

	final private SproxydAdapterConfig m_config;

	final private int m_storeId;

	final private String m_baseURLStr;

	final private OkHttpClient m_httpClient;
}
