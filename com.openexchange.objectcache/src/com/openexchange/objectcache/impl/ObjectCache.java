/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IObjectStore;
import com.openexchange.objectcache.api.IReadAccess;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.KeyObject;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheConfig;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ObjectCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class ObjectCache implements IObjectCache {

    final public static Logger LOG = LoggerFactory.getLogger(ObjectCache.class);

    final private static GroupConfig GROUP_CONFIG_EMPTY = GroupConfig.builder().build();
    final private static CacheObject CACHE_OBJECT_EMPTY = new CacheObject(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
    final private static CacheObject[]  CACHE_OBJECT_ARRAY_EMPTY = new CacheObject[0];

    /**
     * Initializes a new {@link ObjectCache}.
     */
    public ObjectCache(
        @NonNull final ObjectCacheConfig config,
        @NonNull final Optional<Set<IObjectStore>> objectStoreSetOpt,
        @NonNull final Optional<ObjectCacheDatabase> databaseOpt) {

        super();

        m_config = config;
        m_objectCacheMetrics = new ObjectCacheMetrics(this);

        // check for valid ObjectStores and database
        final Set<IObjectStore> objectStoreSet = objectStoreSetOpt.isPresent() ? objectStoreSetOpt.get() : null;

        m_database = databaseOpt.isPresent() ? databaseOpt.get() : null;
        m_valid = (null != objectStoreSet) && (objectStoreSet.size() > 0) && hasDatabase();

        if (m_valid) {
            final int objectStoreCount = objectStoreSet.size();
            int curArrayPos = -1;

            m_objectStoreMap = new HashMap<>(objectStoreCount);
            m_objectStoreArray = new IObjectStore[objectStoreCount];

            for (final IObjectStore curObjectStore : objectStoreSetOpt.get()) {
                m_objectStoreArray[++curArrayPos] = curObjectStore;
                m_objectStoreMap.put(curObjectStore.getId(), curObjectStore);
            }

            // read existing GroupConfigs from database and register/start GroupCaches
            final String[] groupIds = m_database.getGroupIds();

            if (null != groupIds) {
                for (final String curGroupId : groupIds) {
                    final GroupConfig curGroupConfig = m_database.getGroupConfig(curGroupId);

                    if ((null != curGroupConfig) && curGroupConfig.isValid()) {
                        try {
                            implRemoveAndRestartGroupCache(curGroupConfig);
                        } catch (final ObjectCacheException e) {
                            ObjectCacheUtils.logExcp(e);
                        }
                    }
                }
            }

            m_objectStoreRemover = new ObjectStoreRemover(this);

            LOG.info("OC - ObjectCache running...");
        } else {
            m_objectStoreArray = null;
            m_objectStoreMap = null;
            m_objectStoreRemover = null;

            LOG.error("OC - ObjectCache not available! [database available: {}, filestore available: {}]",
                (hasDatabase() ? Boolean.TRUE.toString() : Boolean.FALSE.toString()),
                (hasObjectStore() ? Boolean.TRUE.toString() : Boolean.FALSE.toString()));
        }
    }

    // - IObjectCache ------------------------------------------------------

    @Override
    public JSONObject getHealth() {
        final JSONObject jsonHealth = new JSONObject();

        try {
            jsonHealth.
            put(OBJECTCACHE_DATABASE_AVAILABLE, hasDatabase()).
            put(OBJECTCACHE_DATABASE_CONNECTION, new StringBuilder(256).
                append(m_config.getDBHost()).append(':').append(m_config.getDBPort()).
                append('/').append(m_config.getDBSchema()).toString()).
            put(OBJECTCACHE_OBJECTSTORE_AVAILABLE, hasObjectStore()).
            put(OBJECTCACHE_OBJECTSTORE_IDS, new StringBuilder(256).
                append(m_config.getObjectStoreIds()).toString());
        } catch (final JSONException e) {
            LOG.error("OC unable to create health JSON object", e);
        }

        return jsonHealth;
    }

    @Override
    public boolean isValid() {
        return m_valid;
    }

    @Override
    public boolean hasDatabase() {
        return (null != m_database);
    }

    @Override
    public boolean hasObjectStore() {
        return (null != m_objectStoreMap);
    }

    @Override
    public DatabaseType getDatabaseType() {
        return (null != m_database) ?
            m_database.getDatabaseType() :
                DatabaseType.NONE;
    }

    @Override
    public GroupConfig registerGroup(final GroupConfig groupConfig) throws ObjectCacheException {
        if ((null == groupConfig) || !groupConfig.isValid()) {
            throw new ObjectCacheException("OC not able to register group with empty or invalid Config");
        }

        if (hasDatabase()) {
            // register group at database
            final GroupConfig registeredGroupConfig = m_database.registerGroup(groupConfig);

            // add/update returned GroupConfig to available GroupConfig map
            if (null != registeredGroupConfig) {
                implRemoveAndRestartGroupCache(registeredGroupConfig);
            }

            return registeredGroupConfig;
        }

        return null;
    }

    @Override
    public GroupConfig getGroupConfig(final String groupId) {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getGroupConfig(groupId) :
                GROUP_CONFIG_EMPTY;
    }

    @Override
    public void shutdownGroup(final String groupId) {
        if (null != groupId) {
            GroupCache groupCacheToShutdown = null;

            synchronized (m_groupCacheMap) {
                groupCacheToShutdown = m_groupCacheMap.remove(groupId);
            }

            if (null != groupCacheToShutdown) {
                groupCacheToShutdown.shutdown();
            }
        }
    }


    @Override
    public String[] getCustomKeys(final String groupId) throws ObjectCacheException {
        if (hasDatabase() && ObjectCacheUtils.isValid(groupId)) {
            final Set<String> keySet = m_database.getCustomPropertyKeys(groupId);
            return keySet.toArray(new String[keySet.size()]);
        }

        return ArrayUtils.EMPTY_STRING_ARRAY;
    }

    @Override
    public boolean hasCustomKey(final String groupId, final String keyId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId))?
            m_database.hasCustomPropertyKey(groupId, keyId) :
                false;
    }

    @Override
    public JSONObject getUserData(@NonNull final String groupId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getUserData(groupId) :
                JSONObject.EMPTY_OBJECT;
    }

    // -------------------------------------------------------------------------

    @Override
    public boolean containsGroup(final String groupId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.contains(groupId) :
                false;
    }

    @Override
    public boolean containsKey(final String groupId, final String keyId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId)) ?
            m_database.contains(groupId, keyId) :
                false;
    }

    @Override
    public boolean contains(final String groupId, final String keyId, final String fileId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId) && ObjectCacheUtils.isValid(fileId)) ?
            m_database.contains(groupId, keyId, fileId) :
                false;
    }

    // -------------------------------------------------------------------------

    @Override
    public ICacheObject get(final String groupId, final String keyId, final String fileId) throws ObjectCacheException {
        if (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId) && ObjectCacheUtils.isValid(fileId)) {
            final CacheObject cacheObj = new CacheObject(groupId, keyId, fileId);
            final ObjectStoreData objectStoreData = m_database.getObjectStoreData(cacheObj);

            if (ObjectCacheUtils.isValid(objectStoreData)) {
                cacheObj.setObjectStoreData(objectStoreData);
                return cacheObj;
            }
        }

        return CACHE_OBJECT_EMPTY;
    }

    @Override
    public ICacheObject[] get(final String groupId, final String keyId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId)) ?
            m_database.getFileItems(groupId, keyId) :
                CACHE_OBJECT_ARRAY_EMPTY;
    }

    @Override
    public ICacheObject[] get(final String groupId, final Properties properties) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && (null != properties) && (properties.size() > 0)) ?
            m_database.getFileItems(groupId, properties) :
                CACHE_OBJECT_ARRAY_EMPTY;
    }

    // -------------------------------------------------------------------------

    @Override
    public long getKeyCount(final String groupId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getKeyCount(groupId) :
                0;
    }

    @Override
    public String[] getKeys(final String groupId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getKeyIds(groupId) :
                ArrayUtils.EMPTY_STRING_ARRAY;
    }

    @Override
    public long iterateKeysByDescendingAge(final String groupId, final String whereClause, final String limitClause, final Consumer<KeyObject> keyConsumer) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.iterateKeysByDescendingAge(groupId, whereClause, limitClause, keyConsumer) :
                0;
    }

    @Override
    public long getGroupCount() throws ObjectCacheException {
        return hasDatabase() ?
            m_database.getGroupCount() :
                0;
    }

    @Override
    public String[] getGroups() throws ObjectCacheException {
        return hasDatabase() ?
            m_database.getGroupIds() :
                ArrayUtils.EMPTY_STRING_ARRAY;
    }

    // -------------------------------------------------------------------------

    @Override
    public long getGroupSize(final String groupId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getGroupLength(groupId) :
                0;
    }

    @Override
    public long getGroupSize(final String groupId, final Properties properties) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId)) ?
            m_database.getGroupLength(groupId, null, properties) :
                0;
    }

    @Override
    public long getKeySize(final String groupId, final String keyId) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId)) ?
            m_database.getGroupLength(groupId, keyId, null) :
                0;
    }

    @Override
    public long getKeySize(final String groupId, final String keyId, final Properties properties) throws ObjectCacheException {
        return (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId)) ?
            m_database.getGroupLength(groupId, keyId, properties) :
                0;
    }

    @Override
    public long getMaxKeyAgeMillis(final String groupId) throws ObjectCacheException {
        GroupCache groupCache = null;

        synchronized (m_groupCacheMap) {
            groupCache = m_groupCacheMap.get(groupId);
        }

        if (null != groupCache) {
            return groupCache.getMaxKeyAgeMillis();
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    @Override
    public boolean remove(final ICacheObject iFileItem) throws ObjectCacheException {
        final CacheObject cacheObject = (CacheObject) iFileItem;

        if (hasDatabase() && ObjectCacheUtils.isValid(cacheObject)) {
            final String cacheObjectKey = cacheObject.getKey();

            try {
                final List<ObjectStoreData> deletedOjectStoreDataList = new ArrayList<>();

                IdLocker.lock(cacheObjectKey);

                if (1 == m_database.deleteEntry(cacheObject.getGroupId(), cacheObject.getKeyId(), cacheObject.getFileId(), deletedOjectStoreDataList)) {
                    return (m_objectStoreRemover.addObjectStoreDatasToRemove(deletedOjectStoreDataList) > 0);
                }
            } finally {
                IdLocker.unlock(cacheObjectKey);
            }
        }

        return false;
    }

    @Override
    public boolean remove(final String groupId, final String keyId, final String fileId) throws ObjectCacheException {
        return remove(new CacheObject(groupId, keyId, fileId));
    }

    @Override
    public int remove(final ICacheObject[] fileElements) throws ObjectCacheException {
        int removedFileCount = 0;

        if (null != fileElements) {
            for (final ICacheObject curFileItem : fileElements) {
                remove(curFileItem);
                ++removedFileCount;
            }
        }

        return removedFileCount;
    }

    @Override
    public int remove(final String groupId, final Properties properties) throws ObjectCacheException {
        if (hasDatabase() && isNotEmpty(groupId) && (null != properties) && (properties.size() > 0)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(1024);
            final int deletedEntries = m_database.deleteByProperties(groupId, properties, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeKey(final String groupId, final String keyId) throws ObjectCacheException {
        if (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyId)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(64);
            final int deletedEntries = m_database.deleteByKey(groupId, keyId, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeKeys(final String groupId, final String[] keyIds) throws ObjectCacheException {
        if (hasDatabase() && ObjectCacheUtils.isValid(groupId) && ObjectCacheUtils.isValid(keyIds)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(1024);
            final String[] targetKeyIds = Arrays.asList(keyIds).stream().filter(ObjectCacheUtils::isValid).toArray(p -> new String[p]);

            final int deletedEntries = m_database.deleteByGroupKeys(groupId, targetKeyIds, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    @Override
    public int removeGroup(final String groupId) throws ObjectCacheException {
        if (hasDatabase() && ObjectCacheUtils.isValid(groupId)) {
            final List<ObjectStoreData> deletedObjectStoreDataList = new ArrayList<>(8192);
            final int deletedEntries = m_database.deleteByKey(groupId, null, deletedObjectStoreDataList);

            if (deletedEntries > 0) {
                return m_objectStoreRemover.addObjectStoreDatasToRemove(deletedObjectStoreDataList);
            }
        }

        return 0;
    }

    // -------------------------------------------------------------------------

    @Override
    public IReadAccess getReadAccess(final ICacheObject iCacheObject) throws ObjectCacheException {
        final CacheObject cacheObject = (CacheObject) iCacheObject;

        if (ObjectCacheUtils.isInvalid(cacheObject)) {
            throw new ObjectCacheException("Invalid cache object");
        }

        final String cacheObjectKey = cacheObject.getKey();

        try {
            IdLocker.lock(cacheObjectKey);

            final ReadAccess readAcc = new ReadAccess(this, m_database, cacheObject);

            try {
                readAcc.open();
                return readAcc;
            } catch (final Exception e) {
                ObjectCacheUtils.close(readAcc);
                throw new ObjectCacheException(e);
            }
        } finally {
            IdLocker.unlock(cacheObjectKey);
        }
    }

    @Override
    public IReadAccess getReadAccess(final String groupId, final String keyId, final String fileId) throws ObjectCacheException {
        return getReadAccess(new CacheObject(groupId, keyId, fileId));
    }

    @Override
    public IWriteAccess getWriteAccess(final ICacheObject iCacheObject) throws ObjectCacheException {
        final CacheObject cacheObject = (CacheObject) iCacheObject;

        if (ObjectCacheUtils.isInvalid(cacheObject)) {
            throw new ObjectCacheException("Invalid cache object");
        }

        final String cacheObjectKey = cacheObject.getKey();

        try {
            IdLocker.lock(cacheObjectKey);

            final WriteAccess writeAcc = new WriteAccess(this, m_database, cacheObject);

            try {
                writeAcc.open();
                return writeAcc;
            } catch (final Exception e) {
                ObjectCacheUtils.close(writeAcc);
                throw new ObjectCacheException(e);
            }
        } finally {
            IdLocker.unlock(cacheObjectKey);
        }
    }

    @Override
    public IWriteAccess getWriteAccess(final String groupId, final String keyId, final String fileId) throws ObjectCacheException {
        return getWriteAccess(new CacheObject(groupId, keyId, fileId));
    }

    // - Package internal API --------------------------------------------------

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            synchronized (m_groupCacheMap) {
                m_groupCacheMap.forEach((curKey, curGroupCache) -> curGroupCache.shutdown());
                m_groupCacheMap.clear();
            }

            if (null != m_objectStoreRemover) {
                m_objectStoreRemover.shutdown();
            }

            m_objectCacheMetrics.shutdown();
            ObjectCacheUtils.shutdown();
        }
    }

    /**
     * @return
     */
    public ObjectCacheMetrics getObjectCacheMetrics() {
        return m_objectCacheMetrics;
    }

    /**
     * @param objectStoreData
     * @return
     */
    protected IObjectStore getObjectStore(@NonNull final ObjectStoreData objectStoreData) {
        return m_objectStoreMap.get(Integer.valueOf(objectStoreData.getObjectStoreNumber()));
    }

    /**
     * @param cacheObject
     * @return
     * @throws OXException
     */
    protected void getObjectStoreObject(@NonNull final CacheObject cacheObject, @NonNull final File contentFile) throws Exception {
        if (ObjectCacheUtils.isValid(cacheObject)) {
            final ObjectStoreData objectStoreData = cacheObject.getOjectStoreData();

            if (ObjectCacheUtils.isValid(objectStoreData)) {
                final IObjectStore objectStore = getObjectStore(objectStoreData);

                if (null != objectStoreData) {
                    objectStore.getObject(objectStoreData.getObjectStoreId(), contentFile);
                }
            }
        }
    }

    /**
     * @param cacheObject
     * @return
     */
    protected ObjectStoreData createNewObjectStoreObject(@NonNull final ICacheObject cacheObject, @NonNull final File contentFile ) throws IOException {
        final int objectStoreCount = m_objectStoreArray.length;

        if (objectStoreCount > 0) {
            // walk over all available ObjectStores round robin wise and create a new ObjectStore object
            for (int i = 0; i < objectStoreCount; ++i) {
                final IObjectStore objectStore = m_objectStoreArray[implGetNextObjectStorePos()];

                try {
                    // create new ObjectStore object
                    final String objectId = objectStore.createObject(contentFile);

                    if (ObjectCacheUtils.isValid(objectId)) {
                        return new ObjectStoreData(objectStore.getId(), objectId);
                    }
                } catch (final Exception e) {
                    throw new IOException("Creation of new ObjectStore object failed: " + cacheObject.toString(), e);
                }
            }
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected synchronized int implGetNextObjectStorePos() {
        return (m_curObjectStorePos = ++m_curObjectStorePos % m_objectStoreArray.length);
    }

    /**
     * @param groupConfig
     */
    protected void implRemoveAndRestartGroupCache(@NonNull final GroupConfig groupConfig) throws ObjectCacheException {
        final String groupId = groupConfig.getGroupId();

        synchronized (m_groupCacheMap) {
            final GroupCache groupCache = m_groupCacheMap.remove(groupId);

            if (null != groupCache) {
                groupCache.shutdown();

                try {
                    groupCache.join(1000);
                } catch (@SuppressWarnings("unused") final InterruptedException e) {
                    // ok
                }
            }

            m_groupCacheMap.put(groupId, new GroupCache(groupConfig, this));

            // update dynamic metrics registration
            m_objectCacheMetrics.groupChanged(groupId);
        }
    }

    // - Members ---------------------------------------------------------------

    final protected AtomicBoolean m_running = new AtomicBoolean(true);

    final protected ObjectCacheConfig m_config;

    final protected ObjectCacheMetrics m_objectCacheMetrics;

    final protected boolean m_valid;

    final protected ObjectCacheDatabase m_database;

    final protected IObjectStore[] m_objectStoreArray;

    final protected Map<Integer, IObjectStore> m_objectStoreMap;

    final protected ObjectStoreRemover m_objectStoreRemover;

    final protected Map<String, GroupCache> m_groupCacheMap = new HashMap<>();

    protected int m_curObjectStorePos = -1;
}
