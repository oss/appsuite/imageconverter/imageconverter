/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.NonNull;

/**
 * {@link CacheObjectProperties}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
class CacheObjectProperties {

    /**
     * Initializes a new {@link CacheObjectProperties}.
     */
    @SuppressWarnings("unused")
    private CacheObjectProperties() {
        // not to be used
    }

    /**
     * Initializes a new {@link CacheObjectProperties}.
     */
    protected CacheObjectProperties(@NonNull final Set<String> availableCustomKeys) {
        super();
        m_customKeys = availableCustomKeys;
    }

    /**
     * Initializes a new {@link CacheObjectProperties}.
     * @param createDateMillis
     */
    protected CacheObjectProperties(@NonNull final Set<String> availableCustomKeys, final long createDateMillis) {
        this(availableCustomKeys);

        m_createDateMillis = m_accessDateMillis = createDateMillis;

    }

    // - Implementation ------------------------------------------------

    /**
     * @return
     */
    protected boolean isChanged() {
        return !m_propertiesChangedSet.isEmpty();
    }

    /**
     *
     */
    protected void reset() {
        m_propertiesChangedSet.clear();
    }

    /**
     * Gets the m_createDateMillis
     *
     * @return The m_createDateMillis
     */
    protected long getCreateDateMillis() {
        return m_createDateMillis;
    }

    /**
     * Sets the m_accessDateMillis
     *
     * @param createDateMillis The m_createDateMillis to set
     */
    protected void setCreateDateMillis(final long createDateMillis ) {
        if (createDateMillis != m_createDateMillis) {
            m_createDateMillis = createDateMillis;
            m_propertiesChangedSet.add(GroupConfig.KEY_CREATEDATE);
        }
    }

    /**
     * @return
     */
    protected boolean isCreateDateChanged() {
        return m_propertiesChangedSet.contains(GroupConfig.KEY_CREATEDATE);
    }

    /**
     * Gets the m_accessDateMillis
     *
     * @return The m_accessDateMillis
     */
    protected long getModificationDateMillis() {
        return m_accessDateMillis;
    }

    /**
     * Sets the m_accessDateMillis
     *
     * @param accessDateMillis The m_accessDateMillis to set
     */
    protected void setModificationDateMillis(final long accessDateMillis ) {
        if (accessDateMillis != m_accessDateMillis) {
            m_accessDateMillis = accessDateMillis;
            m_propertiesChangedSet.add(GroupConfig.KEY_MODIFICATIONDATE);
        }
    }

    /**
     * @return
     */
    protected boolean isModificationDateChanged() {
        return m_propertiesChangedSet.contains(GroupConfig.KEY_MODIFICATIONDATE);
    }

    /**
     * Gets the m_length
     *
     * @return The m_length
     */
    protected long getLength() {
        return m_length;
    }

    /**
     * Gets the m_length
     *
     * @return The m_length
     */
    protected void setLength(final long length) {
        if (length != m_length) {
            m_length = length;
            m_propertiesChangedSet.add(GroupConfig.KEY_LENGTH);
        }
    }

    /**
     * @return
     */
    protected boolean isLengthChanged() {
        return m_propertiesChangedSet.contains(GroupConfig.KEY_LENGTH);
    }

    /**
     * @param customKey The customKey name
     * @return
     */
    protected String getCustomKeyValue(final String customKey) {
        final String customKeyValue = isNotEmpty(customKey) ? m_properties.get(customKey) : null;

        return (null != customKeyValue) ? customKeyValue : StringUtils.EMPTY;
    }

    /**
     * @param customKey The customKey name
     * @param value The customKey value
     */
    protected void setCustomKeyValue(final String customKey, final String value) {
        if (isNotEmpty(customKey) && m_customKeys.contains(customKey) && !getCustomKeyValue(customKey).equals(value)) {
            m_properties.put(customKey, value);
            m_propertiesChangedSet.add(customKey);
        }
    }

    /**
     * @param customKey The customKey name
     * @return
     */
    protected boolean isCustomKeyValueChanged(final String customKey) {
        return ((isNotEmpty(customKey) && m_customKeys.contains(customKey)) ? m_propertiesChangedSet.contains(customKey) : false);
    }

    /**
     * Gets the available {@link Set} of customKeys at the time of opening an {@link ICacheObject}
     *
     * @return The m_name
     */
    protected Set<String> getCustomKeys() {
        return m_customKeys;
    }

    /**
     * @return The Properties, containing all available key/value pair
     */
    protected Properties getProperties() {
        return implGetProperties(false, false, false);
    }

    /**
     * @return The Properties, containing all available key/value pair
     */
    protected Properties getDefaultProperties() {
        return implGetProperties(false, false, true);
    }

    /**
     * @return The Properties, containing all available customKey/value pair
     */
    protected Properties getCustomProperties() {
        return implGetProperties(false, true, false);
    }

    /**
     * @return The Properties, containing all changed key/value pair
     */
    protected Properties getChangedProperties() {
        return implGetProperties(true, false, false);
    }

    /**
     * @return The Properties, containing all changed customKey/value pair
     */
    protected Properties getChangedDefaultProperties() {
        return implGetProperties(true, false, true);
    }

    /**
     * @return The Properties, containing all changed customKey/value pair
     */
    protected Properties getChangedCustomProperties() {
        return implGetProperties(true, true, false);
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    private Properties implGetProperties(boolean changedOnly, boolean customKeysOnly, boolean defaultKeysOnly) {
        final boolean getAll = !changedOnly;
        final Properties properties = new Properties();

        if (!customKeysOnly || defaultKeysOnly) {
            if (getAll || isCreateDateChanged()) {
                properties.put(GroupConfig.KEY_CREATEDATE, Long.valueOf(getCreateDateMillis()));
            }

            if (getAll || isModificationDateChanged()) {
                properties.put(GroupConfig.KEY_MODIFICATIONDATE, Long.valueOf(getModificationDateMillis()));
            }

            if (getAll || isLengthChanged()) {
                properties.put(GroupConfig.KEY_LENGTH, Long.valueOf(getLength()));
            }
        }

        if (!defaultKeysOnly || customKeysOnly) {
            // get changed customKeys and values
            for (final String curKey : m_customKeys) {
                if (getAll || m_propertiesChangedSet.contains(curKey)) {
                    final String curValue = m_properties.get(curKey);

                    if (isNotEmpty(curValue)) {
                        properties.put(curKey, curValue);
                    }
                }
            }
        }

        return properties;
    }

    // - Members ---------------------------------------------------------------

    protected long m_createDateMillis = 0;

    protected long m_accessDateMillis = 0;

    protected long m_length = 0;

    protected Map<String, String> m_properties = new HashMap<>();

    protected Set<String> m_propertiesChangedSet = new HashSet<>();

    protected Set<String> m_customKeys = null;
}
