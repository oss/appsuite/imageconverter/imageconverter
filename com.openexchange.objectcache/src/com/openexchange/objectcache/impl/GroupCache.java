/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.objectcache.impl;

import static com.openexchange.objectcache.impl.ObjectCache.LOG;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.MutableWrapper;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link GroupCache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class GroupCache extends Thread {

    final private static int MAX_FILE_COUNT = 1000;

    /**
     * Initializes a new {@link GroupCache}.
     * @param config
     * @param objectCache
     * @throws ObjectCacheException
     */
    public GroupCache(@NonNull final GroupConfig config, @NonNull final ObjectCache objectCache) throws ObjectCacheException {
        super("ObjectCacheGroup-" + config.getGroupId());

        m_objectCache = objectCache;

        if (!config.isValid()) {
            throw new ObjectCacheException("Provided GroupConfig instance is not valid");
        }

        m_groupId = config.getGroupId();
        m_maxGroupSize = config.getMaxGroupSize();
        m_maxKeyCount = config.getMaxKeyCount();
        m_keyTimeoutMillis = config.getKeyTimeoutMillis();
        m_cleanupPeriodMillis = config.getCleanupPeriodMillis();

        start();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        LOG.trace("OC GroupCache thread worker started");

        // time to wait for first cleanup, afterwards the configured cleanup period is used
        long cleanupWaitTimeMillis = Math.min(m_cleanupPeriodMillis, 15000);

        while (isRunning()) {
            try {
                sleep(cleanupWaitTimeMillis);
                implPerformCleanup();
                cleanupWaitTimeMillis = m_cleanupPeriodMillis;
            } catch (@SuppressWarnings("unused") final InterruptedException e) {
                LOG.trace("OC GroupCache thread worker received interrupt: " + m_groupId);
            }
        }

        LOG.trace("OC GroupCache thread worker finished: " + m_groupId);
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            long shutdownStartTimeMillis = 0;

            LOG.info("OC GroupCache starting shutdown: " + m_groupId);
            shutdownStartTimeMillis = System.currentTimeMillis();

            interrupt();

            LOG.info(new StringBuilder(256).append("OC GroupCache shutdown finished in ").append(System.currentTimeMillis() - shutdownStartTimeMillis).append("ms: ").append(m_groupId).toString());
        }
    }

    /**
     * isRunning
     */
    public boolean isRunning() {
        return m_running.get();
    }

    /**
     * @return
     */
    public long getMaxKeyAgeMillis() {
        final long oldestKeyTimestampMillis = m_oldestKeyTimestampMillis.get();
        return (oldestKeyTimestampMillis > 0) ? (System.currentTimeMillis() - oldestKeyTimestampMillis) : 0;
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private void implPerformCleanup() {
        final long startTimeMillis = System.currentTimeMillis();
        long countDurationMillis = 0;
        long timeDurationMillis = 0;
        long sizeDurationMillis = 0;
        int countRemoved = 0;
        int timeRemoved = 0;
        int sizeRemoved = 0;

        try {
            countRemoved = implRemoveObsoleteCountBasedKeys();
        } catch (final ObjectCacheException e) {
            LOG.error("OC GroupCache received exception during detection of removable count based keys: {}", m_groupId, e);
        } finally {
            countDurationMillis = (timeDurationMillis = System.currentTimeMillis()) - startTimeMillis;
        }

        try {
            timeRemoved = implRemoveObsoleteTimeBasedKeys();
        } catch (final ObjectCacheException e) {
            LOG.error("OC GroupCache received exception during detection of removable time based keys: {}", m_groupId, e);
        } finally {
            timeDurationMillis = (sizeDurationMillis = System.currentTimeMillis()) - timeDurationMillis;
        }

        try {
            sizeRemoved = implRemoveObsoleteSizeBasedKeys();
        } catch (final ObjectCacheException e) {
            LOG.error("OC GroupCache received exception during detection of removable size based keys: {}", m_groupId, e);
        } finally {
            sizeDurationMillis = System.currentTimeMillis() - sizeDurationMillis;
        }

        try {
            // get age of oldest key for this group
            final MutableWrapper<Long> oldestKeyTimestampMillis = new MutableWrapper<>(Long.valueOf(0));

            if (1 == m_objectCache.iterateKeysByDescendingAge(
                m_groupId,
                null,
                "1",
                curKeyObject -> oldestKeyTimestampMillis.set(curKeyObject.getModificationDate().getTime()))) {

                m_oldestKeyTimestampMillis.set(oldestKeyTimestampMillis.get().longValue());
            }
        } catch (final ObjectCacheException e) {
            LOG.error("OC GroupCache received exception during retrieval of max key age: {}", m_groupId, e);
        }

        // log on INFO or TRACE level, depending on number of removed keys
        final String cleanupLogInfo = new StringBuilder(256).
            append("OC GroupCache cleanup for ").append(m_groupId).
            append(" removed keys based on count/size/time ").append(countRemoved).append('/').append(sizeRemoved).append('/').append(timeRemoved).
            append(" with duration ").append(countDurationMillis).append('/').append(sizeDurationMillis).append('/').append(timeDurationMillis).append("  ms").toString();

        if ((countRemoved > 0) || (timeRemoved > 0) || (sizeRemoved > 0)) {
            LOG.info(cleanupLogInfo);
        } else {
            LOG.trace(cleanupLogInfo);
        }
    }

    /**
     * @return
     * @throws ObjectCacheException
     */
    private int implRemoveObsoleteCountBasedKeys() throws ObjectCacheException {
        int removedKeys = 0;

        if (m_maxKeyCount >= 0) {
            final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
            long curCacheKeyCount = m_objectCache.getKeyCount(m_groupId);

            while (curCacheKeyCount > m_maxKeyCount) {
                final String limitClause = Long.toString(Math.min(curCacheKeyCount - m_maxKeyCount, MAX_FILE_COUNT));

                keyIdSet.clear();
                m_objectCache.iterateKeysByDescendingAge(
                    m_groupId,
                    null,
                    limitClause,
                    curElement -> keyIdSet.add(curElement.getKeyId()));

                final int curKeyCountToRemove = keyIdSet.size();

                if (curKeyCountToRemove <= 0) {
                    // leave in case, we got no items at all
                    break;
                }
                m_objectCache.removeKeys(m_groupId, keyIdSet.toArray(new String[keyIdSet.size()]));

                removedKeys += curKeyCountToRemove;
                curCacheKeyCount -= curKeyCountToRemove;
            }
        }

        return removedKeys;
    }

    /**
     * @return
     * @throws ObjectCacheException
     */
    private int implRemoveObsoleteTimeBasedKeys() throws ObjectCacheException {
        int removedKeys = 0;

        if (m_keyTimeoutMillis >= 0) {
            final long maxKeyTimeMillis = System.currentTimeMillis() - m_keyTimeoutMillis;
            final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
            final String whereClause = "fp.ModificationDate <= " + maxKeyTimeMillis;
            final String limitClause = Integer.toString(MAX_FILE_COUNT);

            // no removal, if keys to remove < 0
            while (true) {
                keyIdSet.clear();
                m_objectCache.iterateKeysByDescendingAge(
                    m_groupId,
                    whereClause,
                    limitClause,
                    curElement -> keyIdSet.add(curElement.getKeyId()));

                final int curKeyCountToRemove = keyIdSet.size();

                if (curKeyCountToRemove <= 0) {
                    // leave in case, we got no items at all
                    break;
                }
                m_objectCache.removeKeys(m_groupId, keyIdSet.toArray(new String[keyIdSet.size()]));
                removedKeys += curKeyCountToRemove;
            }
        }

        return removedKeys;
    }

    /**
     * @return
     * @throws ObjectCacheException
     */
    private int implRemoveObsoleteSizeBasedKeys() throws ObjectCacheException {
        int removedKeys = 0;

        if (m_maxGroupSize >= 0) {
            final long sizeToRemove = m_objectCache.getGroupSize(m_groupId) - m_maxGroupSize;

            if (sizeToRemove > 0) {
                final Set<String> keyIdSet = new LinkedHashSet<>(MAX_FILE_COUNT);
                final String limitClause = Integer.toString(MAX_FILE_COUNT);
                final AtomicLong removedSize = new AtomicLong(0);

                while (removedSize.get() < sizeToRemove) {
                    keyIdSet.clear();
                    m_objectCache.iterateKeysByDescendingAge(
                        m_groupId,
                        null,
                        limitClause,
                        curElement -> {
                            if (removedSize.get() < sizeToRemove) {
                                keyIdSet.add(curElement.getKeyId());
                                removedSize.addAndGet(curElement.getLength());
                            }
                        });

                    final int curKeyCountToRemove = keyIdSet.size();

                    if (curKeyCountToRemove <= 0) {
                        // leave in case, we got no items at all
                        break;
                    }
                    m_objectCache.removeKeys(m_groupId, keyIdSet.toArray(new String[keyIdSet.size()]));
                    removedKeys += curKeyCountToRemove;
                }
            }
        }

        return removedKeys;
    }

    // - Members ---------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicLong m_oldestKeyTimestampMillis = new AtomicLong(0);

    final private IObjectCache m_objectCache;

    final private String m_groupId;

    final private long m_maxGroupSize;

    final private long m_maxKeyCount;

    final private long m_keyTimeoutMillis;

    final private long m_cleanupPeriodMillis;
}
