/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import java.awt.Dimension;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.apache.commons.io.FileUtils;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataException;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imagetransformation.ScaleType;
import com.openexchange.objectcache.api.IWriteAccess;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;

/**
 * {@link ImageConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.2
 */
/**
 * {@link ImageConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
class ImageConverterJob implements Runnable, Closeable {

    /**
     * Initializes a new {@link ImageConverterJob}.
     * @param imageConverter
     * @param imageKey
     * @param inputStm
     * @param configuredImageFormats
     * @param availableImageFormats
     * @param queue
     * @param context
     * @throws ImageConverterException
     */
    ImageConverterJob(
        @NonNull final ImageConverter imageConverter,
        @NonNull final String imageKey,
        @NonNull final InputStream inputStm,
        @NonNull final TreeSet<ImageFormat> configuredImageFormats,
        @NonNull final TreeSet<ImageFormat> availableImageFormats,
        @NonNull final ImageConverterQueue queue,
        @Nullable final String context) throws ImageConverterException {

        super();

        m_imageConverter = imageConverter;
        m_queue = queue;
        m_imageKey = imageKey;
        m_context = context;
        m_inputFile = ImageConverterUtils.createTempFile("ici-");

        if (null == m_inputFile) {
            throw new ImageConverterException("IC server ConversionJob error while creating input file (key: " + imageKey + ")");
        }

        try {
            // create tmp. input file from input stream, file is removed by calling close method
            FileUtils.copyInputStreamToFile(inputStm, m_inputFile);

            if (m_inputFile.length() < 1) {
                throw new IOException("IC server ConversionJob creation of job not possible due to missing or empty input file (key: " + imageKey + ")");
            }

            // read metadata from input file
            m_imageMetadata = implCreateKeyMetadata(queue.getMetadataReader(), m_imageKey, m_inputFile, m_context);

            if (null == m_imageMetadata) {
                throw new MetadataException("IC server ConversionJob error while trying to read image metadata from input file (key: " + imageKey + ")");
            }
        } catch (IOException | MetadataException | ObjectCacheException e) {
            close();
            throw new ImageConverterException("IC server ConversionJob error while creating ImageConverterJob", e);
        }

        m_imageFormats = ImageConverterUtils.createImageFormatTargetList(configuredImageFormats, availableImageFormats, m_imageMetadata);

        if (m_imageFormats.size() > 0) {
            implFillProcessMap();
        }

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server preparing async jobs (key: {}, available: [{}], needed: [{}])",
                imageKey,
                ImageConverterUtils.getImageFormatsListString(availableImageFormats),
                ImageConverterUtils.getImageFormatsListString(m_imageFormats));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() {
        ImageConverterUtils.deleteTempFile(m_inputFile);
        m_inputFile = null;
        m_formatsToProcess.clear();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        final var trace = LOG.isTraceEnabled();
        ImageFormat curTargetFormat = null;

        if (trace) {
            LOG.trace("IC server ConversionJob run started (key: {})", m_imageKey);
        }

        try {
            final List<ImageFormat> workingImageFormatsList = new LinkedList<>();

            // use a working list to allow arbitrary removal of entries from m_formatsToProcess
            // map while iterating over all open working List entries, created from map's keySet
            workingImageFormatsList.addAll(m_formatsToProcess.keySet());

            for (final var iter = workingImageFormatsList.iterator(); iter.hasNext();) {
                curTargetFormat = iter.next();

                if (trace) {
                    LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                        append("ConversionJob worker thread asynchronously processing ").
                        append(m_imageKey).append(": ").
                        append(curTargetFormat.getFormatString()).toString());
                }

                implProcessKey(curTargetFormat, true);
            }
        } catch (final Exception e) {
            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append(m_imageKey).append(": ").
                append((null != curTargetFormat) ? curTargetFormat.getFormatString() :"null").toString(), e);
        } finally {
            ImageConverterUtils.IC_MONITOR.incrementProcessedKeys(m_processingTime);

            if (trace) {
                LOG.trace("IC server ConversionJob run finished (key: {})", m_imageKey);
            }
        }
    }

    /**
     * @return
     */
    public boolean hasFormatsToProcess() {
        return (m_imageFormats.size() > 0);
    }

    /**
     * @return
     */
    public IMetadata getImageMetadata() {
        return m_imageMetadata;
    }

    /**
     * @return
     */
    public String getImageKey() {
        return m_imageKey;
    }

    /**
     *
     */
    public void initMeasurementTime() {
        m_measurementStartTimeMillis = System.currentTimeMillis();
    }

    /**
     * @return
     */
    public long getCurrentMeasurementTimeMillis() {
        return (System.currentTimeMillis() - m_measurementStartTimeMillis);
    }

    /**
     * Process requested
     *
     * @param targetFormat
     * @return
     * @throws ImageConverterException
     */
    protected boolean implProcessKey(@NonNull final ImageFormat targetFormat, final boolean isAsync) throws ImageConverterException {
        var processingFormat = m_formatsToProcess.get(targetFormat);
        final var subProcessingStartTimeMillis = System.currentTimeMillis();
        var ret = false;

        if (null == processingFormat) {
            // check first, if we need to create the reference format entry for the requested ScaleType
            final var referenceFormat = m_referenceFormatMap.get(targetFormat.getScaleType());

            if ((null != referenceFormat) && (null != (processingFormat = m_formatsToProcess.get(referenceFormat)))) {
                ret = implCreateThumbnail(m_imageKey, m_inputFile, m_imageMetadata, referenceFormat, processingFormat, m_context, isAsync);

                if (ret) {
                    m_formatsToProcess.remove(referenceFormat);
                }
            } else {
                ret = true;
            }

            if (ret) {
                implCreatReferenceThumbnail(m_imageKey, m_imageMetadata, targetFormat, referenceFormat, m_context, isAsync);
            }
        } else {
            ret = implCreateThumbnail(m_imageKey, m_inputFile, m_imageMetadata, targetFormat, processingFormat, m_context, isAsync);
        }

        if (ret) {
            m_formatsToProcess.remove(targetFormat);
        }

        m_processingTime += (System.currentTimeMillis() - subProcessingStartTimeMillis);

        return ret;
    }

    /**
     * @param targetFormat
     * @return
     * @throws ImageConverterException
     */
    public MetadataImage processTargetFormat(@NonNull final ImageFormat targetImageFormat) throws ImageConverterException {
        final var formatIsAvailable = !m_formatsToProcess.containsKey(targetImageFormat);

        try {
            // no need to do anything, if requested format has already been processed, otherwise process format
            if (formatIsAvailable || implProcessKey(targetImageFormat, false)) {
                return new MetadataImage(ImageConverterUtils.readImageByTargetFormat(m_queue.getFileItemService(), m_imageKey, targetImageFormat, false), m_imageMetadata);
            }
        } catch (final Exception e) {
            throw new ImageConverterException(ImageConverterUtils.IC_STR_BUILDER().append(m_imageKey).append(" / ").append((null != targetImageFormat) ? targetImageFormat.getFormatString() : "n/a").toString(), e);
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private void implFillProcessMap() {
        // The process map is filled with 3 different kinds of key/value pairs:
        // 1. key and value are (!=null) and same: do a real image processing with the key format and store under key format
        // 2. key and value are (!=null) but different: do real image processing with the value format, but store under key format
        // 3. key is (!=null) and value is (==null): set only the reference property and store under key format without real image processing

        final var imageDimension = m_imageMetadata.getImageDimension();

        for (final ImageFormat curImageFormat : m_imageFormats) {
            final var curScaleType = curImageFormat.getScaleType();
            final var referenceFormat = m_referenceFormatMap.get(curScaleType);

            if (null != referenceFormat) {
                // if a reference format is set, we need to check, if
                // the current format image result matches the
                // reference format result, in order to properly
                // use the reference format
                if (ImageConverterUtils.shouldUseReferenceFormat(imageDimension, curImageFormat)) {
                    m_formatsToProcess.put(curImageFormat, null);
                } else {
                    m_formatsToProcess.put(curImageFormat, curImageFormat);
                }

            } else if (ImageConverterUtils.shouldUseReferenceFormat(imageDimension, curImageFormat)) {
                // the imageFormat at the first reference format position is substituted with the original size format
                final var originalFormat = ImageFormat.createFrom(curImageFormat.getFormatShortName(),
                    true,
                    imageDimension.width,
                    imageDimension.height,
                    curImageFormat.getScaleType(),
                    false,
                    curImageFormat.getQuality());

                m_referenceFormatMap.put(curScaleType, curImageFormat);
                m_formatsToProcess.put(curImageFormat, originalFormat);
            } else {
                m_formatsToProcess.put(curImageFormat, curImageFormat);
            }
        }
    }

    /**
     * @return
     * @throws MetadataException
     */
    private IMetadata implCreateKeyMetadata(
        @NonNull final IMetadataReader metadataReader,
        @NonNull final String imageKey,
        @NonNull final File inputFile,
        @Nullable final String context) throws MetadataException, ObjectCacheException {

        final var ret = metadataReader.readMetadata(inputFile);

        if (null != ret) {
            try (final var fileWriteAcess = m_queue.getFileItemService().getWriteAccess(
                ImageConverterUtils.IMAGECONVERTER_GROUPID,
                imageKey,
                ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID)) {

                if (null != fileWriteAcess) {
                    try (final var printWriter = new PrintWriter(fileWriteAcess.getOutputStream())) {
                        printWriter.write(ret.getJSONObject().toString());
                    }

                    implSetProperties(fileWriteAcess, context, null);

                    if (LOG.isTraceEnabled()) {
                        final var imageDimension = ret.getImageDimension();
                        final var formatName = ret.getImageFormatName();

                        LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                            append("[").append(imageDimension.width).append('x').append(imageDimension.height).append("] ").
                            append(formatName).append(" image: ").
                            append(imageKey).toString());
                    }
                }
            } catch (final Exception e) {
                LOG.error("IC server error while creating metadata entry (key: {})", imageKey, e);

                implRemoveFile(imageKey, ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID);

                throw new ObjectCacheException("IC server caught Exception while trying to read metadata from cache", e);
            }
        }

        return ret;
    }

    /**
     * @param writeAccess
     * @param referencePropertyValue
     * @throws ObjectCacheException
     */
    private static void implSetProperties(@NonNull final IWriteAccess writeAccess, @Nullable final String context, @Nullable final String referencePropertyValue) throws ObjectCacheException {
        if (null != context) {
            writeAccess.setKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_CONTEXT, context);
        }

        if (null != referencePropertyValue) {
            writeAccess.setKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_REFERENCE, referencePropertyValue);
        }
    }

    /**
     * @param targetFormat
     * @param processingFormat
     * @throws ImageConverterException
     */
    private boolean implCreateThumbnail(@NonNull final String imageKey, @NonNull final File inputFile,
        @NonNull final IMetadata imageMetadata,
        @NonNull final ImageFormat targetFormat,
        @NonNull final ImageFormat processingFormat,
        @Nullable final String context,
        final boolean isAsync) throws ImageConverterException {

        final var objectCache = m_queue.getFileItemService();
        final var targetFormatStr = targetFormat.getFormatString();
        final var usedProcessingFormat = (null != processingFormat) ? processingFormat : targetFormat;
        var ret = false;

        try (final var fileWriteAccess = objectCache.getWriteAccess(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, targetFormatStr)) {
            if ((null != fileWriteAccess) && m_queue.getImageProcessor().scale(imageKey, inputFile, fileWriteAccess.getOutputFile(), usedProcessingFormat)) {
                implSetProperties(fileWriteAccess, context, null);
                ret = true;
            }

            if (LOG.isTraceEnabled()) {
                final var imageDimension = imageMetadata.getImageDimension();
                final var imageFormatShortName = imageMetadata.getImageFormatName();
                final var traceMsgBuilder = ImageConverterUtils.IC_STR_BUILDER().
                    append("image processor ").
                    append(isAsync ? "a" : "").append("synchronously").
                    append(ret ? " scaled image to create " : " could not scale image to create ").
                    append(imageFormatShortName).append(" thumbnail for ");

                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(traceMsgBuilder).
                    append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, usedProcessingFormat.getFormatString())).toString());
            }
        } catch (@SuppressWarnings("unused") final Exception e) {
            final var imageDimension = imageMetadata.getImageDimension();
            final var imageFormatShortName = imageMetadata.getImageFormatName();

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("error while ").append(isAsync ? "a" : "").
                append("synchronously creating ").append(imageFormatShortName).append(" thumbnail for ").
                append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, usedProcessingFormat.getFormatString())).toString());
        } finally {
            if (!ret) {
                implRemoveFile(imageKey, targetFormatStr);
            }
        }

        return ret;
    }

    /**
     * @param targetFormat
     * @throws ImageConverterException
     */
    private void implCreatReferenceThumbnail(@NonNull final String imageKey,
        @NonNull final IMetadata imageMetadata,
        @NonNull final ImageFormat targetFormat,
        @NonNull final ImageFormat referenceFormat,
        @Nullable final String context,
        final boolean isAsync) throws ImageConverterException {

        // we create a reference entry in case the shrinkOnly Flag
        // is set and the scaling would expand the source format
        final var targetFormatStr = targetFormat.getFormatString();
        final var referenceFormatStr = referenceFormat.getFormatString();

        try (final var fileWriteAccess = m_queue.
            getFileItemService().
            getWriteAccess(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, targetFormatStr)) {

            implSetProperties(fileWriteAccess, context, referenceFormatStr);

            if (LOG.isTraceEnabled()) {
                final var imageDimension = imageMetadata.getImageDimension();
                final var imageFormatShortName = imageMetadata.getImageFormatName();

                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(isAsync ? "a" : "").
                    append("synchronously created reference entry for ").
                    append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, referenceFormatStr)).toString());
            }
        } catch (@SuppressWarnings("unused") final Exception e) {
            implRemoveFile(imageKey, targetFormatStr);

            final var imageDimension = imageMetadata.getImageDimension();
            final var imageFormatShortName = imageMetadata.getImageFormatName();

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("error while ").append(isAsync ? "a" : "").
                append("synchronously creating reference entry for ").
                append(implGetImageInformationString(imageKey, imageDimension, imageFormatShortName, targetFormatStr, referenceFormatStr)).toString());
        }
    }

    /**
     * @param imageKey
     * @param fileId
     */
    private void implRemoveFile(@NonNull final String imageKey, @NonNull final String fileName) {
        try {
            m_queue.getFileItemService().remove(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey, fileName);
        } catch (final ObjectCacheException e) {
            if (LOG.isTraceEnabled()) {
                LOG.trace(ImageConverterUtils.IC_STR_BUILDER().
                    append(imageKey).append(": ").
                    append(fileName).toString(), e);
            }
        }
    }

    /**
     * @param imageKey
     * @param imageDimension
     * @param imageFormatShortName
     * @param targetFormatStr
     * @param usedProcessingFormat
     * @return
     */
    private StringBuilder implGetImageInformationString(
        final String imageKey,
        final Dimension imageDimension,
        final String imageFormatShortName,
        final String targetFormatStr,
        final String usedProcessingFormatStr) {

        return ImageConverterUtils.STR_BUILDER().
            append("[").append(imageDimension.width).append('x').append(imageDimension.height).append("] ").
            append(imageFormatShortName).append(" image: ").
            append(imageKey).append(" / ").append(targetFormatStr).
            append(" (physicalFormat: ").append(usedProcessingFormatStr).append(')');
    }

    // - Members ---------------------------------------------------------------

    final ImageConverter m_imageConverter;

    final private String m_imageKey;

    final private TreeSet<ImageFormat> m_imageFormats;

    final private ImageConverterQueue m_queue;

    final private String m_context;

    final private IMetadata m_imageMetadata;

    final private Map<ImageFormat, ImageFormat> m_formatsToProcess = Collections.synchronizedMap(new LinkedHashMap<>());

    final private Map<ScaleType, ImageFormat> m_referenceFormatMap = new HashMap<>(ScaleType.values().length);

    private File m_inputFile;

    private long m_measurementStartTimeMillis = 0;

    private long m_processingTime = 0;
}
