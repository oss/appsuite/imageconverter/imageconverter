/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.objectcache.api.NonNull;

/**
 * {@link ImageConverterDirect}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.5
 */
public class ImageConverterDirect {

	/**
	 * Initializes a new {@link ImageConverterDirect}.
	 */
	public ImageConverterDirect(
			@NonNull final ImageConverter imageConverter,
			@NonNull final ImageProcessor imageProcessor,
			@NonNull final IMetadataReader metadataReader) {

		super();

		m_imageConverter = imageConverter;
		m_imageProcessor = imageProcessor;
		m_metadataReader = metadataReader;
	}

	/**
	 * @param sourceImageFile The source image file to be converted
	 * @param targetFormat The target format for the image conversion
	 * @return The converted <code>MetadataImage</code> or <code>null</code> in case of an error
	 * @throws ImageConverterException
	 */
	MetadataImage convert(@NonNull final String imageKey, @NonNull final File sourceImageFile, @NonNull final ImageFormat targetFormat) throws ImageConverterException {
		File tmpTargetImageFile = null;

		try {
			final var imageFormatsRecord = m_imageConverter.getImageFormatsRecord();
			final var imageMetadata = m_metadataReader.readMetadata(sourceImageFile);
			final var requestTargetFormat = ImageConverterUtils.getBestMatchingFormat(
					imageFormatsRecord.configuredImageFormats(),
					imageMetadata,
					targetFormat);

			if (null == requestTargetFormat) {
				throw new ImageConverterException("IC server DirectConverter is not able to parse requested image format: " +
						targetFormat);
			}

			if ((null != (tmpTargetImageFile = ImageConverterUtils.createTempFile("icdo-"))) &&
					m_imageProcessor.scale(imageKey, sourceImageFile, tmpTargetImageFile, implGetProcessingImageFormat(imageMetadata, requestTargetFormat))) {
				return new MetadataImage(new ByteArrayInputStream(FileUtils.readFileToByteArray(tmpTargetImageFile)), imageMetadata);
			}
		} catch (final Exception e) {
			throw new ImageConverterException(ImageConverterUtils.IC_STR_BUILDER().
					append("DIRECT_CONVERSION: ").append(targetFormat).append(" / ").
					append((null != targetFormat) ?
							targetFormat.getFormatString() :
							"null").toString(), e);
		} finally {
			ImageConverterUtils.deleteTempFile(tmpTargetImageFile);
		}

		return null;
	}

	/**
	 * @param inputStm The source image stream to be converted
	 * @param targetFormat The target format for the image conversion
	 * @return The converted <code>MetadataImage</code> or <code>null</code> in case of an error
	 * @throws ImageConverterException
	 */
	MetadataImage convert(@NonNull final String imageKey, @NonNull final InputStream inputStm, @NonNull final ImageFormat targetFormat) throws ImageConverterException {
		File tmpSourceImageFile = null;

		try {
			if (null != (tmpSourceImageFile = ImageConverterUtils.createTempFile("icdi-"))) {
				FileUtils.copyInputStreamToFile(inputStm, tmpSourceImageFile);
				return convert(imageKey, tmpSourceImageFile, targetFormat);
			}
		} catch (final IOException e) {
			throw new ImageConverterException(ImageConverterUtils.STR_BUILDER().
					append("DIRECT_CONVERSION: ").append(targetFormat).append(" / ").
					append((null != targetFormat) ?
							targetFormat.getFormatString() :
							"null").toString(), e);
		} finally {
			ImageConverterUtils.deleteTempFile(tmpSourceImageFile);
		}

		return null;
	}

	// - Implementation --------------------------------------------------------

	/**
	 * @param imageMetadata
	 * @param imageFormat
	 * @return
	 */
	private @NonNull ImageFormat implGetProcessingImageFormat(@NonNull final IMetadata imageMetadata, @NonNull final ImageFormat imageFormat) {
		final var imageFormatShortName = imageMetadata.getImageFormatName();
		final var isTransparent = imageFormatShortName.startsWith("png") || imageFormatShortName.startsWith("tif");

		ImageFormat.ImageType retImageType = null;

        switch (imageFormat.getFormatShortName()) {
            case "jpg": {
                retImageType = ImageFormat.ImageType.JPG;
                break;
            }

            case "png": {
                retImageType = ImageFormat.ImageType.PNG;
                break;
            }

            case "auto":
            default: {
                retImageType = (isTransparent ? ImageFormat.ImageType.PNG : ImageFormat.ImageType.JPG);
                break;
            }
        }

		final ImageFormat ret = new ImageFormat(retImageType);

		ret.setAutoRotate(imageFormat.isAutoRotate());
		ret.setWidth(imageFormat.getWidth());
		ret.setHeight(imageFormat.getHeight());
		ret.setScaleType(imageFormat.getScaleType());
		ret.setShrinkOnly(imageFormat.isShrinkOnly());

		return ret;
	}

	// - Members ---------------------------------------------------------------

	final private ImageConverter m_imageConverter;

	final private ImageProcessor m_imageProcessor;

	final private IMetadataReader m_metadataReader;
}
