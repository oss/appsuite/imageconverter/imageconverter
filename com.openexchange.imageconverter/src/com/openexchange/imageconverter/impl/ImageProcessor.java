/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.IMOps;
import org.im4java.process.ProcessExecutor;
import org.im4java.process.ProcessStarter;
import org.json.JSONException;
import org.json.JSONServices;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.threadpool.AbstractTask;
import com.openexchange.threadpool.ThreadPools;

/**
 * {@link ImageProcessor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageProcessor {

    final private static String KILL_ORPHANED_EXECUTABLE = "/opt/open-xchange/sbin/imageconverter-kill-orphans";
    final private static String KILL_ORPHANED_COMMAND_PATTERN = "convert";
    final private static String IM_OUTPUT_FILE_PATTERN = "oxicim-%d";
    final private static String IM_OUTPUT_FILE_0 = "oxicim-0";
    final private static long KILL_ORPHANED_TIMEOUT_MILLIS = 30000;

    /**
     * {@link ImageProcessorStatus}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.1
     */
    public static class ImageProcessorStatus {

        /**
         * Initializes a new {@link ImageProcessorStatus}.
         * @param isAvailable
         * @param executablePath
         * @param isGraphicsMagick
         */
        public ImageProcessorStatus(final String processorDescription, final boolean isAvailable) {
            m_processorDescription = processorDescription;
            m_isAvailable = isAvailable;
        }

        /**
         * @return
         */
        public String getProcessorDescription() {
            return m_processorDescription;
        }

        /**
         * @return
         */
        public boolean isAvailable() {
            return m_isAvailable;
        }

        // - Members -----------------------------------------------------------

        final private String m_processorDescription;

        final private boolean m_isAvailable;
    }

    /**
     * Initializes a new {@link ImageProcessor}.
     */
    public ImageProcessor(@NonNull final String searchPath, final boolean useGraphicsMagick, @Nullable final String userComment) {
        super();

        ProcessStarter.setGlobalSearchPath(searchPath);

        m_useGraphicsMagick = useGraphicsMagick;
        m_imageProcessorStatus = implGetImageProcessorStatus(searchPath);
        m_userComment = userComment;

        // check for orphaned process cleanup tool
        final var killOrphanedProcessExecutable = new File(KILL_ORPHANED_EXECUTABLE);

        if (killOrphanedProcessExecutable.canExecute()) {
            LOG.info("IC server ImageProcessor is using orphaned process cleanup tool: {}", KILL_ORPHANED_EXECUTABLE);
        } else if (killOrphanedProcessExecutable.exists()) {
            LOG.warn("IC server ImageProcessor found orphaned process cleanup tool, but it is not executable : {}", KILL_ORPHANED_EXECUTABLE);
        } else {
            LOG.warn("IC server ImageProcessor did not find orphaned process cleanup tool: {}", KILL_ORPHANED_EXECUTABLE);
        }
    }

    /**
     *
     */
    public void shutdown() {
        m_killOrphanedProcessTimer.cancel();
        m_processExecutor.shutdownNow();

        try {
            m_processExecutor.awaitTermination(ImageConverterUtils.AWAIT_TERMINATION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        } catch (@SuppressWarnings("unused") final InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * @return
     */
    public ImageProcessorStatus getStatus() {
        return m_imageProcessorStatus;
    }

    /**
     * @param inputFile might be <code>null</code> in which
     *  case a default input image is used for testing purposes.
     * @param outputFile
     * @param imageFormat
     * @return
     * @throws ImageConverterException
     */
    public boolean scale(
        final String imageKey,
        final File inputFile,
        @NonNull final File outputFile,
        @NonNull final ImageFormat imageFormat) throws ImageConverterException {

        if ("auto".equalsIgnoreCase(imageFormat.getFormatShortName())) {
            LOG.error("IC server ImageProcessor needs correct target format! 'auto' format is provided (key: {}, processing: {})", imageKey, imageFormat.getFormatString());
            return false;
        }

        var ret = false;

        if (isAlive()) {
            final var imOp = new IMOperation();
            final var imWidth = ImageConverterUtils.getIM4JExtent(imageFormat.getWidth());
            final var imHeight = ImageConverterUtils.getIM4JExtent(imageFormat.getHeight());
            final var icScaleType = imageFormat.getScaleType();

            // write user comment into target image, if set
            if (null != m_userComment) {
                imOp.comment(m_userComment);
            }

            imOp.addImage();

            if (imageFormat.isAutoRotate()) {
                imOp.autoOrient();
            }

            switch (icScaleType) {
                case CONTAIN: {
                    imOp.resize(imWidth, imHeight, imageFormat.isShrinkOnly() ? Character.valueOf('>') : null);
                    break;
                }

                case CONTAIN_FORCE_DIMENSION: {
                    // http://www.imagemagick.org/Usage/resize/
                    // http://www.imagemagick.org/Usage/thumbnails/#fit_summery
                    imOp.resize(imWidth, imHeight, imageFormat.isShrinkOnly() ? Character.valueOf('>') : null);
                    imOp.gravity("center");
                    imOp.extent(imWidth, imHeight);
                    break;
                }

                case COVER: {
                    imOp.resize(imWidth, imHeight, Character.valueOf('^'));
                    break;
                }

                case COVER_AND_CROP: {
                    // Fill Area Flag ('^' flag)
                    imOp.resize(imWidth, imHeight, Character.valueOf('^'));
                    imOp.gravity("center");
                    imOp.extent(imWidth, imHeight);
                    break;
                }

                case AUTO:
                default:
                    imOp.scale(imWidth, imHeight);
                    break;
            }

            ret = submitTask(imageKey, inputFile, outputFile, imageFormat, imOp);
        }

        return ret;
    }

    /**
     * Perform cleanup of orphaned convert jobs, started as
     * child processes from ImageMagich (e.g. convert-im*)
     */
    public synchronized static void killOrphanedProcess(final boolean force) throws Exception {
        ThreadPools.submitElseExecute(new AbstractTask<Void>() {
            @Override
            public Void call() throws Exception {
                final var killOrphansCmdVector = new Vector<String>();
                final var userName = System.getProperty("user.name");

                killOrphansCmdVector.add(KILL_ORPHANED_EXECUTABLE);
                killOrphansCmdVector.add(userName);
                killOrphansCmdVector.add(KILL_ORPHANED_COMMAND_PATTERN);

                final var procBuilder = new ProcessBuilder(killOrphansCmdVector);

                procBuilder.directory(null);
                procBuilder.redirectErrorStream(false);

                final var killOrphansProcess = procBuilder.start();

                if ((null != killOrphansProcess) && (null != killOrphansProcess.getInputStream())) {
                    try (final var bufferedReader = new BufferedReader(new InputStreamReader(killOrphansProcess.getInputStream()))) {
                        String jsonOutputStr = null;

                        while (null != (jsonOutputStr = bufferedReader.readLine())) {
                            if (jsonOutputStr.length() > 0) {
                                try {
                                    final var jsonObj = JSONServices.parseObject(jsonOutputStr);
                                    final var jsonPidArray = jsonObj.optJSONArray("killedPids");

                                    if (null != jsonPidArray) {
                                        final var killedPidCount = jsonPidArray.length();

                                        if (force || (killedPidCount > 0)) {
                                            final var logKillPidsBuilder = ImageConverterUtils.IC_STR_BUILDER().
                                                append("killed ").
                                                append(killedPidCount).
                                                append(" orphaned convert process(es) for user '").append(userName).
                                                append("' with command pattern '").append(KILL_ORPHANED_COMMAND_PATTERN).
                                                append("' during cleanup: ");

                                            for (var i = 0; i < killedPidCount; ++i) {
                                                final var curPid = jsonPidArray.optString(i);

                                                if (i > 0) {
                                                    logKillPidsBuilder.append(", ");
                                                }

                                                logKillPidsBuilder.append((null != curPid) ? curPid : "n/a");
                                            }

                                            LOG.info(logKillPidsBuilder.toString());
                                        }
                                    }
                                } catch (@SuppressWarnings("unused") final JSONException e) {
                                    // ok, we're only interested in JSON output to parse here
                                }
                            }
                        }
                    } catch (final Exception e) {
                        LOG.error("IC server received Exception when running orphaned process cleanup task", e);
                    }
                }

                return null;
            }
        });
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected boolean isAlive() {
        return !m_processExecutor.isTerminated() && !m_processExecutor.isTerminating();
    }

    /**
     * @param inputFile might be <code>null</code> in which
     *  case a default input image is used for testing purposes.
     * @param outputFile
     * @param imageFormat
     * @param imageOps
     * @return
     * @throws ImageConverterException
     */
    protected boolean submitTask(
        final String imageKey,
        final File inputFile,
        @NonNull final File outputFile,
        @NonNull final ImageFormat imageFormat,
        @NonNull final IMOps imageOps) throws ImageConverterException {

        final var tmpOutputDir = ImageConverterUtils.createTempDir("icd-");

        // always check if temp. directory could be created
        if (null == tmpOutputDir) {
            return false;
        }

        final var convertCmd = new ConvertCmd(m_useGraphicsMagick);
        ImageConverterException rethrowable = null;

        try {
            imageOps.quality(Double.valueOf(imageFormat.getQuality()));
            imageOps.addImage();

            // outputFile is just a file pattern for ImageMagick to create
            // the final output files of the current operation.
            // A pattern is needed for e.g. TIFF multipage images
            final var outputPatternFile = new File(tmpOutputDir, IM_OUTPUT_FILE_PATTERN);
            final var processTask = convertCmd.getProcessTask(imageOps,
                (null != inputFile) ? inputFile.getAbsolutePath() : "logo:",
                    ImageConverterUtils.STR_BUILDER().
                    append(imageFormat.getFormatShortName()).
                    append(':').
                    append(outputPatternFile.getAbsolutePath()).toString());

            if (null != processTask) {
                final Future<?> future = m_processExecutor.submit(processTask);

                try {
                    if (null != future.get(ImageConverterConfig.IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)) {
                        rethrowable = new ImageConverterException("IC server error while executing IM4Java ProcessTask");
                    } else {
                        final var firstSequenceFile = new File(tmpOutputDir, IM_OUTPUT_FILE_0);

                        // copy first result file with name IM_OUTPUT_FILE_0
                        // within currently used tmpOutputDir to final output
                        // file. Watch for valid sequence file as e.g.
                        // GraphicsMagick doesn't create sequences at all by default
                        FileUtils.copyFile(firstSequenceFile.canRead() ? firstSequenceFile : outputPatternFile, outputFile);
                    }
                } catch (@SuppressWarnings("unused") final TimeoutException e) {
                    processTask.cancel(true);

                    // copy input file to error directory, if configured
                    if (null != ImageConverterConfig.IMAGECONVERTER_ERROR_PATH) {
                        try {
                            FileUtils.copyFile(inputFile, new File(ImageConverterConfig.IMAGECONVERTER_ERROR_PATH, inputFile.getName()));
                        } catch (final Exception e1) {
                            LOG.error("IC server received Exception (key: {})", imageKey, e1);
                        }
                    }

                    LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                        append("conversion timeout error while executing IM4Java ProcessTask after ").
                        append(ImageConverterConfig.IMAGECONVERTER_CONVERT_TIMEOUT_MILLIS).append("ms (key: {})").toString(), imageKey);

                    // call orphaned process kill task after timer delay, if a kill orphans task is not yet running
                    if (m_killOrphanedProcessScheduled.compareAndSet(false, true)) {
                        m_killOrphanedProcessTimer.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                try {
                                    m_killOrphanedProcessScheduled.set(false);
                                    killOrphanedProcess(false);
                                } catch (final Exception e) {
                                    LOG.error("IC server received Exception when calling orphaned process cleanup task", e);
                                }
                            }
                        }, KILL_ORPHANED_TIMEOUT_MILLIS);
                    }

                    return false;
                } catch (final IOException e) {
                    rethrowable = new ImageConverterException("IC server error while copying IM4Java transformation result", e);
                }
            }
        } catch (final IM4JavaException e) {
            rethrowable = new ImageConverterException("IC server error while creating IM4Java ProcessTask", e);
        } catch (final IOException e) {
            rethrowable = new ImageConverterException("IC server error while creating IM4Java ProcessTask", e);
        } catch (final ExecutionException e) {
            rethrowable = new ImageConverterException("IC server error while running IM4Java ProcessTask", e);
        } catch (@SuppressWarnings("unused") final InterruptedException e) {
            LOG.trace("IC server interrupted while running IM4Java ProcessTask (key: {})", imageKey);
        } catch (@SuppressWarnings("unused") final RejectedExecutionException e) {
            LOG.trace("IC server rejected execution while running IM4Java ProcessTask (key: {})", imageKey);
        } finally {
            ImageConverterUtils.deleteTempFile(tmpOutputDir);
        }

        if (null != rethrowable) {
            throw rethrowable;
        }

        return true;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param searchPath
     * @return
     */
    ImageProcessorStatus implGetImageProcessorStatus(@NonNull final String searchPath) {
        var convertExecutable = new File("n/a");
        var isAvailable = false;

        if (StringUtils.isNotBlank(searchPath)) {
            isAvailable = (convertExecutable = new File(searchPath, "convert")).canExecute() &&
                (!m_useGraphicsMagick || (convertExecutable = new File(searchPath, "gm")).canExecute());
        }

        if (!isAvailable) {
            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("is not able to detect a valid ").
                append(m_useGraphicsMagick ? "GraphicsMagick 'gm'" : "ImageMagick 'convert' ").
                append("executable at configured search directory: ").append(searchPath).toString());
        } else {
            LOG.info(ImageConverterUtils.IC_STR_BUILDER().
                append("is using ").
                append(m_useGraphicsMagick ? "GraphicsMagick 'gm'" : "ImageMagick 'convert' ").
                append("executable located at: ").append(convertExecutable).toString());
        }

        return new ImageProcessorStatus(isAvailable ? convertExecutable.getPath() : "n/a", isAvailable);
    }

    // - Members ---------------------------------------------------------------

    final private ProcessExecutor m_processExecutor = new ProcessExecutor();

    final private boolean m_useGraphicsMagick;

    final private ImageProcessorStatus m_imageProcessorStatus;

    final private String m_userComment;

    // - Static Members --------------------------------------------------------

    final private static Timer m_killOrphanedProcessTimer = new Timer();

    final private static AtomicBoolean m_killOrphanedProcessScheduled = new AtomicBoolean(false);
}
