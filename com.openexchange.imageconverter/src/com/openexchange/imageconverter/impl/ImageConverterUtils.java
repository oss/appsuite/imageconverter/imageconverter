/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.json.JSONServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.annotation.NonNull;
import com.openexchange.annotation.Nullable;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.ObjectCacheException;
import dev.failsafe.Failsafe;
import dev.failsafe.RetryPolicy;

/**
 * {@link ImageConverterUtils}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class ImageConverterUtils {

    public static ImageConverterMBean IC_MBEAN = null;

    public static ImageConverterMetrics IC_METRICS = null;

    public static ImageConverterMonitoring IC_MONITOR = null;


    final public static Logger LOG = LoggerFactory.getLogger(ImageConverter.class);

    /**
     * !!! Do not change !!!
     */
    final public static String IMAGECONVERTER_METADATA_FILEID = "OX_ICMETA";

    /**
     * !!! Do not change !!!
     */
    final public static String IMAGECONVERTER_KEY_CONTEXT = "CTX";

    /**
     * !!! Do not change !!!
     */
    final public static String IMAGECONVERTER_KEY_REFERENCE = "REF";

    /**
     * In order to initialize both following variables, the method has to be called as
     * soon as the ImageConverter singleton has been created
     * (preferable from the Ctor of the ImageConverter singleton itself)
     */
    public static ImageConverter IC = null;

    final public static String[] IMAGECONVERTER_KEYS = { IMAGECONVERTER_KEY_CONTEXT, IMAGECONVERTER_KEY_REFERENCE };

    final public static String IMAGECONVERTER_GROUPID = "OX_IC";

    final public static int MAX_IMAGE_EXTENT = 8192;

    final public static long AWAIT_TERMINATION_TIMEOUT_MILLIS = 2000;

    final private static int FILE_DELETE_ATTEMPTS = 5;

    /**
     * Initializes a new {@link ImageConverterUtils}.
     */
    private ImageConverterUtils() {
        super();
    }

    /**
     * This method needs to be called as soon as an ImageConverter has been created
     *
     * @param imageConverter
     */
    public static void init(@NonNull final ImageConverter imageConverter) {
        IC_MONITOR = new ImageConverterMonitoring(IC = imageConverter);
        IC_METRICS = new ImageConverterMetrics(IC_MONITOR);
    }

    /**
     * @return
     */
    public static StringBuilder STR_BUILDER() {
        return new StringBuilder(256);
    }

    /**
     * @return
     */
    public static StringBuilder IC_STR_BUILDER() {
        return STR_BUILDER().append("IC server ");
    }

    /**
     * @return
     */
    public static @Nullable File createTempFile(@Nullable final String prefix) {
        try {
            return File.createTempFile(((null != prefix) ? (prefix +  "_")  : StringUtils.EMPTY) + TEMP_COUNTER.incrementAndGet(), ".tmp",
                ImageConverterConfig.IMAGECONVERTER_SPOOLPATH);
        } catch (final IOException e) {
            LOG.error("IC server can not create temp. file", e);
        }

        return null;
    }

    /**
     * @return
     */
    public static @Nullable File createTempDir(@Nullable final String prefix) {
        try {
            final var tmpDirPath = Files.createTempDirectory(ImageConverterConfig.IMAGECONVERTER_SPOOLPATH.toPath(),
                ((null != prefix) ? prefix : StringUtils.EMPTY) + TEMP_COUNTER.incrementAndGet());

            if (null != tmpDirPath) {
                return tmpDirPath.toFile();
            }
        } catch (final Exception e) {
            LOG.error("IC server can not create temp. directory", e);
        }

        return null;

    }

    /**
     * @param fileToDelete
     */
    public static void deleteTempFile(@Nullable final File fileToDelete) {
        if (null != fileToDelete) {
            Failsafe.with(RETRY_POLICY_DELETE_FILE).
            onFailure(event -> LOG.error("IC server was not able to delete temp. file after {}/{} attempts within {}ms: {}",
                event.getAttemptCount(), FILE_DELETE_ATTEMPTS,
                event.getElapsedTime().toMillis(),
                fileToDelete.getAbsolutePath(), event.getException())).
            onSuccess(event -> {
                final var attemptCount = event.getAttemptCount();

                if (attemptCount > 1) {
                    LOG.warn("IC server deleted temp. file after {} attempts within {}ms: {}",
                        attemptCount, event.getElapsedTime().toMillis(), fileToDelete.getAbsolutePath());
                }
            }).
            run(() -> {
                if (fileToDelete.exists()) {
                    if (!fileToDelete.canWrite()) {
                        throw new IOException("IC server can not delete write protected temp. file: " + fileToDelete.getAbsolutePath());
                    }
                    FileUtils.forceDelete(fileToDelete);

                    Thread.yield();

                    if (fileToDelete.exists()) {
                        // throw exception if file still exists
                        throw new IOException("IC server could not delete temp. file althogh no Exception has been thrown: " + fileToDelete.getAbsolutePath());
                    }
                }
            });
        }
    }

    /**
     * @param availableFormats
     * @param requestFormat
     * @return
     */
    public static ImageFormat getBestMatchingFormat(@NonNull final TreeSet<ImageFormat> imageFormats, @NonNull final IMetadata imageMetadata, @NonNull final ImageFormat requestFormat) {
        final var trace = LOG.isTraceEnabled();
        final var imageFormatTargetList = createImageFormatTargetList(imageFormats, null, imageMetadata);
        ImageFormat ret = null;

        if (imageFormatTargetList.size() > 0) {
            // Type safety warning has been checked!
            @SuppressWarnings("unchecked")
            final var candidateSet = (TreeSet<ImageFormat>) EMPTY_CANDIDATE_SET.clone();
            final var curTestSet = (BitSet) m_paramTestSet.clone();
            final var imageDimension = imageMetadata.getImageDimension();
            final List<ImageFormat> workingImageFormatsList = new ArrayList<>(imageFormatTargetList.size());

            workingImageFormatsList.addAll(imageFormatTargetList);

            if (requestFormat.getWidth() <= 0) {
                requestFormat.setWidth(imageDimension.width);
            }

            if (requestFormat.getHeight() <= 0) {
                requestFormat.setHeight(imageDimension.height);
            }

            // iterate over all left entries within the imageFormatList
            // and add all entries, whose to be tested parameters, defined
            // in the test bit set, match the equivalent requestFormat parameters;
            // leave the loop, if the candidate list contains at least one entry;
            // calling #implCreateImageFormatCandidateList with an empty
            // test set adds all entries from the current imageFormatList
            do {
                implFillImageFormatCandidateList(workingImageFormatsList, curTestSet, requestFormat, candidateSet);

                if (curTestSet.isEmpty()) {
                    break;
                }

                // reduce the to be test parameters set by the
                // last entry, the set entries are added
                // with decreasing performance;
                // this means, that with each iteration within
                // this loop, the set of parameters that must
                // match is reduced, getting at least one
                // entry within the candidate list
                curTestSet.clear(curTestSet.length() - 1);
            } while (candidateSet.isEmpty());

            if (!candidateSet.isEmpty()) {
                final var requestDimension = getResultingImageDimension(imageDimension, requestFormat);

                // iterate over all target formats from the
                // candidate set and find best matching
                for (final @NonNull ImageFormat curTargetFormat : candidateSet) {
                    final var curResultingTargetDimension = getResultingImageDimension(imageDimension, curTargetFormat);

                    // search until candidates' format dimension is equal
                    // or larger than the requests' format dimension
                    if ((curResultingTargetDimension.width >= requestDimension.width) &&
                        (curResultingTargetDimension.height >= requestDimension.height)) {

                        ret = curTargetFormat;
                        break;
                    }
                }

                if (null == ret) {
                    // take largest possible dimension, in case no candidate matched so far
                    ret = candidateSet.last();
                }
            }

            if (null == ret) {
                ret = imageFormatTargetList.last();

                // this should not happen at all, if a valid ImageFormat list is provided
                LOG.error("IC server found no best matching image format (requested: {}, returning: {})",
                    requestFormat.getFormatString(), ret.getFormatString());
            }

            if (trace) {
                LOG.trace("IC server found best matching image format (requested: {}, returning: {})",
                    requestFormat.getFormatString(), ret.getFormatString());
            }
        } else {
            LOG.warn("IC server image format list has no valid entries => please check configuration of imageconverter.properties");
        }

        if (ret != null) {
            ImageConverterUtils.IC_MONITOR.registerAndIncreaseImageFormatRequestCount(requestFormat, ret.getWidth(), ret.getHeight(), imageMetadata.getImageFormatName());
        }
        return ret;
    }

    /**
     * @param imageFormat
     * @return
     */
    public static ImageFormat makeImageFormatUnique(@NonNull final ImageFormat imageFormat) {
        final var ret = new ImageFormat();

        ret.setImageType(imageFormat.getImageType());
        ret.setAutoRotate(imageFormat.isAutoRotate());
        ret.setWidth(imageFormat.getWidth());
        ret.setHeight(imageFormat.getHeight());
        ret.setScaleType(imageFormat.getScaleType());
        ret.setShrinkOnly(imageFormat.isShrinkOnly());
        ret.setQuality(imageFormat.getQuality());

        return ret;
    }

    /**
     * @param extent
     * @param positiveOnly
     * @return
     */
    public static Integer getIM4JExtent(final int extent) {
        return Integer.valueOf((extent > 0) ? Math.min(extent, MAX_IMAGE_EXTENT) : -1);
    }

    /**
     * @param imageDimension
     * @param targetFormat
     * @return
     */
    public static Dimension getResultingImageDimension(final Dimension imageDimension, final ImageFormat targetFormat) {
        final double targetWidth = targetFormat.getWidth();
        final double targetHeight = targetFormat.getHeight();
        var imgDstWidth = imageDimension.width;
        var imgDstHeight = imageDimension.height;

        if ((targetWidth > 0) && (targetHeight > 0) && (imgDstWidth > 0) && (imgDstHeight > 0)) {
            final var widthScaleFactor = targetWidth / imageDimension.width;
            final var heightScaleFactor = targetHeight / imageDimension.height;

            switch (targetFormat.getScaleType()) {
                case CONTAIN: {
                    // Choose smallest boundary
                    var scaleFactor = Math.min(widthScaleFactor, heightScaleFactor);

                    if (targetFormat.isShrinkOnly()) {
                        scaleFactor = Math.min(scaleFactor, 1.0);
                    }

                    imgDstWidth = (int) Math.round(imgDstWidth * scaleFactor);
                    imgDstHeight = (int) Math.round(imgDstHeight * scaleFactor);

                    break;
                }

                case COVER: {
                    // Choose largest boundary
                    final var scaleFactor = Math.max(widthScaleFactor, heightScaleFactor);

                    imgDstWidth = (int) Math.round(imgDstWidth * scaleFactor);
                    imgDstHeight = (int) Math.round(imgDstHeight * scaleFactor);

                    break;
                }

                case AUTO:
                case COVER_AND_CROP:
                case CONTAIN_FORCE_DIMENSION:
                default: {
                    imgDstWidth = (int) targetWidth;
                    imgDstHeight = (int) targetHeight;

                    break;
                }
            }

            if (imgDstWidth < 3) {
                imgDstWidth = 3;
            }

            if (imgDstHeight < 3) {
                imgDstHeight = 3;
            }
        }

        return new Dimension(imgDstWidth, imgDstHeight);
    }

    /**
     * @param imageDimension
     * @param targetFormat
     * @return
     * @throws Exception
     */
    public static boolean shouldUseReferenceFormat(@NonNull final Dimension imageDimension, @NonNull final ImageFormat targetFormat) {
        // reference formats are possible for all different scaling types;
        // autoRotate=true and shrinkOnly=true flags are mandatory in order to use reference formats
        return ((targetFormat.isAutoRotate() == true) && targetFormat.isShrinkOnly() &&
            (compare(getResultingImageDimension(imageDimension, targetFormat), imageDimension) >= 0));
    }

    /**
     * @param first
     * @param second
     * @return
     */
    public static int compare(@NonNull final Dimension first, @NonNull final Dimension second) {
        // the used area of the given  dimensions is compared
        return Long.compare(((long) first.width) * first.height, ((long) second.width) * second.height);
    }

    /**
     * @param imageKey
     * @param imageFormatStr
     * @return
     * @throws ObjectCacheException
     * @throws IOException
     */
    public static InputStream readImageByTargetFormat(
        @NonNull final IObjectCache objectCache,
        @NonNull final String imageKey,
        @NonNull final ImageFormat imageFormat,
        final boolean removeImageKeyOnError) throws ImageConverterException {

        final var imageFileName = imageFormat.getFormatString();

        try (final var fileReadAcess = objectCache.getReadAccess(IMAGECONVERTER_GROUPID, imageKey, imageFileName)) {
            if (null != fileReadAcess) {
                final var referenceFormatStr = fileReadAcess.getKeyValue(ImageConverterUtils.IMAGECONVERTER_KEY_REFERENCE);

                if (StringUtils.isNotEmpty(referenceFormatStr)) {
                    return readImageByTargetFormat(objectCache, imageKey, ImageFormat.parseImageFormat(referenceFormatStr), removeImageKeyOnError);
                }
                try (final var resultInputStm = fileReadAcess.getInputStream()) {
                    if (null != resultInputStm) {
                        return new ByteArrayInputStream(IOUtils.toByteArray(resultInputStm));
                    }
                }
            }
        } catch (@SuppressWarnings("unused") final Exception e) {
            if (removeImageKeyOnError) {
                LOG.warn(IC_STR_BUILDER().
                    append("error when reading image => Removing image key ").
                    append(IMAGECONVERTER_GROUPID).append('/').append(imageKey).toString());

                try {
                    objectCache.removeKey(IMAGECONVERTER_GROUPID, imageKey);
                } catch (final Exception e1) {
                    LOG.error("IC server caught Exception", e1);
                }
            }

            throw new ImageConverterException(e);
        }

        return null;
    }

    /**
     * @param imageKey
     * @return
     * @throws ImageConverterException
     */
    public static IMetadata readMetadata(
        @NonNull final IObjectCache objectCache,
        @NonNull final IMetadataReader metadataReader,
        @NonNull final String imageKey,
        final boolean removeImageKeyOnError) throws ImageConverterException {

        try {
            final var metadataFileItem = objectCache.get(IMAGECONVERTER_GROUPID, imageKey, ImageConverterUtils.IMAGECONVERTER_METADATA_FILEID);

            // first check, if a metadata file item exists for the given key at all
            if (null != metadataFileItem) {
                // retrieve the metadata content for the given image key
                try (final var fileReadAcess = objectCache.getReadAccess(metadataFileItem)) {
                    if (null != fileReadAcess) {
                        try (final var resultInputStm = fileReadAcess.getInputStream()) {
                            if (null != resultInputStm) {
                                final var reader = new BufferedReader(new InputStreamReader(resultInputStm, "UTF-8"));
                                final var jsonStrBuilder = STR_BUILDER();
                                String readStr = null;

                                while (null != (readStr = reader.readLine())) {
                                    jsonStrBuilder.append(readStr);
                                }

                                if (jsonStrBuilder.length() > 0) {
                                    return metadataReader.readMetadata(JSONServices.parseObject(jsonStrBuilder.toString()));
                                }
                            }
                        }
                    }
                }
            }
        } catch (final Exception e) {
            if (removeImageKeyOnError) {
                LOG.warn(IC_STR_BUILDER().
                    append("error when reading image metadata => Removing image key ").
                    append(IMAGECONVERTER_GROUPID).append('/').append(imageKey).toString());

                try {
                    objectCache.removeKey(IMAGECONVERTER_GROUPID, imageKey);
                } catch (final Exception e1) {
                    LOG.error("IC server caught Exception", e1);
                }
            }

            throw new ImageConverterException(e);
        }

        try {
            return metadataReader.readMetadata(new JSONObject());
        } catch (final Exception e) {
            throw new ImageConverterException(e);
        }
    }

    /**
     * @param metadata
     * @param imageFormats
     * @return
     */
    public static TreeSet<ImageFormat> createImageFormatTargetList(
        @NonNull final TreeSet<ImageFormat> configuredImageFormats,
        @Nullable final TreeSet<ImageFormat> availableImageFormats,
        @NonNull final IMetadata metadata) {

        final var ret = new TreeSet<ImageFormat>();
        final var imageFormatShortName = metadata.getImageFormatName();
        final var isAnimated = imageFormatShortName.startsWith("gif");
        final var isTransparent = isAnimated || imageFormatShortName.startsWith("png") || imageFormatShortName.startsWith("tif");

        for (final ImageFormat curFormat : configuredImageFormats) {
            ImageFormat.ImageType targetFormatImageType = null;

            switch (curFormat.getFormatShortName()) {
                case "jpg": {
                    targetFormatImageType = ImageFormat.ImageType.JPG;
                    break;
                }

                case "png": {
                    targetFormatImageType = ImageFormat.ImageType.PNG;
                    break;
                }

                case "auto":
                default: {
                    targetFormatImageType = (isTransparent  ? ImageFormat.ImageType.PNG : ImageFormat.ImageType.JPG);
                    break;
                }
            }

            final var targetImageFormat = new ImageFormat(targetFormatImageType);

            targetImageFormat.setAutoRotate(curFormat.isAutoRotate());
            targetImageFormat.setWidth(curFormat.getWidth());
            targetImageFormat.setHeight(curFormat.getHeight());
            targetImageFormat.setScaleType(curFormat.getScaleType());
            targetImageFormat.setShrinkOnly(curFormat.isShrinkOnly());

            if ((null == availableImageFormats) || !availableImageFormats.contains(targetImageFormat)) {
                ret.add(targetImageFormat);
            }
        }

        return ret;
    }

    /**
     * @param closeable
     */
    public static void close(final Closeable closeable) {
        if (null != closeable) {
            try {
                closeable.close();
            } catch (final IOException e) {
                LOG.error("IC server caught Exception", e);
            }
        }
    }

    /**
     * @param imageFormats
     * @return
     */
    public static String getImageFormatsListString(final TreeSet<ImageFormat> imageFormats) {
        final var configuredFormatsStringBuilder = new StringBuilder(256);

        for (final ImageFormat curConfiguredImageFormat : imageFormats) {
            if (configuredFormatsStringBuilder.length() > 0) {
                configuredFormatsStringBuilder.append(", ");
            }

            configuredFormatsStringBuilder.append(curConfiguredImageFormat.getFormatString());
        }

        return configuredFormatsStringBuilder.toString();
    }

    // - Implementation --------------------------------------------------------

    /**
     * Moves an entry from the given imageFormatList to the returned candidate
     * list, if either the testSet is empty or the to be tested parameters of
     * the given testImageFormat, defined within the given test set, match the
     * parameters of one of the given imageFormatList entries
     *
     * @param imageFormatList
     * @param testParamSet
     * @return
     */
    private static void implFillImageFormatCandidateList(@NonNull final List<ImageFormat> workingImageFormatList, @NonNull final BitSet testParamSet, @NonNull final ImageFormat testImageFormat, final Set<ImageFormat> candidateSet) {
        final var isAutoFormat = StringUtils.startsWithIgnoreCase(testImageFormat.getFormatShortName(), "auto");

        for (final var iter = workingImageFormatList.iterator(); iter.hasNext(); ) {
            @NonNull final var curImageFormat = iter.next();

            if (testParamSet.isEmpty() ||
                (!testParamSet.get(TEST_AUTOROTATE) || (curImageFormat.isAutoRotate() == testImageFormat.isAutoRotate())) &&
                (!testParamSet.get(TEST_SCALETYPE) || (curImageFormat.getScaleType().equals(testImageFormat.getScaleType())) &&
                    (!testParamSet.get(TEST_SHRINKONLY) || (curImageFormat.isShrinkOnly() == testImageFormat.isShrinkOnly())) &&
                    (!testParamSet.get(TEST_FORMATSHORTNAME) || isAutoFormat || curImageFormat.getFormatShortName().equals(testImageFormat.getFormatShortName())))) {

                // if all important attributes match, move format to candidate list
                candidateSet.add(curImageFormat);
                iter.remove();
            }
        }
    }

    // - Static members --------------------------------------------------------

    /**
     *
     */
    final private static TreeSet<ImageFormat> EMPTY_CANDIDATE_SET = new TreeSet<>((@NonNull final ImageFormat imageFormat1, @NonNull final ImageFormat imageFormat2) -> ImageConverterUtils.compare(
        new Dimension(imageFormat1.getWidth(), imageFormat1.getHeight()),
        new Dimension(imageFormat2.getWidth(), imageFormat2.getHeight())));

    /**
     *
     */
    final private static RetryPolicy<Object> RETRY_POLICY_DELETE_FILE = RetryPolicy.builder().
        withMaxAttempts(FILE_DELETE_ATTEMPTS).
        withBackoff(10, 100, ChronoUnit.MILLIS).
        build();

    final private static BitSet m_paramTestSet = new BitSet();

    final private static int TEST_AUTOROTATE = 0;
    final private static int TEST_SCALETYPE = 1;
    final private static int TEST_SHRINKONLY = 2;
    final private static int TEST_FORMATSHORTNAME = 3;

    final private static AtomicLong TEMP_COUNTER = new AtomicLong(0);

    static {
        // sort list of to be registered IMAGECONVERTER_KEYS custom keys
        Arrays.sort(IMAGECONVERTER_KEYS);

        m_paramTestSet.set(TEST_AUTOROTATE);
        m_paramTestSet.set(TEST_SCALETYPE);
        m_paramTestSet.set(TEST_SHRINKONLY);
        m_paramTestSet.set(TEST_FORMATSHORTNAME);
    }
}
