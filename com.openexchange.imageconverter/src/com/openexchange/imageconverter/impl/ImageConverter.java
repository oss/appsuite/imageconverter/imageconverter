/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.imageconverter.impl;

import static com.openexchange.imageconverter.impl.ImageConverterUtils.LOG;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.google.common.base.Throwables;
import com.openexchange.imageconverter.api.IImageConverter;
import com.openexchange.imageconverter.api.IMetadata;
import com.openexchange.imageconverter.api.IMetadataReader;
import com.openexchange.imageconverter.api.ImageConverterException;
import com.openexchange.imageconverter.api.ImageConverterPriority;
import com.openexchange.imageconverter.api.ImageFormat;
import com.openexchange.imageconverter.api.MetadataImage;
import com.openexchange.imageconverter.impl.ImageConverterMonitoring.RequestType;
import com.openexchange.objectcache.api.GroupConfig;
import com.openexchange.objectcache.api.ICacheObject;
import com.openexchange.objectcache.api.IObjectCache;
import com.openexchange.objectcache.api.IdLocker;
import com.openexchange.objectcache.api.NonNull;
import com.openexchange.objectcache.api.Nullable;
import com.openexchange.objectcache.api.ObjectCacheException;
import com.openexchange.osgi.annotation.SingletonService;

/**
 * {@link ImageConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public final class ImageConverter implements IImageConverter {

    /**
     * Initializes a new {@link ImageConverter}.
     *
     * @param objectCache
     * @param config
     */
    @SingletonService
    public ImageConverter(
        @NonNull final IObjectCache objectCache,
        @NonNull final IMetadataReader metadataReader,
        @NonNull final ImageConverterConfig imageConverterConfig) throws ImageConverterException {

        super();

        m_objectCache = objectCache;
        m_metadataReader = metadataReader;
        m_imageConverterConfig = imageConverterConfig;


        final var imageFormatsRecord = imageConverterConfig.getImageFormatsRecord();
        final var configuredImageFormats = imageFormatsRecord.configuredImageFormats();

        // the given image Format list has to contains at least one valid entry
        if (configuredImageFormats.size() < 1) {
            throw new ImageConverterException("IC server detected ImageFormat array with no valid entries => ensure at least one valid ImageFormat contained");
        }

        // create ImageProcessor
        m_imageProcessor = new ImageProcessor(
            ImageConverterConfig.IMAGECONVERTER_SEARCHPATH,
            ImageConverterConfig.IMAGECONVERTER_USE_GRAPHICSMAGICK,
            ImageConverterConfig.IMAGECONVERTER_IMAGE_USERCOMMENT);

        if ((null != m_objectCache) && m_objectCache.isValid()) {
            try {
                // register group at ObjectCache
                final var groupConfig = GroupConfig.builder().
                    withGroupId(ImageConverterUtils.IMAGECONVERTER_GROUPID).
                    withCustomKeys(ImageConverterUtils.IMAGECONVERTER_KEYS).
                    withMaxGroupSize(ImageConverterConfig.IMAGECONVERTER_CACHE_MAX_SIZE).
                    withMaxKeyCount(ImageConverterConfig.IMAGECONVERTER_CACHE_MAX_KEY_COUNT).
                    withKeyTimeoutMillis(ImageConverterConfig.IMAGECONVERTER_CACHE_KEY_TIMEOUT_MILLIS).
                    withCleanupPeriodMillis(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_PERIOD_MILLIS).
                    withCleanupThresholdUpperPercentage(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE).
                    withCleanupThresholdLowerPercentage(ImageConverterConfig.IMAGECONVERTER_CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE).build();

                m_objectCache.registerGroup(groupConfig);
            } catch (final ObjectCacheException e) {
                throw new ImageConverterException("IC server error when registering group at ObjectCache", e);
            }

            // create and start worker queue to perform the image transformations (either synchronously or asynchrnously)
            m_queue = new ImageConverterQueue(this, m_imageProcessor, m_objectCache, m_metadataReader);
        } else {
            m_queue = null;

            LOG.error(ImageConverterUtils.IC_STR_BUILDER().
                append("got no valid ObjectCache interface => Falling back to on demand conversion for cacheAndGet* requests without cache! ").
                append("Please setup the ObjectCache correctly to speed up performance before next service restart.").
                toString());
        }

        // Create ImageConverterDirect optional member to be used for non cached fallback conversions
        m_imageConverterDirect = m_imageProcessor.isAlive() ?
            new ImageConverterDirect(this, m_imageProcessor, metadataReader) :
                null;

        // finally trace list of used image formats
        final var infoStr = ImageConverterUtils.IC_STR_BUILDER().
            append("is using the following target formats:\n");

        for (final ImageFormat curImageFormat : getImageFormatsRecord().configuredImageFormats()) {
            infoStr.append("\tIC ").append(curImageFormat.toString()).append('\n');
        }

        LOG.info(infoStr.toString());

        // initialize ImageConverterUtils with this singleton object
        ImageConverterUtils.init(this);

        implKillOrphanedProcesses();
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            final var shutdownStartTimeMillis = System.currentTimeMillis();

            LOG.info("IC server starting shutdown");

            if (null != m_objectCache) {
                m_objectCache.shutdownGroup(ImageConverterUtils.IMAGECONVERTER_GROUPID);
            }

            // might be null in case no ObjectCache is available
            if (null != m_queue) {
                m_queue.shutdown();
            }

            implKillOrphanedProcesses();

            LOG.info("IC server shutdown finished in {}ms", (System.currentTimeMillis() - shutdownStartTimeMillis));
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_running.get();
    }

    /**
     * @return
     */
    public ImageProcessor.ImageProcessorStatus getImageProcessorStatus() {
        return m_imageProcessor.getStatus();
    }

    /**
     * @return
     */
    public ImageFormatsRecord getImageFormatsRecord() {
        return m_imageConverterConfig.getImageFormatsRecord();
    }

    // - IImageConverter ----------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImage(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public InputStream getImage(final String imageKey, final String requestFormat, final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getImage called (key: {}, requested: {}) ", imageKey, requestFormat);
        }

        var status = (isNotEmpty(imageKey) && isNotEmpty(requestFormat)) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_ERROR;
                return null;
            }

            try (final var metadataImage = m_queue.getMetadataImage(imageKey, requestFormat)) {
                final var ret = (null != metadataImage) ?
                    metadataImage.getImageInputStream(true) :
                        null;

                if (null == ret) {
                    status = ImageConverterStatus.KEY_NOT_AVAILABLE;
                }

                return ret;
            } catch (final Exception e) {
                LOG.trace("IC server caught Exception while getting MetadataImage (key: {}, requested: {})",
                    imageKey, requestFormat, e);

                try {
                    status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                        ImageConverterStatus.CACHE_ERROR :
                            ImageConverterStatus.GENERAL_ERROR;
                } catch (final ClassCastException e1) {
                    status = ImageConverterStatus.GENERAL_ERROR;
                }

                throw new ImageConverterException("IC server error while getting MetadataImage", e);
            }
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#getMetadata(java.lang.String, java.lang.String[])
     */
    @Override
    public IMetadata getMetadata(final String imageKey, final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getMetadata called (key: {}) ", imageKey);
        }

        var status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_ERROR;
                return null;
            }

            final var ret = (ImageConverterStatus.OK == status) ?
                m_queue.getMetadata(imageKey) :
                    null;

            if (null == ret) {
                status = ImageConverterStatus.KEY_NOT_AVAILABLE;
            }

            return ret;
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while getting Metadata", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#getImageAndMetadata(java.lang.String, java.lang.String, java.lang.String[])
     */
    @Override
    public MetadataImage getImageAndMetadata(final String imageKey, final String requestFormat, final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getImageAndMetadata called (key: {}, requested: {}) ", imageKey, requestFormat);
        }

        var status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation possible ATM => return null
            if ((null == m_queue) || !m_cacheStatusOK.get()) {
                status = ImageConverterStatus.CACHE_ERROR;
                return null;
            }

            final var ret = (ImageConverterStatus.OK == status) ?
                m_queue.getMetadataImage(imageKey, requestFormat) :
                    null;

            if (null == ret) {
                status = ImageConverterStatus.KEY_NOT_AVAILABLE;
            }

            return ret;
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while getting ImageAndMetadata", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.GET, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheImage(java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public void cacheImage(final String imageKey, final InputStream inputStm, final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #cacherImage called (key: {}) ", imageKey);
        }

        var status = (isNotEmpty(imageKey) && (null != inputStm)) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            // no cache operation => do nothing
            if (null == m_queue) {
                status = ImageConverterStatus.CACHE_ERROR;
                return;
            }

            if (ImageConverterStatus.OK == status) {
                m_queue.cacheAndGetMetadataImage(imageKey, inputStm, ImageConverterPriority.BACKGROUND, null, implGetContext(context), m_cacheStatusOK.get());
            }
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while caching Image", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.CACHE, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getImage(java.lang.String, java.lang.String, java.lang.String, java.io.InputStream)
     */
    @Override
    public InputStream cacheAndGetImage(final String imageKey, final String requestFormat, final InputStream inputStm, final String... context) throws ImageConverterException {
        return cacheAndGetImage(imageKey, requestFormat, inputStm, new Mutable<>(Boolean.TRUE), context);
    }

    /* (non-Javadoc)
     * @see com.openexchange.imageconverter.api.IImageConverter#cacheAndGetImageAndMetadata(java.lang.String, java.lang.String, java.io.InputStream, java.lang.String[])
     */
    @Override
    public MetadataImage cacheAndGetImageAndMetadata(final String imageKey, final String requestFormat, final InputStream inputStm, final String... context) throws ImageConverterException {
        return cacheAndGetImageAndMetadata(imageKey, requestFormat, inputStm, new Mutable<>(Boolean.TRUE), context);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImages()
     */
    @Override
    public void clearImages(final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #clearImages called");
        }

        var status = ImageConverterStatus.OK;

        try {
            remove(Arrays.asList(getAvailable(implCreateContextProperties(context))));
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while clearing Images", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#clearImagesByKey(java.lang.String)
     */
    @Override
    public void clearImagesByKey(final String imageKey) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #clearImagesByKey called (key: {}) ", imageKey);
        }

        var status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            if (ImageConverterStatus.OK == status) {
                remove(imageKey);
            }
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while clearing images by key", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeyCount()
     */
    @Override
    public long getKeyCount(final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getKeyCount called");
        }

        var status = ImageConverterStatus.OK;

        try {
            return isEmpty(context) ?
                getKeyCount() :
                    getAvailable(implCreateContextProperties(context)).length;
        } catch (final Exception e) {
            LOG.trace("IC server caught Exception while getting key count", e);

            try {
                status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                    ImageConverterStatus.CACHE_ERROR :
                        ImageConverterStatus.GENERAL_ERROR;
            } catch (final ClassCastException e1) {
                status = ImageConverterStatus.GENERAL_ERROR;
            }

            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeysByContext(java.lang.String)
     */
    @Override
    public String[] getKeys(final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getKeys called");
        }

        var status = ImageConverterStatus.OK;

        try {
            return getAvailable(implCreateContextProperties(context));
        } catch (final ImageConverterException e) {
            status = ImageConverterStatus.GENERAL_ERROR;
            LOG.trace("IC server caught Exception while getting keys", e);
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.imageconverter.api.IImageConverter#getKeyCount()
     */
    @Override
    public long getTotalImagesSize(final String... context) throws ImageConverterException {
        final var requestStartTime = System.currentTimeMillis();

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server #getTotalImagesSize called");
        }

        var status = ImageConverterStatus.OK;

        try {
            return isEmpty(context) ?
                getTotalImagesSize() :
                    getAvailableImagesSize(implCreateContextProperties(context));
        } catch (final ImageConverterException e) {
            LOG.trace("IC server caught Exception while getting total images size", e);

            status = ImageConverterStatus.GENERAL_ERROR;
            throw e;
        } finally {
            implFinishRequest(RequestType.ADMIN, requestStartTime, status);
        }
    }

    // - Interface -------------------------------------------------------------

    /**
     * @return
     */
    public @Nullable IObjectCache getFileItemService() {
        return m_objectCache;
    }

    /**
     * @return
     */
    public IMetadataReader getMetadataReader() {
        return m_metadataReader;
    }

    /**
     * @return
     * @throws ImageConverterException
     */
    public long getKeyCount() throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getKeyCount(ImageConverterUtils.IMAGECONVERTER_GROUPID) :
                    0;
        } catch (final ObjectCacheException e) {
            throw new ImageConverterException("IC server error while getting image key count", e);
        }
    }

    /**
     * @return
     */
    public long getTotalImagesSize() throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getGroupSize(ImageConverterUtils.IMAGECONVERTER_GROUPID) :
                    0;
        } catch (final ObjectCacheException e) {
            throw new ImageConverterException("IC server error while getting total images size", e);
        }
    }

    /**
     * @param searchProperties
     * @return
     * @throws ImageConverterException
     */
    public long getAvailableImagesSize(@Nullable final Properties searchProperties) throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.getGroupSize(ImageConverterUtils.IMAGECONVERTER_GROUPID, searchProperties) :
                    0;
        } catch (final ObjectCacheException e) {
            throw new ImageConverterException("IC server error while getting available images size", e);
        }
    }

    /**
     * @param imageKey
     * @return
     * @throws ImageConverterException
     */
    public boolean isAvailable(@NonNull final String imageKey) throws ImageConverterException {
        try {
            return (null != m_objectCache) ?
                m_objectCache.containsKey(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey) :
                    false;
        } catch (final ObjectCacheException e) {
            throw new ImageConverterException("IC server error while checking availability", e);
        }
    }

    /**
     * @param searchProperties
     * @return
     * @throws ImageConverterException
     */
    @NonNull public String[] getAvailable(@Nullable final Properties searchProperties) throws ImageConverterException {
        if (null != m_objectCache) {
            try {
                final var foundObjs = (null != searchProperties) ?
                    m_objectCache.get(ImageConverterUtils.IMAGECONVERTER_GROUPID, searchProperties) :
                        m_objectCache.getKeys(ImageConverterUtils.IMAGECONVERTER_GROUPID);

                if (isNotEmpty(foundObjs)) {
                    final var ret = new String[foundObjs.length];
                    final var imageKeySet = (null != searchProperties) ? new HashSet<>() : null;
                    var count = 0;

                    for (final Object curObj : foundObjs) {
                        if (null != curObj) {
                            final var curImageKey = (curObj instanceof ICacheObject) ?
                                ((ICacheObject) curObj).getKeyId() :
                                    curObj.toString();

                            if (isNotEmpty(curImageKey) && ((null == imageKeySet) || imageKeySet.add(curImageKey))) {
                                ret[count++] = curImageKey;
                            }
                        }
                    }

                    return ArrayUtils.subarray(ret, 0, count);
                }
            } catch (final ObjectCacheException e) {
                throw new ImageConverterException("IC server error while retrieving available image keys", e);
            }
        }

        return ArrayUtils.EMPTY_STRING_ARRAY;
    }

    /**
     * @param imageKey
     * @param inputStm
     * @param context
     * @return
     */
    public void remove(@NonNull final String imageKey) throws ImageConverterException {
        if (m_running.get() && (null != m_queue)) {
            if (IdLocker.lock(imageKey, IdLocker.Mode.TRY_LOCK)) {
                try {
                    // only remove keys that are not currently processed
                    if (!m_queue.isProcessing(imageKey)) {
                        m_objectCache.removeKey(ImageConverterUtils.IMAGECONVERTER_GROUPID, imageKey);
                    }
                } catch (final ObjectCacheException e) {
                    throw new ImageConverterException("IC server error while removing image key", e);
                } finally {
                    IdLocker.unlock(imageKey);
                }
            }
        }
    }

    /**
     * @param imageKeys
     * @throws ImageConverterException
     */
    public void remove(@NonNull final Collection<String> imageKeysCollection) throws ImageConverterException {
        if (!imageKeysCollection.isEmpty() && m_running.get() && (null != m_queue)) {
            final var targetImageKeys = imageKeysCollection.stream().
                filter(curImageKey -> {
                    if (IdLocker.lock(curImageKey, IdLocker.Mode.TRY_LOCK)) {
                        // only remove keys that are not currently processed
                        if (m_queue.isProcessing(curImageKey)) {
                            IdLocker.unlock(curImageKey);
                            return false;
                        }

                        // current key needs to be unlocked after finishing removal
                        return true;
                    }

                    return false;
                }).
                toArray(imageKeyCount -> new String[imageKeyCount]);

            if (isNotEmpty(targetImageKeys)) {
                try {
                    m_objectCache.removeKeys(ImageConverterUtils.IMAGECONVERTER_GROUPID, targetImageKeys);
                } catch (final ObjectCacheException e) {
                    throw new ImageConverterException("IC server error while removing image " + targetImageKeys.length + " keys", e);
                } finally {
                    for (final String curImageKey : targetImageKeys) {
                        IdLocker.unlock(curImageKey);
                    }
                }
            }
        }
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param inputStm
     * @param context
     * @return
     */
    public InputStream cacheAndGetImage(final String imageKey,
        final String requestFormat,
        final InputStream inputStm,
        final Mutable<Boolean> usedCache,
        final String... context) throws ImageConverterException {

        final var requestStartTime = System.currentTimeMillis();
        final var trace = LOG.isTraceEnabled();

        if (trace) {
            LOG.trace("IC server #cacheAndGetImage called (key: {}, requested: {}) ", imageKey, requestFormat);
        }

        var status = isNotEmpty(imageKey) ?
            ImageConverterStatus.OK :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            final var inputBuffer = IOUtils.toByteArray(inputStm);

            // try cache based operation first
            if ((null != m_queue) && (ImageConverterStatus.OK == status)) {
                // only remove entry in error case completely if cache is accesible
                try (final var bufferInputStm = new ByteArrayInputStream(inputBuffer);
                    final var metadataImage = (null != bufferInputStm) ?
                        m_queue.cacheAndGetMetadataImage(imageKey, bufferInputStm, ImageConverterPriority.BACKGROUND, requestFormat, implGetContext(context), m_cacheStatusOK.get()) :
                            (isNotEmpty(requestFormat) ?
                                m_queue.getMetadataImage(imageKey, requestFormat) :
                                    null)) {

                    final var ret = (null != metadataImage) ? metadataImage.getImageInputStream(true) : null;

                    if (null == ret) {
                        status = ImageConverterStatus.KEY_NOT_AVAILABLE;
                    }

                    usedCache.set(Boolean.TRUE);
                    implLogChangedCacheStatus(true);

                    return ret;
                } catch (IOException | ImageConverterException e) {
                    if (trace) {
                        LOG.trace("IC server caught Exception while caching and getting Image (key: {}, requested: {})",
                            imageKey, requestFormat, e);
                    }

                    try {
                        status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                            ImageConverterStatus.CACHE_ERROR :
                                ImageConverterStatus.GENERAL_ERROR;
                    } catch (final ClassCastException e1) {
                        status = ImageConverterStatus.GENERAL_ERROR;
                    }
                }
            }

            // update cache status in case of a cache error
            if (ImageConverterStatus.CACHE_ERROR == status) {
                usedCache.set(Boolean.FALSE);
                implLogChangedCacheStatus(false);
            }

            // no cache based conversion or exception during cache based queue conversion => use on demand conversion, if possible
            if ((null != m_imageConverterDirect) && (null != inputBuffer)) {
                final var targetFormat = ImageFormat.parseImageFormat(requestFormat);

                try (final var bufferInputStm = new ByteArrayInputStream(inputBuffer);
                    final var metadataImage = m_imageConverterDirect.convert(imageKey, bufferInputStm, targetFormat)) {

                    if (null != metadataImage) {
                        return metadataImage.getImageInputStream(true);
                    }
                } catch (final Exception e) {
                    if (trace) {
                        LOG.trace("IC server caught Exception while performing on demand image conversion in #cacheAndGetImage (key: {}, requested: {})",
                            imageKey, requestFormat, e);
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("IC server caught Exception while creating input buffer in #cacheAndGetImage (key: {}, requested: {})",
                imageKey, requestFormat, e);
        } finally {
            implFinishRequest(RequestType.CACHE_AND_GET, requestStartTime, status);
        }

        return null;
    }

    /**
     * @param imageKey
     * @param requestFormat
     * @param inputStm
     * @param context
     * @param usedCache
     * @return
     * @throws ImageConverterException
     */
    public MetadataImage cacheAndGetImageAndMetadata(final String imageKey,
        final String requestFormat,
        final InputStream inputStm,
        final Mutable<Boolean> usedCache,
        final String... context) throws ImageConverterException {

        final var requestStartTime = System.currentTimeMillis();
        final var trace = LOG.isTraceEnabled();

        if (trace) {
            LOG.trace("IC server #cacheAndGetImageAndMetadata called (key: {}, requested: {}) ", imageKey, requestFormat);
        }

        var status = isNotEmpty(imageKey) ?
            (((null != inputStm) || isNotEmpty(requestFormat)) ? ImageConverterStatus.OK : ImageConverterStatus.KEY_NOT_AVAILABLE) :
                ImageConverterStatus.KEY_NOT_VALID;

        try {
            final var inputBuffer = IOUtils.toByteArray(inputStm);

            // try cache based operation first
            if ((null != m_queue) && (ImageConverterStatus.OK == status)) {
                try {
                    usedCache.set(Boolean.TRUE);
                    implLogChangedCacheStatus(true);

                    try (final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer)) {
                        // only remove entry in error case completely if cache is accesible
                        return (null != inputBuffer) ?
                            m_queue.cacheAndGetMetadataImage(imageKey, bufferInputStm, ImageConverterPriority.BACKGROUND, requestFormat, implGetContext(context), m_cacheStatusOK.get()) :
                                m_queue.getMetadataImage(imageKey, requestFormat);
                    }
                } catch (final Exception e) {
                    if (trace) {
                        LOG.trace("IC server caught Exception while caching and getting ImageAndMetadata (key: {}, requested: {})",
                            imageKey, requestFormat, e);
                    }

                    try {
                        status = (Throwables.getCauseAs(e, ObjectCacheException.class) instanceof ObjectCacheException) ?
                            ImageConverterStatus.CACHE_ERROR :
                                ImageConverterStatus.GENERAL_ERROR;
                    } catch (final ClassCastException e1) {
                        status = ImageConverterStatus.GENERAL_ERROR;
                    }
                }
            }

            // update cache status in case of a cache error
            if (ImageConverterStatus.CACHE_ERROR == status) {
                usedCache.set(Boolean.FALSE);
                implLogChangedCacheStatus(false);
            }

            // no cache based conversion or exception during cache based queue conversion => use on demand conversion, if possible
            if ((null != m_imageConverterDirect) && (null != inputBuffer)) {
                final var targetFormat = ImageFormat.parseImageFormat(requestFormat);

                try (final InputStream bufferInputStm = new ByteArrayInputStream(inputBuffer)) {
                    final var ret = m_imageConverterDirect.convert(imageKey, bufferInputStm, targetFormat);

                    if (null != ret) {
                        return ret;
                    }
                } catch (final Exception e) {
                    if (trace) {
                        LOG.trace("IC server caught Exception while performing on demand image conversion in #cacheAndGetImageAndMetadata (key: {}, requested: {})",
                            imageKey, requestFormat, e);
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("IC server caught Exception while creating input buffer in #cacheAndGetImage (key: {}, requested: {})",
                imageKey, requestFormat, e);
        } finally {
            implFinishRequest(RequestType.CACHE_AND_GET, requestStartTime, status);
        }

        return null;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param requestType
     * @param requestStartTime
     * @param imageConverterStatus
     */
    private static void implFinishRequest(@NonNull final RequestType requestType, final long requestStartTime, @NonNull final ImageConverterStatus imageConverterStatus) {
        final var requestDuration = System.currentTimeMillis() - requestStartTime;

        if (LOG.isTraceEnabled()) {
            LOG.trace("IC server request type " + requestType.name() + " finished in " + Long.toString(requestDuration) + "ms");
        }

        ImageConverterUtils.IC_MONITOR.incrementRequests(requestType, requestDuration, imageConverterStatus);
    }

    /**
     *
     */
    private void implKillOrphanedProcesses() {
        try {
            // perform cleanup of possible orphaned convert processes
            ImageProcessor.killOrphanedProcess(true);
        } catch (final Exception e) {
            LOG.error("IC server received Exception when calling orphaned process cleanup task", e);
        }
    }

    /**
     *
     */
    private void implLogChangedCacheStatus(final boolean usedCache) {
        if (usedCache) {
            if (m_cacheStatusOK.compareAndSet(false, true)) {
                LOG.info(ImageConverterUtils.IC_STR_BUILDER().
                    append("(re)enabled cache based image operation.").toString());
            }
        } else if (m_cacheStatusOK.compareAndSet(true, false)) {
            LOG.warn(ImageConverterUtils.IC_STR_BUILDER().
                append("disabled cache based image operation. Using direct on demand conversion instead!").toString());
        }
    }

    /**
     * @param context
     * @return
     */
    private static Properties implCreateContextProperties(final String... context) {
        final var usedContext = implGetContext(context);
        Properties ret = null;

        if (null != usedContext) {
            (ret = new Properties()).setProperty(ImageConverterUtils.IMAGECONVERTER_KEY_CONTEXT, implGetContext(context));
        }

        return ret;
    }

    /**
     * @param context
     * @return
     */
    private static String implGetContext(final String... context) {
        return isNotEmpty(context) && isNotEmpty(context[0]) ? context[0] : null;
    }

    // - Members ---------------------------------------------------------------

    final private IObjectCache m_objectCache;

    final private IMetadataReader m_metadataReader;

    final private ImageConverterConfig m_imageConverterConfig;

    final private ImageConverterDirect m_imageConverterDirect;

    final private ImageProcessor m_imageProcessor;

    final private ImageConverterQueue m_queue;

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicBoolean m_cacheStatusOK = new AtomicBoolean(true);
}

