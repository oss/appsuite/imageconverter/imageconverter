package com.openexchange.imageconverter.impl;

import java.util.TreeSet;

import com.openexchange.imageconverter.api.ImageFormat;

public record ImageFormatsRecord(TreeSet<ImageFormat> configuredImageFormats, TreeSet<ImageFormat> possibleImageFormats) {}


